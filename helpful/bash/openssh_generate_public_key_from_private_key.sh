#!/usr/bin/env bash

# This format can be used in VCS like gitlab and bitbucket
ssh-keygen -y -f id_rsa > id_rsa.pub

# ...this one cannot
openssl rsa -in mykey.pem -pubout > mykey.pub