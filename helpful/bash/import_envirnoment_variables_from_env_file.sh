#!/usr/bin/env bash

while LANG=C IFS= read -r in ; do if [[ "$in" != $'\r' ]]; then export "${in//[$'\t\r\n']}"; fi done <.env
