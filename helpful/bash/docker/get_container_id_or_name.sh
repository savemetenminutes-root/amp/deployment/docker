#!/usr/bin/env bash

# Get the container ID
env | grep -oP '(?<=HOSTNAME=).*'
# - or -
hostname
# - or -
less -FX /proc/sys/kernel/hostname

# Get the container name
# dnsutils contains dig, net-tools contains ifconfig
# apt-install dnsutils net-tools
dig -x `ifconfig eth0 | grep 'inet' | awk '{print $2}' | grep -oP '(addr:\K)?.*'` +short | cut -d'.' -f1
