#!/usr/bin/env bash

# https://github.com/nginx/nginx/blob/master/conf/fastcgi_params
export QUERY_STRING=XDEBUG_SESSION_START=asd;
export REQUEST_METHOD=OPTIONS;
export CONTENT_TYPE=;
export CONTENT_LENGTH=0;

export SCRIPT_NAME=/index.php;
export REQUEST_URI=/sso/login;
export DOCUMENT_URI=/index.php;
export DOCUMENT_ROOT=/var/www/content/eduspire-sso/public;
export SERVER_PROTOCOL=HTTP/1.1
export REQUEST_SCHEME=https
export HTTPS=on

export GATEWAY_INTERFACE=CGI/1.1
export SERVER_SOFTWARE=nginx/1.15.2

export REMOTE_ADDR=222.222.0.1
export REMOTE_PORT=491
export SERVER_ADDR=222.222.0.10
export SERVER_PORT=491
export SERVER_NAME=\$host

# PHP only, required if PHP was built with --enable-force-cgi-redirect
# fastcgi_param  REDIRECT_STATUS    200;

export SCRIPT_FILENAME=/var/www/content/eduspire-sso/public/index.php;

export XDEBUG_SESSION=asd;
export XDEBUG_SESSION_START=asd;
export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_port=9900 remote_host=127.0.0.1 remote_connect_back=0";
echo "OPTIONS /sso/login?XDEBUG_SESSION_START=asd HTTP/1.1
Origin: https://localhost:4200
Access-Control-Request-Method: OPTIONS
User-Agent: PostmanRuntime/7.28.4
Cache-Control: no-cache
Host: 127.0.0.1:20491
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
Cookie: XDEBUG_SESSION=asd
" | cgi-fcgi -bind -connect "127.0.0.1:9090"



# var_export($_SERVER, true);
# array (
#   'HOSTNAME' => '7320684e29f4',
#   'PHP_VERSION' => '8.0.9',
#   'PHP_INI_DIR' => '/usr/local/etc/php',
#   'GPG_KEYS' => '1729F83938DA44E27BA0F4D3DBDB397470D12172 BFDDD28642824F8118EF77909B67A5C12229118F',
#   'PHP_LDFLAGS' => '-Wl,-O1 -pie',
#   'PWD' => '/var/www/content',
#   'DB_PORT' => '3306',
#   'NOTVISIBLE' => 'in users profile',
#   'HOME' => '/root',
#   'PHP_SHA256' => '71a01b2b56544e20e28696ad5b366e431a0984eaa39aa5e35426a4843e172010',
#   'PHPIZE_DEPS' => 'autoconf 		dpkg-dev dpkg 		file 		g++ 		gcc 		libc-dev 		make 		pkgconf 		re2c',
#   'DB_HOST' => 'eduspire-mariadb',
#   'PHP_URL' => 'https://www.php.net/distributions/php-8.0.9.tar.xz',
#   'DOCTRINE_DB_PORT' => '3306',
#   'DOCTRINE_DB_HOST' => 'eduspire-mariadb',
#   'PHP_EXTRA_CONFIGURE_ARGS' => '--enable-fpm --with-fpm-user=www-data --with-fpm-group=www-data --disable-cgi',
#   'SHLVL' => '1',
#   'PHP_CFLAGS' => '-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
#   'PATH' => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
#   'PHP_ASC_URL' => 'https://www.php.net/distributions/php-8.0.9.tar.xz.asc',
#   'PHP_CPPFLAGS' => '-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64',
#   '_' => '/usr/bin/supervisord',
#   'SUPERVISOR_ENABLED' => '1',
#   'SUPERVISOR_SERVER_URL' => 'unix:///run/supervisord.sock',
#   'SUPERVISOR_PROCESS_NAME' => 'php',
#   'SUPERVISOR_GROUP_NAME' => 'php',
#   'DD_VERSION' => '',
#   'DD_SERVICE' => '',
#   'DD_ENV' => '',
#   'USER' => 'root',
#   'HTTP_COOKIE' => 'XDEBUG_SESSION=asd',
#   'HTTP_CONNECTION' => 'keep-alive',
#   'HTTP_ACCEPT_ENCODING' => 'gzip, deflate, br',
#   'HTTP_HOST' => '127.0.0.1:20491',
#   'HTTP_POSTMAN_TOKEN' => '9ec10e74-ecd0-4703-b795-b7fd8d045afa',
#   'HTTP_CACHE_CONTROL' => 'no-cache',
#   'HTTP_USER_AGENT' => 'PostmanRuntime/7.28.4',
#   'HTTP_ACCESS_CONTROL_REQUEST_METHOD' => 'OPTIONS',
#   'HTTP_ORIGIN' => 'https://localhost:4200',
#   'SCRIPT_FILENAME' => '/var/www/content/eduspire-sso/public/index.php',
#   'REDIRECT_STATUS' => '200',
#   'SERVER_NAME' => '$host',
#   'SERVER_PORT' => '491',
#   'SERVER_ADDR' => '222.222.0.10',
#   'REMOTE_PORT' => '51642',
#   'REMOTE_ADDR' => '222.222.0.1',
#   'SERVER_SOFTWARE' => 'nginx/1.15.2',
#   'GATEWAY_INTERFACE' => 'CGI/1.1',
#   'HTTPS' => 'on',
#   'REQUEST_SCHEME' => 'https',
#   'SERVER_PROTOCOL' => 'HTTP/1.1',
#   'DOCUMENT_ROOT' => '/var/www/content/eduspire-sso/public',
#   'DOCUMENT_URI' => '/index.php',
#   'REQUEST_URI' => '/sso/login',
#   'SCRIPT_NAME' => '/index.php',
#   'CONTENT_LENGTH' => '',
#   'CONTENT_TYPE' => '',
#   'REQUEST_METHOD' => 'OPTIONS',
#   'QUERY_STRING' => '',
#   'FCGI_ROLE' => 'RESPONDER',
#   'PHP_SELF' => '/index.php',
#   'REQUEST_TIME_FLOAT' => 1643648167.793583,
#   'REQUEST_TIME' => 1643648167,
# )
