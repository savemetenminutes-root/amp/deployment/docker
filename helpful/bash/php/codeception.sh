#!/usr/bin/env bash

# Run last failed tests (kinda buggy). Requires the RunFailed extension which is enabled by default and available since
# Codeception 2.0
./vendor/codeception/codeception/codecept run -g failed
# - or -
vendor/bin/codecept run -g failed
