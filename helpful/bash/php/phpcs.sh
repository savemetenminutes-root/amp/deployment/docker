#!/usr/bin/env bash

# Exclude a certain sniff. Sniffs are located in vendor/squizlabs/php_codesniffer/src/Standards and subdirectories.
# The "Sniff" suffix must be omitted. Globbing is permitted on the last segment. Comma separate multiple sniffs.
# Default options can be provided in a phpcs.xml file.
./vendor/squizlabs/php_codesniffer/bin/phpcs --exclude="Standards.Generic.ExecutableFile,Generic.Formatting.*"

# Show the sniff codes in all reports (useful in order to figure out what needs to be excluded)
vendor/bin/phpcs -s
