#!/usr/bin/env bash

# Useful options:
#
# -P, --perl-regexp
# Interpret PATTERN as a Perl regular expression. This is highly experimental and grep -P may warn of unimplemented features.
#
# -E, --extended-regexp
# Interpret PATTERN as an extended regular expression (ERE, see below). (-E is specified by POSIX .)

# Find all occurrences of strings ending with .yml which don't start with a hashtag (Perl style Regular Expressions must be enabled for the negative backreference to work)
grep -P '^(?!#).*\.yml$' docker-compose.yml.template
