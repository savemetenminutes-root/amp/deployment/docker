#!/usr/bin/env bash

# Create a SOCKS proxy tunnel to the target IP address on port 5858 using TCPv4
ssh -4 -D 5858 root@192.168.0.253
# From the source host the newly opened connection can be used with cURL for example
curl --insecure -x socks5h://127.0.0.1:5858 -O https://ftp5.gwdg.de/pub/opensuse/discontinued/distribution/11.4/repo/oss/suse/x86_64/libpopt0-1.16-4.1.x86_64.rpm
# ...or Git
git clone --recurse-submodules git@github.com:boostorg/boost.git --config 'http.proxy=socks5h://127.0.0.1:5858'
# ...or some programs respect the ALL_PROXY environment variable
export ALL_PROXY=socks5h://127.0.0.1:5858
