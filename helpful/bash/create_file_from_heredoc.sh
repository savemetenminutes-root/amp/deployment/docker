#!/usr/bin/env bash

# Allow variable expansion
cat > /etc/mysql/conf.d/custom.cnf << EOL
EOL

# Literal content (no expansion)
cat > /etc/mysql/conf.d/custom.cnf << 'EOL'
EOL
