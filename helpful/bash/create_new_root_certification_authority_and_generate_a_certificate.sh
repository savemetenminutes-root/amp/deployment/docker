#!/usr/bin/env bash

# Configuring your CA
mkdir -p /home/myCA/certs
mkdir /home/myCA/csr
mkdir /home/myCA/newcerts
mkdir /home/myCA/private
cp /etc/pki/tls/openssl.cnf /home/myCA
cd /home/myCA
echo 00 > serial
echo 00 > crlnumber
touch index.txt
nano /home/myCA/openssl.cnf
## change:
#dir = /etc/pki/CA		# Where everything is kept
## to:
#dir = /home/myCA		# Where everything is kept
# Create the CA
# Create CA private key: (replace PASSWORD)
openssl genrsa -des3 -passout pass:PASSWORD -out  private/rootCA.key 2048
# Remove passphrase (replace PASSWORD)
openssl rsa -passin pass:PASSWORD -in private/rootCA.key -out private/rootCA.key
# Create CA self-signed certificate
#/C=	Country	BG
#/ST=	State	Shumen
#/L=	Location	Shumen
#/O=	Organization	Templay
#/OU=	Organizational Unit	Management
#/CN=	Common Name	templay.org
openssl req -config openssl.cnf -new -x509 -subj '/C=BG/ST=Shumen/L=Shumen/O=Templay/OU=Management/CN=templay.org' -days 999 -key private/rootCA.key -out certs/rootCA.crt



# Generate a new certificate
cd /home/myCA

# Create a SSL Server certificate
# Create a private key (replace PASSWORD)
openssl genrsa -des3 -passout pass:PASSWORD -out /path/to/new_private_key.key 2048
# Remove passphrase (replace PASSWORD)
openssl rsa -passin pass:PASSWORD -in /path/to/new_private_key.key -out /path/to/new_private_key.key
# Create a Certificate Sign Request (replace xxx.xxx.xxx.xxx:xxxxx with ip address and port)
openssl req -config openssl.cnf -new -subj '/C=BG/ST=Shumen/L=Shumen/O=Templay/OU=Management/CN=xxx.xxx.xxx.xxx:xxxxx' -key /path/to/new_private_key.key -out /path/to/new_certificate_sign_request.csr
# Create the certificate
openssl ca -batch -config openssl.cnf -days 999 -in /path/to/new_certificate_sign_request.csr -out /path/to/new_certificate.crt -keyfile private/rootCA.key -cert certs/rootCA.crt -policy policy_anything
# Export the certificate to pkcs12 for import in the browser (replace PASSWORD)
openssl pkcs12 -export -passout pass:PASSWORD -in /path/to/new_certificate.crt -inkey /path/to/new_private_key.key -certfile certs/rootCA.crt -out /path/to/new_pk12_sstore.p12
