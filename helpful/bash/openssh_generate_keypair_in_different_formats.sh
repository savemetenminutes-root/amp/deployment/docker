#!/usr/bin/env bash

# With no format option (default) or with `-m rfc4716` the private key format is RFC4716 and looks like this:
'
-----BEGIN OPENSSH PRIVATE KEY-----
{content}
-----END OPENSSH PRIVATE KEY-----
'
# With `-m pem` the private key format is PKCS#1 PEM and looks like this:
ssh-keygen -b 4096 -t rsa -m pem -C "email@example.com" -f ./private_key_name
'
-----BEGIN RSA PRIVATE KEY-----
{content}
-----END RSA PRIVATE KEY-----
'
# With `-m pkcs8` the private key format is PKCS#8 PEM and looks like this:
ssh-keygen -b 4096 -t rsa -m pkcs8 -C "email@example.com" -f ./private_key_name
'
-----BEGIN PRIVATE KEY-----
{content}
-----END PRIVATE KEY-----
'

# Rename the public key
mv ./private_key_name.pub ./public_key_name.pub.old

# The public key will have name ./private_key_name.pub
# The default public key format is OpenSSH specific and looks like this:
'
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCgGyZzI66sdvojQDoxo+FPbNmJ3lW9asZxJBi8j4Iq+NB9CeZYRHGX2kMQtnFqfhuzDhNvarScybnZqR4QBcio4508pLGz6TYCMeGdoVB7tweJ5m7lAWjPvIW3alSv8pEjvnFXbLUVckwmHRJHUYSGE3WmBYG9cGoe/cK1f9T4ddr1EbVRr+DE+Dlguw5P+pBB9Ss01G0bW34nMJoi4/aqj24ylahb/7ia0BdajGYrFmz1eubbSA/Shtpw0WFTxwmCetEgMv1Jjy+aWoMiJd8+JjYYLJKcr8ZECsnLB0pM3iZVF56Y80eEdS3S/e/qjD7dAyj+gaobPUbGWcarQqlJIXTK8rOQMsw0BXkhTJG50bm3HiqIjkLS3zclRNZSR0YNx/oiFblleFumLPUoA4OxrvnSwtQ7ki3il2tyQD2ah8Qdo2IKz3WkBNcAMnhWC3250R897o31EENVZdtt0lk3d2z+wzU05KUjDnsoFEK706HWSBDJd1GpkDoK9j/1PzemWcT5/IEACA+vEKZhlRBeacIGl8kom96RZQfF9mJoBRcmDHyteUKpiOUBAdTPPsFjvhS9//PI+/fnMCHHBs4Jr3YVWk5oh3fwZSAugtkbly/dlg4ihTwWLsNPr9qVPPCOhQSXZGaGvEiBSoqppq7PSUIudIFGPji/BD9KVJiUeQ== email@example.com
'
# To convert it to another format use the following commands (by default the `-e` option outputs the result to stdout so
# to store to a file use any one of the second commands in the set):
# With no format option (default) or with `-m rfc4716` the public key format is RFC4716 and looks like this:
ssh-keygen -e -f ./public_key_name.pub.old
ssh-keygen -e -f ./public_key_name.pub.old > ./public_key_name.pub
'
---- BEGIN SSH2 PUBLIC KEY ----
Comment: "4096-bit RSA, converted by Milen@DESKTOP-0807983 from OpenSS"
{content}}
---- END SSH2 PUBLIC KEY ----
'
# With `-m pem` the public key format is PKCS#1 PEM and looks like this:
ssh-keygen -e -m pem -f ./public_key_name.pub.old
ssh-keygen -e -m pem -f ./public_key_name.pub.old > ./public_key_name.pub
'
-----BEGIN RSA PUBLIC KEY-----
{content}
-----END RSA PUBLIC KEY-----
'
# With `-m pkcs8` the public key format is PKCS#8 PEM and looks like this:
ssh-keygen -e -m pkcs8 -f ./public_key_name.pub.old
ssh-keygen -e -m pkcs8 -f ./public_key_name.pub.old > ./public_key_name.pub
'
-----BEGIN PRIVATE KEY-----
{content}
-----END PRIVATE KEY-----
'
