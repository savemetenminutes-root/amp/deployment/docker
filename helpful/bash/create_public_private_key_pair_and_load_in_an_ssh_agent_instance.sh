#!/usr/bin/env bash

# Generate public/private key
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
# Create a new ssh-agent instance (available only in the current shell)
eval $(ssh-agent -s)
# Add the private key to the agent
ssh-add ~/.ssh/id_rsa
