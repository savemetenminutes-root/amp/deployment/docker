# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
source .git-prompt.sh
source .git-completion.sh

# the next line uses tput from the ncurses package
PS1='\u@\[`[ $? = 0 ] && X=2 || X=1; tput setaf $X`\]\h\[`tput sgr0`\]('$(uname -r)'):$PWD$(__git_ps1 " (%s)")\n\$ '
