#!/usr/bin/env bash

cd directory && git ls-files -z | xargs -0 git update-index --assume-unchanged && cd ..
cd directory && git ls-files -z | xargs -0 git update-index --no-assume-unchanged && cd ..
