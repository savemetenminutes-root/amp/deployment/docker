#!/bin/bash

git for-each-ref --sort=-committerdate --format='%(objectname) %(objecttype) %(refname) %(committerdate:iso8601)'
