#!/usr/bin/env bash

# Example:
# test_connectivity_to_host_port.sh 127.0.0.1 80

nc -z -v -w5 $1 $2
