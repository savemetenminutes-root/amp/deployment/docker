#!/usr/bin/env bash

# List files to be installed by a package
apt-get install apt-file
apt-file update
apt-file list $package

# To find which package provides a file that is not currently on your system
apt-file search /path/to/file

# List installed package files
dpkg -L $package

# ...or if you have the package as a .deb file locally already
dpkg --contents $package.deb

# To find which package provides a file that is already on your system
dpkg -S /path/to/file
