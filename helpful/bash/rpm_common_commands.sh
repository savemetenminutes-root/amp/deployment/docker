#!/usr/bin/env bash

# Install multiple packages replacing all files and packages, allowing old packages
rpm -ivh \
    --replacepkgs \
    --oldpackage \
    --replacefiles \
    --force \
    gcc-c%2B%2B-4.5-19.1.x86_64.rpm \
    gcc45-c%2B%2B-4.5.1_20101208-9.8.x86_64.rpm \
    libstdc%2B%2B45-4.5.1_20101208-9.8.x86_64.rpm \
    libstdc%2B%2B45-devel-4.5.1_20101208-9.8.x86_64.rpm
