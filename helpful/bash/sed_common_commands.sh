#!/usr/bin/env bash

# Useful options:
#
# -r, --regexp-extended
# use extended regular expressions in the script.
#
# -i[SUFFIX], --in-place[=SUFFIX]
# edit files in place (makes backup if extension supplied). The default operation mode is to break symbolic and hard links. This can be changed with --follow-symlinks and --copy.

# In place edit the /init/init-project-users-databases-plain.sql file to remove all lines starting with a hashtag
sed -i -r 's/#.*//' /init/init-project-users-databases-plain.sql
# In place edit the /etc/fstab file to remove lines containing /install
sed -i 'N;/.*\/install/{s#.*/install.*\n##}' /etc/fstab;
# Remove from services: to the end of the docker-compose.yml.template file and output the result (the file remains unchanged)
sed '/services:/,/\$/ d' docker-compose.yml.template
# In place edit the /init/init-project-users-databases-plain.sql file to remove all blank lines
sed -i -r '/^[[:space:]]*$/d; /^[[:space:]]*$/d; :a;$!{N;s/\n//;ba;}; s/;/;\n/g' /init/init-project-users-databases-plain.sql
