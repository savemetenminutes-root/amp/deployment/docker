<?php

echo
hash(
    'sha512/256',
    password_hash(
        uniqid()
        . '_'
        . 'some other unique string like email for example'
        . '_'
        . time(),
        PASSWORD_DEFAULT
    )
);
