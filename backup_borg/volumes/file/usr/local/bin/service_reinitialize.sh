#!/bin/bash

mkdir -p /borgbackup/mounts/mysql/dump/database/
mkdir -p /borgbackup/repos/mysql/dump/database/coreapi/
borg init --encryption=repokey /borgbackup/repos/mysql/dump/database/coreapi/

cat > /etc/crontab << EOL
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the \`crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

#   m       h   dom     mon     dow     user    command
    17      *   *       *       *       root    cd / && run-parts --report /etc/cron.hourly
    25      6   *       *       *       root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
    47      6   *       *       7       root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
    52      6   1       *       *       root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
    *       *   *       *       *       root    export COREAPI_MYSQL_PASSWORD=${COREAPI_MYSQL_PASSWORD} && export BORG_PASSPHRASE=${BORG_PASSPHRASE} && backup_database_coreapi.sh >/proc/1/fd/1 2>&1
#
EOL