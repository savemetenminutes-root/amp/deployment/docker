#!/bin/bash

docker stats \
    default-telegraf \
    default-influxdb \
    default-chronograf \
    default-kapacitor \
