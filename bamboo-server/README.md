### Bamboo Server

Before being able to connect Bamboo agents to the Bamboo Server an installation routine must be run on the server by navigating to the Bamboo Server web interface and following the required steps. After the Bamboo Agent connects it will need to be permitted access to the Bamboo Server. [https://confluence.atlassian.com/bamboo/agent-authentication-289277196.html](https://confluence.atlassian.com/bamboo/agent-authentication-289277196.html) 
