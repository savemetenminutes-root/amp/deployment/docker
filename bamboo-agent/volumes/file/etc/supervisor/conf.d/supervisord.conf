[unix_http_server]
# On Debian/Ubuntu this is the default sock path which supervisorctl looks up. By providing exactly this value,
# supervisorctl can be used without specifying -c/--configuration (where a [supervisorctl] section with an appropriate
# serverurl= directive should be present) or -s/--serverurl. This path varies across different supervisor builds on
# different Linux distros.
file=/var/run/supervisor.sock

[inet_http_server]
port=0.0.0.0:9001

[rpcinterface:supervisor]
supervisor.rpcinterface_factory=supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///var/run/supervisor.sock

[supervisord]
nodaemon=true
logfile=/var/log/supervisor/supervisord.log
pidfile=/var/run/supervisord.pid



[program:rsyslogd]
# command lists "; && ||" are not supported in sh
command=bash -c 'rm -f /var/run/rsyslogd.pid -- && rsyslogd -n'
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true

[program:sshd]
command=/usr/sbin/sshd -D
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true

[program:bamboo-agent]
user=bamboo
directory=/home/bamboo
# On the first run the runAgent.sh script starts the agent and it creates a configuration file at
# /home/bamboo/bamboo-agent-home/conf/wrapper.conf
# which, among other things, contains the URL to the bamboo server and it will retain its value across subsequent runs
# even if another URL is passed as argument to the runAgent.sh script. To enable the possibility of changing the bamboo
# server URL at run-time, the generated wrapper.conf file must be erased to force its regeneration.
command=bash -c 'rm -rf /home/bamboo/bamboo-agent-home/* && /home/bamboo/runAgent.sh "http://%(ENV_COMPOSE_PROJECT_NAME)s-bamboo-server:8085"'
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true
