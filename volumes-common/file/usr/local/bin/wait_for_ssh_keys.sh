#!/bin/bash

SERVICE_NAME=""
CHECK_PORT=""
ADD_SUFFIX=""
OPTIND=1
while getopts ":s:p:a" o; do
    case "${o}" in
        s)
            SERVICE_NAME=${OPTARG}
            ;;
        p)
            CHECK_PORT=${OPTARG}
            ;;
        a)
            ADD_SUFFIX="true"
            ;;
    esac
done
shift $((OPTIND-1))

if [[ -z ${SERVICE_NAME} ]]; then
    exit 1
fi

HOST="${COMPOSE_PROJECT_NAME:-helden}-${SERVICE_NAME}"
# Using pseudo-device files: http://www.tldp.org/LDP/abs/html/devref1.html
#</dev/tcp/${HOST}/${CHECK_PORT}
#echo $?
if [[ "${CHECK_PORT}" = "" ]] || ! nc -z ${HOST} ${CHECK_PORT}; then
    while [[ ! -f /root/.ssh/${SERVICE_NAME}/id_rsa || ! -f /root/.ssh/${SERVICE_NAME}/id_rsa.pub || ! -f /root/.ssh/${SERVICE_NAME}/ssh_host_rsa_key.pub || ! -f /root/.ssh/${SERVICE_NAME}/ssh_host_dsa_key.pub || ! -f /root/.ssh/${SERVICE_NAME}/ssh_host_ecdsa_key.pub || ! -f /root/.ssh/${SERVICE_NAME}/ssh_host_ed25519_key.pub ]]; do
        #echo "Waiting for service ${SERVICE_NAME} to generate its SSH keys..."
        sleep 1
    done
fi

if [[ -f /root/.ssh/${SERVICE_NAME}/id_rsa && -f /root/.ssh/${SERVICE_NAME}/id_rsa.pub && -f /root/.ssh/${SERVICE_NAME}/ssh_host_rsa_key.pub && -f /root/.ssh/${SERVICE_NAME}/ssh_host_dsa_key.pub && -f /root/.ssh/${SERVICE_NAME}/ssh_host_ecdsa_key.pub && -f /root/.ssh/${SERVICE_NAME}/ssh_host_ed25519_key.pub ]]; then
    if [[ -z ${ADD_SUFFIX} ]]; then
        mv -f /root/.ssh/${SERVICE_NAME}/id_rsa /root/.ssh/id_rsa
        chmod 0600 /root/.ssh/id_rsa
        mv -f /root/.ssh/${SERVICE_NAME}/id_rsa.pub /root/.ssh/id_rsa.pub
    else
        mv -f /root/.ssh/${SERVICE_NAME}/id_rsa /root/.ssh/id_rsa_${SERVICE_NAME}
        chmod 0600 /root/.ssh/id_rsa_${SERVICE_NAME}
        mv -f /root/.ssh/${SERVICE_NAME}/id_rsa.pub /root/.ssh/id_rsa_${SERVICE_NAME}.pub
    fi
    # Remove all entries matching this host from the known_hosts and remove the auto-generated backup
    ssh-keygen -R "${COMPOSE_PROJECT_NAME:-default}-${SERVICE_NAME}"
    rm -f /root/.ssh/known_hosts.old
    #ssh-keyscan -H ${COMPOSE_PROJECT_NAME:-default}-php73-fpm > ~/.ssh/known_hosts
    printf "${COMPOSE_PROJECT_NAME:-default}-${SERVICE_NAME} $(cat /root/.ssh/${SERVICE_NAME}/ssh_host_rsa_key.pub)"$'\n'"${COMPOSE_PROJECT_NAME:-default}-${SERVICE_NAME} $(cat /root/.ssh/${SERVICE_NAME}/ssh_host_dsa_key.pub)"$'\n'"${COMPOSE_PROJECT_NAME:-default}-${SERVICE_NAME} $(cat /root/.ssh/${SERVICE_NAME}/ssh_host_ecdsa_key.pub)"$'\n'"${COMPOSE_PROJECT_NAME:-default}-${SERVICE_NAME} $(cat /root/.ssh/${SERVICE_NAME}/ssh_host_ed25519_key.pub)"$'\n' >> /root/.ssh/known_hosts
    rm -f /root/.ssh/${SERVICE_NAME}/ssh_host_rsa_key.pub
    rm -f /root/.ssh/${SERVICE_NAME}/ssh_host_dsa_key.pub
    rm -f /root/.ssh/${SERVICE_NAME}/ssh_host_ecdsa_key.pub
    rm -f /root/.ssh/${SERVICE_NAME}/ssh_host_ed25519_key.pub

    # Hash the known_hosts file and delete the auto-created backup
    ssh-keygen -H -f /root/.ssh/known_hosts
    rm -f /root/.ssh/known_hosts.old
fi
