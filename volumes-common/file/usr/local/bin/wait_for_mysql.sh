#!/bin/bash

# Wait for the MySQL service to be fully up and running
echo "Waiting for: Host ${COMPOSE_PROJECT_NAME:-default}-mysql to start the service on port 3306"
while ! /usr/local/bin/mysqladmin ping --host="${COMPOSE_PROJECT_NAME:-default}-mysql" --port=3306 --silent; do
    #echo 'Waiting for MySQL to start...'
    sleep 1
done
echo "Done waiting for: Host ${COMPOSE_PROJECT_NAME:-default}-mysql to start the service on port 3306"
