#!/bin/bash

export DISPLAY=':99'
export XAUTHORITY='/tmp/xvfb-auth.cookie'
export SCREEN_GEOM='1280x960x16'

Xvfb ${DISPLAY} -auth ${XAUTHORITY} -screen 0 ${SCREEN_GEOM} > /dev/null 2>&1 &
# Wait for Xvfb
MAX_ATTEMPTS=120 # About 60 seconds
COUNT=0
#echo -n "Waiting for Xvfb to be ready..."
while ! xdpyinfo -display ${DISPLAY} >/dev/null 2>&1; do
  #echo -n "."
  sleep 0.50s
  COUNT=$(( COUNT + 1 ))
  if [ "${COUNT}" -ge "${MAX_ATTEMPTS}" ]; then
    #echo "  Gave up waiting for X server on ${DISPLAY}"
    exit 1
  fi
done
#echo "  Done - Xvfb is ready!"
/usr/bin/fluxbox -display ${DISPLAY} > /dev/null 2>&1 &
echo "DISPLAY=${DISPLAY}"
echo "XAUTHORITY=${XAUTHORITY}"