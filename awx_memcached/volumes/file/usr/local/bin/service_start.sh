#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
# or most of the time the line below is enough (if not a symlink)
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )";

rm -f /run/supervisord.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /run/supervisord.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
