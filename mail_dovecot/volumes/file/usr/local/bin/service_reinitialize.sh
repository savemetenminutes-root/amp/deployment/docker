#!/bin/bash

# retarded dovecot... telling me what i can run with root and what i HAVE to run as the dovecot user... still not as retarded as docker for windows mounting everything as root/0755... gotta jump through hoops to be able to persist the mail on the host while at the same time keep the dumb dovecot permission error free
rsync -avz /mail_storage/* /home/vmail/
chown -R vmail:vmail /home/vmail
chmod -R 0755 /home/vmail

. wait_for_mariadb.sh
. wait_for_mysql.sh

/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS '${MAILSERVER_DOVECOT_MYSQL_USERNAME}'@'%' IDENTIFIED BY '${MAILSERVER_DOVECOT_MYSQL_PASSWORD}';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS ${MAILSERVER_DOVECOT_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.* TO '${MAILSERVER_DOVECOT_MYSQL_USERNAME}'@'%';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "
CREATE TABLE IF NOT EXISTS ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users (
	id int(11) NOT NULL auto_increment,
	userid VARCHAR(128) NOT NULL,
	domain VARCHAR(128) NOT NULL,
	password VARCHAR(255) NOT NULL,
	home VARCHAR(255),
	uid INTEGER,
	gid INTEGER,
	PRIMARY KEY (id)
);
INSERT INTO ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users
  (id, userid, domain, password)
VALUES
  ('1', 'admin',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('2', 'sales',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('3', 'support',                  'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('4', 'no_reply',                 'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('5', 'help',                     'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('6', 'test0',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('7', 'test1',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('8', 'test2',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('9', 'test3',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('10', 'test4',                   'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('11', 'test5',                   'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('12', 'a',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('13', 'b',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('14', 'c',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('15', 'd',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('16', 'e',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('17', 'f',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))))
ON DUPLICATE KEY UPDATE id=id, userid=userid, domain=domain, password=password;
UPDATE ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users
SET
  home=CONCAT('/home/vmail/', domain, '/', userid),
  uid=103,
  gid=104
;
" "${MAILSERVER_DOVECOT_MYSQL_DATABASE}"

/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS '${MAILSERVER_DOVECOT_MYSQL_USERNAME}'@'%' IDENTIFIED BY '${MAILSERVER_DOVECOT_MYSQL_PASSWORD}';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS ${MAILSERVER_DOVECOT_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.* TO '${MAILSERVER_DOVECOT_MYSQL_USERNAME}'@'%';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "
CREATE TABLE IF NOT EXISTS ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users (
	id int(11) NOT NULL auto_increment,
	userid VARCHAR(128) NOT NULL,
	domain VARCHAR(128) NOT NULL,
	password VARCHAR(255) NOT NULL,
	home VARCHAR(255),
	uid INTEGER,
	gid INTEGER,
	PRIMARY KEY (id)
);
INSERT INTO ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users
  (id, userid, domain, password)
VALUES
  ('1', 'admin',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('2', 'sales',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('3', 'support',                  'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('4', 'no_reply',                 'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('5', 'help',                     'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('6', 'test0',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('7', 'test1',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('8', 'test2',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('9', 'test3',                    'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('10', 'test4',                   'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('11', 'test5',                   'savemetenminutes.com', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('12', 'a',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('13', 'b',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('14', 'c',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('15', 'd',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('16', 'e',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16)))),
  ('17', 'f',                       'a.a',                  ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))))
ON DUPLICATE KEY UPDATE id=id, userid=userid, domain=domain, password=password;
UPDATE ${MAILSERVER_DOVECOT_MYSQL_DATABASE}.users
SET
  home=CONCAT('/home/vmail/', domain, '/', userid),
  uid=103,
  gid=104
;
" "${MAILSERVER_DOVECOT_MYSQL_DATABASE}"

rm -f /var/run/dovecot/master.pid

cat > /etc/dovecot/dovecot-sql.conf << EOL
driver = mysql

# The mysqld.sock socket may be in different locations in different systems.
# Use "host= ... pass=foo#bar" with double-quotes if your password has '#' character.
# If you need SSL connection, you can add ssl_ca or ssl_ca_path
# You can also use ssl_cert/ssl_key, ssl_cipher, ssl_verify_server_cert
# or provide option_file and option_group

# Alternatively you can connect to localhost as well:
#connect = host=localhost dbname=mails user=admin password=pass # port=3306
connect = host=${COMPOSE_PROJECT_NAME:-default}-mysql dbname=${MAILSERVER_DOVECOT_MYSQL_DATABASE} user=${MAILSERVER_DOVECOT_MYSQL_USERNAME} password=${MAILSERVER_DOVECOT_MYSQL_PASSWORD}



#password_query = SELECT userid AS username, domain, password \
#  FROM users WHERE userid = '%n' AND domain = '%d'

default_pass_scheme = PLAIN
#default_pass_scheme = SHA512-CRYPT


password_query = SELECT '%w' as password, '%u' AS user \
  FROM users WHERE userid = '%n' AND domain = '%d' AND password = ENCRYPT('%w',password)


user_query = SELECT home, uid, gid FROM users WHERE userid = '%n' AND domain = '%d'


# For using doveadm -A:
iterate_query = SELECT userid AS username, domain FROM users
EOL

sed -i 's/@commonName@/'"${MAILSERVER_DEFAULT_DOMAIN}"'/' /usr/share/dovecot/dovecot-openssl.cnf
prevDir=$(pwd)
cd /usr/share/dovecot/ && /usr/share/dovecot/mkcert.sh && cd ${prevDir}
