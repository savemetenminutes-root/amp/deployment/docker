FROM openjdk:14-ea-jdk-alpine

# The cron package is available on Alpine Linux under the busybox package which is already installed by default
RUN apk --update add --no-cache supervisor rsyslog && mkdir -p /var/log/supervisor && mkdir -p /var/spool/rsyslog

# Enable SSH: ##################################################################
ARG LINUX_ROOT_PASSWORD
ENV LINUX_ROOT_PASSWORD ${LINUX_ROOT_PASSWORD}

RUN apk update && apk add openssh pwgen && \
    mkdir -p /var/run/sshd && \
    if [ ! -z "${LINUX_ROOT_PASSWORD}" ]; then echo "root:${LINUX_ROOT_PASSWORD}" | chpasswd; else export LINUX_ROOT_PASSWORD="$(pwgen -1 32)" && printf "${LINUX_ROOT_PASSWORD}\n" >> /.env && echo "root:${LINUX_ROOT_PASSWORD}" | chpasswd; fi && \
    sed -i -r 's/^#?PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?PermitEmptyPasswords.*/PermitEmptyPasswords yes/' /etc/ssh/sshd_config && \
    sed -i -r 's:^#?AuthorizedKeysFile.*:AuthorizedKeysFile %h/.ssh/authorized_keys:' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?GatewayPorts.*/GatewayPorts yes/' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?PermitTunnel.*/PermitTunnel yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
#RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN rm -f /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa && \
    rm -f /etc/ssh/ssh_host_dsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa && \
    rm -f /etc/ssh/ssh_host_ecdsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa && \
    rm -f /etc/ssh/ssh_host_ed25519_key && \
    ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
# END: enable SSH ###################################################################################################

RUN apk --update add --no-cache bash openrc
