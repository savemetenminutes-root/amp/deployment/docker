#!/bin/bash

docker stats \
    default-centos7 \
    default-debian9_stretch \
    default-debian10_buster \
    default-ubuntu18_cosmic \
    default-ubuntu19_eoan \
    default-alpine3 \
    default-oraclelinux7 \
    default-oraclelinux8 \
