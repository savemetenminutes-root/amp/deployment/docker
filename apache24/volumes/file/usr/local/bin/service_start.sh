#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )";

rm -f /var/run/supervisor.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /var/run/supervisor.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
