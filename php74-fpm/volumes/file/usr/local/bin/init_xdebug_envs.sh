#!/bin/bash

export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_port=9900 remote_host=127.0.0.1 remote_connect_back=0"
export PHP_IDE_CONFIG="serverName=127.0.0.1"
export COMPOSER_ALLOW_XDEBUG=1
