#!/bin/bash

echo '
  ______                                 __       __         ________                    __       __  __                        __
 /      \                               |  \     /  \       |        \                  |  \     /  \|  \                      |  \
|  €€€€€€\  ______  __     __   ______  | €€\   /  €€  ______\€€€€€€€€______   _______  | €€\   /  €€ \€€ _______   __    __  _| €€_     ______    _______
| €€___\€€ |      \|  \   /  \ /      \ | €€€\ /  €€€ /      \ | €€  /      \ |       \ | €€€\ /  €€€|  \|       \ |  \  |  \|   €€ \   /      \  /       \
 \€€    \   \€€€€€€\\€€\ /  €€|  €€€€€€\| €€€€\  €€€€|  €€€€€€\| €€ |  €€€€€€\| €€€€€€€\| €€€€\  €€€€| €€| €€€€€€€\| €€  | €€ \€€€€€€  |  €€€€€€\|  €€€€€€€
 _\€€€€€€\ /      €€ \€€\  €€ | €€    €€| €€\€€ €€ €€| €€    €€| €€ | €€    €€| €€  | €€| €€\€€ €€ €€| €€| €€  | €€| €€  | €€  | €€ __ | €€    €€ \€€    \
|  \__| €€|  €€€€€€€  \€€ €€  | €€€€€€€€| €€ \€€€| €€| €€€€€€€€| €€ | €€€€€€€€| €€  | €€| €€ \€€€| €€| €€| €€  | €€| €€__/ €€  | €€|  \| €€€€€€€€ _\€€€€€€\
 \€€    €€ \€€    €€   \€€€    \€€     \| €€  \€ | €€ \€€     \| €€  \€€     \| €€  | €€| €€  \€ | €€| €€| €€  | €€ \€€    €€   \€€  €€ \€€     \|       €€
  \€€€€€€   \€€€€€€€    \€      \€€€€€€€ \€€      \€€  \€€€€€€€ \€€   \€€€€€€€ \€€   \€€ \€€      \€€ \€€ \€€   \€€  \€€€€€€     \€€€€   \€€€€€€€ \€€€€€€€

                                          ------------------------------------------------------------------
                                                      PROJECT START-UP | DOCKER-COMPOSE WRAPPER
                                                                     TOOOL
                                          We pass command-line arguments to docker-compose for you! (mostly)
                                          ------------------------------------------------------------------

';

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"

usage() {
    echo "Usage: $0 [[-q <development|staging|production>]|[-f <DOCKER_COMPOSE_YML_FILE>...]] [DOCKER_COMPOSE_OPTIONS] [DOCKER_COMPOSE_COMMAND] [DOCKER_COMPOSE_ARGS...]" 1>&2;
    exit 0;
}

gitAccessServices=( "php73-fpm" )
bashHistoryServices=( "alpine3" "ansible" "apache24" "backup_borg" "beats_filebeat" "beats_metricbeat" "cassandra" "centos7" "chronograf" "cron" "debian9_stretch" "debian10_buster" "elasticsearch" "fluxbox-vnc" "grafana" "haproxy" "influxdb" "jsdetox" "kafka" "kapacitor" "kibana" "logstash" "mail_dovecot" "mail_postfix" "mongo" "mysql" "nginx-unit1" "nodejs11" "openapi-gui" "openapigenerator" "openjdk14" "opensuse_leap" "opensuse_tumbleweed" "php73-fpm" "postgres" "python3" "rabbitmq" "redis" "redmine" "sftp" "sonarqube" "telegraf" "test" "tomcat" "traefik" "ubuntu18_cosmic" "ubuntu19_eoan" "xwiki" )

HEHE_ENVIRONMENT="development"
while getopts ":q:" o; do
    case "${o}" in
        q)
            HEHE_ENVIRONMENT=${OPTARG}
            ;;
    esac
done
shift $((OPTIND-1))

if [[ ${HEHE_ENVIRONMENT} != "development" && ${HEHE_ENVIRONMENT} != "staging" && ${HEHE_ENVIRONMENT} != "production" ]]; then
    echo "Wrong value for [-q]. Possible values are development (default), staging, production."
    echo
    usage
    exit 1
fi

# This creates the docker.compose.*.yml files from the templates.
# The first command removes everything from "services:" (including) to the end of the template file.
# The second command inserts the "services:\n" back.
# The third command joins all the *.yml occurrences in the template with the corresponding file contents
# The output of the three commands is concatenated and stored (overwritten) in a corresponding docker-compose.*.yml file
# The whole thing is repeated (this is one DRY fuck...) for each of the three templates.
# The next line is needed otherwise on Windows `grep -P` fails with the following error:
# "grep: -P supports only unibyte and UTF-8 locales"
# causing the compiled docker-compose files to have empty "services:" contents.
export LC_CTYPE="C.UTF-8"
{ \
    sed '/services:/,/\$/ d' docker-compose.yml.template; \
    echo 'services:'$'\n'; \
    #sed -r 's/^(?<=#).+\.yml$//' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
    grep -P '^(?!#).*\.yml$' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
} | cat > docker-compose.yml
{ \
    sed '/services:/,/\$/ d' docker-compose.staging.yml.template; \
    echo 'services:'$'\n'; \
    #sed -r 's/^(?<=#).+\.yml$//' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
    grep -P '^(?!#).*\.yml$' docker-compose.staging.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
} | cat > docker-compose.staging.yml
{ \
    sed '/services:/,/\$/ d' docker-compose.production.yml.template; \
    echo 'services:'$'\n'; \
    #sed -r 's/^(?<=#).+\.yml$//' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
    grep -P '^(?!#).*\.yml$' docker-compose.production.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
} | cat > docker-compose.production.yml

keysDir="$HOME/.ssh";
systemDrive='c'
if [ ! -z "${SYSTEMDRIVE}" ]; then
    systemDrive="$(eval echo ${SYSTEMDRIVE%?} | tr '[:upper:]' '[:lower:]')"
fi
#keysDir="/${systemDrive}/Users/$(whoami)/.ssh"; # Git Bash
if [[ "${HOME}" = "/home/mobaxterm" ]]; then
    keysDir="/cygdrive/${systemDrive}/Users/$(whoami)/.ssh"; # Cygwin
    if [ ! -d "${keysDir}" ]; then
        keysDir="/drives/${systemDrive}/Users/$(whoami)/.ssh"; # MobaXterm Bash
    fi
fi
echo "Using ${keysDir} for keys directory..."
if [[ ! -f "${keysDir}/id_rsa" || ! -f "${keysDir}/id_rsa.pub" ]]; then
    if [ ! -f "${keysDir}/id_rsa" ]; then
        echo "Error: Could not find the current user's private key.";
    fi
    if [ ! -f "${keysDir}/id_rsa.pub" ]; then
        echo "Error: Could not find the current user's public key.";
    fi
    echo 'Error: Some containers will not be able to run `composer install` due to private repo dependencies. Exiting...';
    exit 1;
else
    for ((j=0;j<${#gitAccessServices[@]};j++)); do
        mkdir -p "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}";
        rm -f "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}/id_rsa";
        rm -f "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}/id_rsa.pub";
        cp -f "${keysDir}/id_rsa" "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}/";
        cp -f "${keysDir}/id_rsa.pub" "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}/";
        chmod 0600 "${DIR}/${gitAccessServices[${j}]}/volumes-shared/directory/.ssh/git/${gitAccessServices[${j}]}/id_rsa";

        #eval $(ssh-agent -s);
        #ssh-add ~/.ssh/id_rsa;
    done
fi

echo 'Checking the hosts file for some required values...';
declare -A hostsEntries;
hostsEntries["0:url"]="d.default.com";
hostsEntries["0:target"]="127.0.0.1";
hostsEntries["1:url"]="www.google-analytics.com";
hostsEntries["1:target"]="127.0.0.1";
hostsEntries["2:url"]="google-analytics.com";
hostsEntries["2:target"]="127.0.0.1";
hostsEntries["3:url"]="ssl.google-analytics.com";
hostsEntries["3:target"]="127.0.0.1";
lineSeparator=$'\n';
if [[ $(uname -a | grep "^CYGWIN") || $(uname -a | grep "^MINGW64") ]]; then
    lineSeparator=$'\r\n';
fi
hostsFileDir='/etc';
hostsFilePath='/etc/hosts';
if [ -f "/${systemDrive}/Windows/system32/drivers/etc/hosts" ]; then # Git Bash
    hostsFileDir="/${systemDrive}/Windows/system32/drivers/etc";
    hostsFilePath="${hostsFileDir}/hosts";
else
    if [ -f "/cygdrive/${systemDrive}/Windows/system32/drivers/etc/hosts" ]; then # Cygwin
        hostsFileDir="/cygdrive/${systemDrive}/Windows/system32/drivers/etc";
        hostsFilePath="${hostsFileDir}/hosts";
    else
        if [ -f "/drives/${systemDrive}/Windows/system32/drivers/etc/hosts" ]; then # MobaXterm Bash
            hostsFileDir="/drives/${systemDrive}/Windows/system32/drivers/etc";
            hostsFilePath="${hostsFileDir}/hosts";
        fi
    fi
fi
hostsFileWritable=false
if [[ ! -p ${hostsFilePath} ]]; then
    ( [ -e ${hostsFilePath} ] && >> ${hostsFilePath} ) 2> /dev/null && hostsFileWritable=true || hostsFileWritable=false;
    if [[ ! -w ${hostsFilePath} ]]; then
        hostsFileWritable=false;
    fi
fi
hostsFileEncoding=false
iconvEncodingExists=false
if [[ $(which file) && $(which iconv) ]]; then
    hostsFileEncoding=$( file --brief --mime-encoding ${hostsFilePath} );
    if [[ $(iconv -l | grep -i ${hostsFileEncoding}) ]]; then
        iconvEncodingExists=true
    fi
fi
if [[ ! -f ${hostsFilePath} || ! -r ${hostsFilePath} || -p ${hostsFilePath} || "$hostsFileWritable" = false || ! $(which file) || ! $(which iconv) || "${iconvEncodingExists}" = false ]]; then
    if [[ ! -f ${hostsFilePath} ]]; then
        echo 'Warning: Could not find your hosts file at '${hostsFilePath}'.';
    else
        if [[ ! -r ${hostsFilePath} ]]; then
            echo 'Warning: The hosts file at '${hostsFilePath}' is not readable.';
        fi
        if [[ -p ${hostsFilePath} ]]; then
            echo 'Warning: The hosts file at '${hostsFilePath}' is a pipe.';
        fi
        if [[ "$hostsFileWritable" = false ]]; then
            echo 'Warning: The hosts file at '${hostsFilePath}' is not writable.';
        fi
        if [[ ! -r ${hostsFilePath} || "$hostsFileWritable" = false ]]; then
            echo '         You can try running the script with root/administrator privileges.';
        fi
    fi
    if [[ ! $(which file) || ! $(which iconv) ]]; then
        if [[ ! $(which file) ]]; then
            echo 'Warning: Cannot find the `file` utility!';
        fi
        if [[ ! $(which iconv) ]]; then
            echo 'Warning: Cannot find the `iconv` utility!';
        fi
    else
        if [[ "${iconvEncodingExists}" = false ]]; then
            echo 'Warning: Cannot convert your hosts file encoding to something the current shell can work with!';
        fi
    fi
    echo '         You could try to fix the warnings and re-run the script.';
    echo '         The script will not be able to populate your hosts file automatically.';
    echo '         Make sure that the following entries exist in your hosts file:';
    echo '         ------------------------- '${hostsFilePath}' -------------------------';
    echo '         ...';
    for ((j=0;j<$((${#hostsEntries[@]} / 2));j++)); do
    echo '         '${hostsEntries["${j}:target"]}'    '${hostsEntries["${j}:url"]};
    done;
    echo '         ...';
    echo '         --------------------------------------------------------------';
else
    echo 'Current hosts file encoding: '${hostsFileEncoding};
    hostsFileContent="$(iconv -f ${hostsFileEncoding} -t UTF-8 ${hostsFilePath})"; # Fixes problems with encoding when writing the content back to file on Windows
    hostsEntriesAdd=();
    j=0;
    k=0;
    for ((j=0;j<$((${#hostsEntries[@]} / 2));j++)); do
        target=${hostsEntries["${j}:target"]};
        url=${hostsEntries["${j}:url"]};
        if [[ ! $(echo "${hostsFileContent}" | grep -E "${target//\./\\.}[[:space:]]+${url//./\\.}") ]]; then
            echo "Adding entry: ${hostsEntries["${j}:target"]}"    "${hostsEntries["${j}:url"]}";
            hostsEntriesAdd[${j}]="${lineSeparator}${hostsEntries["${j}:target"]}    ${hostsEntries["${j}:url"]}";
            ((k++));
        fi
    done;
    hostsFileContentAdd="";
    if (( "${#hostsEntriesAdd[@]}" )); then
        hostsFileContent="${hostsFileContent}${lineSeparator}# SaveMeTenMinutes DevStack"
        hostsFileContentAdd="${lineSeparator}# SaveMeTenMinutes DevStack"
        for ((j=0;j<${#hostsEntriesAdd[@]};j++)); do
            hostsFileContent="${hostsFileContent}${hostsEntriesAdd[${j}]}";
            hostsFileContentAdd="${hostsFileContentAdd}${hostsEntriesAdd[${j}]}";
        done
        hostsFileContent="${hostsFileContent}${lineSeparator}# END: SaveMeTenMinutes DevStack${lineSeparator}";
        hostsFileContentAdd="${hostsFileContentAdd}${lineSeparator}# END: SaveMeTenMinutes DevStack${lineSeparator}";
        hostsEntriesAdd=("${lineSeparator}# SaveMeTenMinutes DevStack" "${hostsEntriesAdd[@]}");
        hostsEntriesAdd=("${hostsEntriesAdd[@]}" "${lineSeparator}# END: SaveMeTenMinutes DevStack${lineSeparator}");
        echo 'Writing hosts file converted to UTF-8 encoding...';
        echo "${hostsFileContent}" > ${hostsFilePath};
    fi
fi

for ((j=0;j<${#bashHistoryServices[@]};j++)); do
    touch "${DIR}/${bashHistoryServices[${j}]}/volumes/file/root/.bash_history";
done

#if [[ $(which git) ]]; then
#    currentDir=$(pwd)
#    cd "${DIR}/webapp/content/"
#    git update-index --assume-unchanged config/environment.js
#    cd "${currentDir}"
#else
#    echo 'Warning: Cannot find `git`! `git update-index --assume-unchanged config/environment.js` needs to be executed manually on the webapp to prevent the docker-specific config from being staged.';
#fi

command="docker-compose $@";
if [[ "${HEHE_ENVIRONMENT}" = "staging" ]]; then
    command="docker-compose -f docker-compose.staging.yml $@";
else
    if [[ "${HEHE_ENVIRONMENT}" = "production" ]]; then
        command="docker-compose -f docker-compose.production.yml $@";
    fi
fi
export HEHE_ENVIRONMENT
#echo "Executing `${command}`..."; # APPARENTLY BACKTICKS WITHIN DOUBLE QUOTES RUN THE COMMAND AND FOR SOME REASON ONLY THE STDERR ("Starting <container> ... done" messages) OF docker-compose IS BEING OUTPUT. STDOUT WHICH PRINTS THE CONTAINER LOGS GETS LOST WHEN THE EXECUTED COMMAND IS PASSED AS AN ARGUMENT TO ECHO :S
echo 'Executing `'${command}'`...'; # ...SO SINGLE QUOTES NEED TO BE USED TO ECHO THE MESSAGE PROPERLY AND EVAL THE COMMAND WITH THE LINE BELOW
eval ${command}; 
