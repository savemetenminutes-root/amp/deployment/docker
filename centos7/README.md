### Install x11vnc and xvfb on RHEL7
```
curl -Lo epel-release-latest-7.noarch.rpm https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y epel-release-latest-7.noarch.rpm
yum install -y --enablerepo rhel-7-server-optional-rpms xorg-x11-server-Xvfb xorg-x11-utils fluxbox x11vnc
```
