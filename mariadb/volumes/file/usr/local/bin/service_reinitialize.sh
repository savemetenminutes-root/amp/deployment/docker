#!/bin/bash

rm -f /etc/mysql/conf.d/custom.cnf
cat > /etc/mysql/conf.d/custom.cnf << EOL
[client]
default-character-set=utf8mb4

[mysql]
default-character-set=utf8mb4

[mysqld]
#init-file=/init/init-project-users-databases-plain.sql
init-connect='SET NAMES utf8mb4'
#character-set-client-handshake=utf8mb4
character-set-server=utf8mb4
collation-server=utf8mb4_general_ci
log-output=TABLE,FILE
slow-query-log-file=/var/log/mysql/slow.log
general-log-file=/var/log/mysql/general.log
EOL
