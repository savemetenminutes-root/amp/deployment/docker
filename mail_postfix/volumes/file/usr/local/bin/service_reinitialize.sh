#!/bin/bash

. wait_for_mariadb.sh
. wait_for_mysql.sh

/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS '${MAILSERVER_POSTFIX_MYSQL_USERNAME}'@'%' IDENTIFIED BY '${MAILSERVER_POSTFIX_MYSQL_PASSWORD}';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.* TO '${MAILSERVER_POSTFIX_MYSQL_USERNAME}'@'%';"
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'
/usr/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mariadb -u root -p"${MYSQL_ROOT_PASSWORD}" -e "
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.virtual_domains (
  id int(11) NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.users (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email (email),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.forwardings (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  source varchar(100) NOT NULL,
  destination varchar(100) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.transports (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  transport varchar(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.virtual_domains
  (id, name)
VALUES
  ('1', 'savemetenminutes.com'),
  ('2', 'savemetenminutes.biz'),
  ('3', 'savemetenminutes.org'),
  ('4', 'savemetenminutes.net'),
  ('5', 'savemetenminutes.rocks'),
  ('6', 'savemetenminutes.sucks'),
  ('7', 'savemetenminutes'),
  ('8', 'a.a')
ON DUPLICATE KEY UPDATE id=id, name=name
;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.users
  (id, domain_id, password, email)
VALUES
  ( '1', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'admin@savemetenminutes.com'),
  ( '2', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'sales@savemetenminutes.com'),
  ( '3', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'support@savemetenminutes.com'),
  ( '4', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'no_reply@savemetenminutes.com'),
  ( '5', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'help@savemetenminutes.com'),
  ( '6', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test0@savemetenminutes.com'),
  ( '7', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test1@savemetenminutes.com'),
  ( '8', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test2@savemetenminutes.com'),
  ( '9', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test3@savemetenminutes.com'),
  ('10', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test4@savemetenminutes.com'),
  ('11', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test5@savemetenminutes.com'),
  ('12', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'a@a.a'),
  ('13', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'b@a.a'),
  ('14', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'c@a.a'),
  ('15', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'd@a.a'),
  ('16', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'e@a.a'),
  ('17', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'f@a.a')
ON DUPLICATE KEY UPDATE id=id, domain_id=domain_id, password=password, email=email
;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.forwardings
  (id, domain_id, source, destination)
VALUES
  ('1', '1', 'alias0@savemetenminutes.com', 'aliastarget0@savemetenminutes.com'),
  ('1', '1', 'alias1@savemetenminutes.com', 'aliastarget1@savemetenminutes.com')
ON DUPLICATE KEY UPDATE id=id, domain_id=domain_id, source=source, destination=destination
;
" "${MAILSERVER_POSTFIX_MYSQL_DATABASE}"

/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE USER IF NOT EXISTS '${MAILSERVER_POSTFIX_MYSQL_USERNAME}'@'%' IDENTIFIED BY '${MAILSERVER_POSTFIX_MYSQL_PASSWORD}';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "CREATE DATABASE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE} CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "GRANT ALL PRIVILEGES ON ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.* TO '${MAILSERVER_POSTFIX_MYSQL_USERNAME}'@'%';"
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e 'FLUSH PRIVILEGES;'
/usr/local/bin/mysql -h "${COMPOSE_PROJECT_NAME}"-mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -e "
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.virtual_domains (
  id int(11) NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.users (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email (email),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.forwardings (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  source varchar(100) NOT NULL,
  destination varchar(100) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.transports (
  id int(11) NOT NULL auto_increment,
  domain_id int(11) NOT NULL,
  transport varchar(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.virtual_domains
  (id, name)
VALUES
  ('1', 'savemetenminutes.com'),
  ('2', 'savemetenminutes.biz'),
  ('3', 'savemetenminutes.org'),
  ('4', 'savemetenminutes.net'),
  ('5', 'savemetenminutes.rocks'),
  ('6', 'savemetenminutes.sucks'),
  ('7', 'savemetenminutes'),
  ('8', 'a.a')
ON DUPLICATE KEY UPDATE id=id, name=name
;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.users
  (id, domain_id, password, email)
VALUES
  ( '1', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'admin@savemetenminutes.com'),
  ( '2', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'sales@savemetenminutes.com'),
  ( '3', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'support@savemetenminutes.com'),
  ( '4', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'no_reply@savemetenminutes.com'),
  ( '5', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'help@savemetenminutes.com'),
  ( '6', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test0@savemetenminutes.com'),
  ( '7', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test1@savemetenminutes.com'),
  ( '8', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test2@savemetenminutes.com'),
  ( '9', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test3@savemetenminutes.com'),
  ('10', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test4@savemetenminutes.com'),
  ('11', '1', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'test5@savemetenminutes.com'),
  ('12', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'a@a.a'),
  ('13', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'b@a.a'),
  ('14', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'c@a.a'),
  ('15', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'd@a.a'),
  ('16', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'e@a.a'),
  ('17', '8', ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT('\$6\$', SUBSTRING(SHA(RAND()), -16))), 'f@a.a')
ON DUPLICATE KEY UPDATE id=id, domain_id=domain_id, password=password, email=email
;
INSERT INTO ${MAILSERVER_POSTFIX_MYSQL_DATABASE}.forwardings
  (id, domain_id, source, destination)
VALUES
  ('1', '1', 'alias0@savemetenminutes.com', 'aliastarget0@savemetenminutes.com'),
  ('1', '1', 'alias1@savemetenminutes.com', 'aliastarget1@savemetenminutes.com')
ON DUPLICATE KEY UPDATE id=id, domain_id=domain_id, source=source, destination=destination
;
" "${MAILSERVER_POSTFIX_MYSQL_DATABASE}"

cat > /etc/postfix/mysql-virtual_domains.cf << EOL
user = ${MAILSERVER_POSTFIX_MYSQL_USERNAME}
password = ${MAILSERVER_POSTFIX_MYSQL_PASSWORD}
dbname = ${MAILSERVER_POSTFIX_MYSQL_DATABASE}
query = SELECT name AS virtual FROM virtual_domains WHERE name='%s'
hosts = ${COMPOSE_PROJECT_NAME:-default}-mysql
EOL

cat > /etc/postfix/mysql-virtual_email2email.cf << EOL
user = ${MAILSERVER_POSTFIX_MYSQL_USERNAME}
password = ${MAILSERVER_POSTFIX_MYSQL_PASSWORD}
dbname = ${MAILSERVER_POSTFIX_MYSQL_DATABASE}
query = SELECT email FROM users WHERE email='%s'
hosts = ${COMPOSE_PROJECT_NAME:-default}-mysql
EOL

cat > /etc/postfix/mysql-virtual_forwardings.cf << EOL
user = ${MAILSERVER_POSTFIX_MYSQL_USERNAME}
password = ${MAILSERVER_POSTFIX_MYSQL_PASSWORD}
dbname = ${MAILSERVER_POSTFIX_MYSQL_DATABASE}
query = SELECT destination FROM forwardings WHERE source='%s'
hosts = ${COMPOSE_PROJECT_NAME:-default}-mysql
EOL

cat > /etc/postfix/mysql-virtual_mailboxes.cf << EOL
user = ${MAILSERVER_POSTFIX_MYSQL_USERNAME}
password = ${MAILSERVER_POSTFIX_MYSQL_PASSWORD}
dbname = ${MAILSERVER_POSTFIX_MYSQL_DATABASE}
query = SELECT CONCAT(SUBSTRING_INDEX(email,'@',-1),'/',SUBSTRING_INDEX(email,'@',1),'/') FROM users WHERE email='%s'
hosts = ${COMPOSE_PROJECT_NAME:-default}-mysql
EOL

chmod o= /etc/postfix/mysql-virtual_*.cf
chgrp postfix /etc/postfix/mysql-virtual_*.cf

cat > /etc/postfix/sasl/smtpd.conf << EOL
pwcheck_method: saslauthd
mech_list: plain login
allow_plaintext: true
auxprop_plugin: mysql
sql_hostnames: ${COMPOSE_PROJECT_NAME:-default}-mysql
sql_user: ${MAILSERVER_POSTFIX_MYSQL_USERNAME}
sql_passwd: ${MAILSERVER_POSTFIX_MYSQL_PASSWORD}
sql_database: ${MAILSERVER_POSTFIX_MYSQL_DATABASE}
sql_select: select password from users where email = '%u@%r'
sql_verbose: yes
log_level: 7
EOL

chmod o= /etc/postfix/sasl/smtpd.conf

cat > /etc/pam.d/smtp << EOL
auth    required   pam_mysql.so user=${MAILSERVER_POSTFIX_MYSQL_USERNAME} passwd=${MAILSERVER_POSTFIX_MYSQL_PASSWORD} host=${COMPOSE_PROJECT_NAME:-default}-mysql db=${MAILSERVER_POSTFIX_MYSQL_DATABASE} table=users usercolumn=email passwdcolumn=password crypt=1
account sufficient pam_mysql.so user=${MAILSERVER_POSTFIX_MYSQL_USERNAME} passwd=${MAILSERVER_POSTFIX_MYSQL_PASSWORD} host=${COMPOSE_PROJECT_NAME:-default}-mysql db=${MAILSERVER_POSTFIX_MYSQL_DATABASE} table=users usercolumn=email passwdcolumn=password crypt=1
EOL
chmod o= /etc/pam.d/smtp

rm -f /etc/postfix/smtpd.cert
rm -f /etc/postfix/smtpd.key
openssl \
    req -new -outform PEM -out /etc/postfix/smtpd.cert \
    -newkey rsa:4096 -nodes -keyout /etc/postfix/smtpd.key -keyform PEM -days 365 -x509 \
	-subj '/C=BG/ST=Shumen/L=Shumen/O=MilenKirilov/OU=IT Department/CN='"${MAILSERVER_DEFAULT_DOMAIN}"
chmod o= /etc/postfix/smtpd.key

# Postfix uses a jailed copy of /etc/resolv.conf which should be present at /var/spool/postfix/etc/resolv.conf
# The two files need to have exactly the same contents otherwise Postfix shows a warning during initialization.
# This will also make sure that the DNS server Postfix uses is docker's "embedded DNS server in user-defined networks"
# (127.0.0.11)
# See: https://docs.docker.com/v17.09/engine/userguide/networking/configure-dns/
cp -f /etc/resolv.conf /var/spool/postfix/etc/

postconf -e 'myhostname = '"${MAILSERVER_DEFAULT_DOMAIN}"
postconf -e 'mydestination = ""'
postconf -e 'mynetworks = 127.0.0.0/12'
postconf -e 'mynetworks_style = subnet'
postconf -e 'message_size_limit = 30720000'
postconf -e 'virtual_alias_domains ='
postconf -e 'virtual_alias_maps = proxy:mysql:/etc/postfix/mysql-virtual_forwardings.cf, mysql:/etc/postfix/mysql-virtual_email2email.cf'
postconf -e 'virtual_mailbox_domains = proxy:mysql:/etc/postfix/mysql-virtual_domains.cf'
postconf -e 'virtual_mailbox_maps = proxy:mysql:/etc/postfix/mysql-virtual_mailboxes.cf'
postconf -e 'virtual_mailbox_base = /home/vmail'
postconf -e 'virtual_uid_maps = static:5000'
postconf -e 'virtual_gid_maps = static:5000'
postconf -e 'smtpd_sasl_auth_enable = yes'
postconf -e 'broken_sasl_auth_clients = yes'
postconf -e 'smtpd_sasl_authenticated_header = yes'
postconf -e 'smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination'
postconf -e 'smtpd_use_tls = yes'
postconf -e 'smtpd_tls_cert_file = /etc/postfix/smtpd.cert'
postconf -e 'smtpd_tls_key_file = /etc/postfix/smtpd.key'
postconf -e 'proxy_read_maps = $local_recipient_maps $mydestination $virtual_alias_maps $virtual_alias_domains $virtual_mailbox_maps $virtual_mailbox_domains $relay_recipient_maps $relay_domains $canonical_maps $sender_canonical_maps $recipient_canonical_maps $relocated_maps $transport_maps $mynetworks'
#postconf -e "virtual_transport = lmtp:inet:$(getent hosts ${COMPOSE_PROJECT_NAME:-default}-mail_dovecot | sed 's/[[:space:]]*'"${COMPOSE_PROJECT_NAME:-default}"'-mail_dovecot//')"
# the previous line is not working and the hosts_fix.sh file solves the issue
#	postconf -e 'virtual_transport = lmtp:inet:'"$DOVECOT_HOSTNAME"
#	postconf -e 'virtual_transport = dovecot' && \
#	postconf -e 'dovecot_destination_recipient_limit=1'
hosts_fix.sh
postconf -e "virtual_transport = lmtp:inet:$(getent hosts ${COMPOSE_PROJECT_NAME:-default}-mail_dovecot | sed 's/[[:space:]]*'"${COMPOSE_PROJECT_NAME:-default}"'-mail_dovecot//')"
