#!/bin/bash

#service rsyslog start && /usr/sbin/sshd -D && postfix -D start
export COMPOSE_PROJECT_NAME="${COMPOSE_PROJECT_NAME:-default}"

rm -f /var/run/supervisor.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /var/run/supervisor.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
#service supervisor start
