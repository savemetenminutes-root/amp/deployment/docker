FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y supervisor rsyslog cron

# Enable SSH: ##################################################################
ARG LINUX_ROOT_PASSWORD
ENV LINUX_ROOT_PASSWORD ${LINUX_ROOT_PASSWORD}

RUN apt-get update && apt-get install -y openssh-server pwgen && \
    mkdir -p /var/run/sshd && \
    if [ ! -z "${LINUX_ROOT_PASSWORD}" ]; then echo "root:${LINUX_ROOT_PASSWORD}" | chpasswd; else export LINUX_ROOT_PASSWORD="$(pwgen -1 32)" && printf "${LINUX_ROOT_PASSWORD}\n" >> /.env && echo "root:${LINUX_ROOT_PASSWORD}" | chpasswd; fi && \
    sed -i -r 's/^#?PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?PermitEmptyPasswords.*/PermitEmptyPasswords yes/' /etc/ssh/sshd_config && \
    sed -i -r 's:^#?AuthorizedKeysFile.*:AuthorizedKeysFile %h/.ssh/authorized_keys:' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?GatewayPorts.*/GatewayPorts yes/' /etc/ssh/sshd_config && \
    sed -i -r 's/^#?PermitTunnel.*/PermitTunnel yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN rm -f /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa && \
    rm -f /etc/ssh/ssh_host_dsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa && \
    rm -f /etc/ssh/ssh_host_ecdsa_key && \
    ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa && \
    rm -f /etc/ssh/ssh_host_ed25519_key && \
    ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519
# END: enable SSH ###################################################################################################

# The `wait_for_mysql.sh` script uses the `mysql` binary. On Debian 10 "Buster" the `mariadb-client` package does not
# contain the `mysql` binary. It must be provided some other way (like mounting it directly inside the container, which
# requires installing the `libncurses5` manually to work).
RUN apt-get update && apt-get install -y \
    libncurses5 \
    mariadb-client

# Used by the wait_for_ssh_keys.sh script
RUN apt-get update && apt-get install -y \
    netcat

RUN \
    apt-get update && apt-get install -y \
    libpam-mysql

ENV DEBIAN_FRONTEND=noninteractive
ARG MAILSERVER_DEFAULT_DOMAIN
ENV MAILSERVER_DEFAULT_DOMAIN ${MAILSERVER_DEFAULT_DOMAIN}
RUN \
    echo "postfix postfix/main_mailer_type string 'No configuration'" | debconf-set-selections && \
    echo "postfix postfix/mailname string ${MAILSERVER_DEFAULT_DOMAIN}" | debconf-set-selections && \
    apt-get update && apt-get install -y \
    postfix postfix-mysql

RUN groupadd -g 5000 vmail && \
	useradd -g vmail -u 5000 vmail -d /home/vmail -m

RUN apt-get update && apt-get install -y \
		libsasl2-2 \
		libsasl2-modules \
		libsasl2-modules-sql \
		sasl2-bin
RUN cp -a /etc/default/saslauthd /etc/default/saslauthd.dist && \
    sed -i 's/START=no/START=yes/' /etc/default/saslauthd && \
    sed -i 's#OPTIONS=".*"#OPTIONS="-c -m /var/spool/postfix/var/run/saslauthd -r"#' /etc/default/saslauthd && \
    rm -r /var/run/saslauthd/ && \
    mkdir -p /var/spool/postfix/var/run/saslauthd && \
    ln -s /var/spool/postfix/var/run/saslauthd /var/run && \
    adduser postfix sasl

RUN cp -f /etc/services /var/spool/postfix/etc/services

#RUN apt-get update && apt-get install -y \
#    net-tools \
#    tcpdump \
#    mailutils \
#    telnet
