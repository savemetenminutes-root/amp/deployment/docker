#!/bin/bash

rm -f /var/run/supervisor.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /var/run/supervisor.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
#xvfb-run --auth-file='/tmp/xvfb-auth.cookie' --server-num=99 --server-args='-screen 0 1280x960x16' /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
#service supervisor start
