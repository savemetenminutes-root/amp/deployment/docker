#!/bin/bash

while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapi 443; do
    echo 'Waiting for the WebAPI to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 4200; do
    echo 'Waiting for the WebApp to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 49152; do
    echo 'Waiting for the WebApp to start...'
    sleep 1
done

# Script vars
scriptsDir="/home/node/app/user_scripts/spookyjs"
webappHostPort="16200"
webappContainerPort="4200"
webapiHostPort="13443"
webapiContainerPort="443"
casperEngine="phantomjs"
commonCommandLineArguments="--proxy-type=none --ignore-ssl-errors=true --ssl-protocol=any"
if [[ "$casperEngine" = "slimerjs" ]]; then
    commonCommandLineArguments="--engine=slimerjs --debug --headless --proxy-type=none --ssl-protocol=any"
fi

currentDir="$(pwd)"
# for headless PhantomJS
export QT_QPA_PLATFORM=offscreen
# for headless SlimerJS
export MOZ_HEADLESS=1
export SLIMERJS_EXECUTABLE="/usr/local/bin/slimerjs"
#export ENGINE_FLAGS="--config=/usr/local/lib/node_modules/slimer/config.js"

echo 'PhantomJS version: '$(phantomjs --version)
echo 'SlimerJS version: '$(/home/node/node_modules/.bin/slimerjs --version)
echo 'CasperJS version: '$(/home/node/node_modules/.bin/casperjs --version)

ssh -T -O "exit" webapp 2>/dev/null >/dev/null
ssh -T -O "exit" webapi 2>/dev/null >/dev/null

echo "Opening an SSH tunnel to the WebApp to forward port ${webappHostPort} to ${webappContainerPort} on the container..."
# Enforcing IPv4 with -4 otherwise an error is thrown:
# bind: Cannot assign requested address
# https://www.electricmonk.nl/log/2014/09/24/ssh-port-forwarding-bind-cannot-assign-requested-address/
command="ssh -f -N -T -M -4 -L ${webappHostPort}:127.0.0.1:${webappContainerPort} webapp"
${command}
if [ "$?" -eq 0 ]; then
    echo 'Success!'
else
    echo '`'${command}'` failed with status: '"$?"
    exit 1
fi

cd "${scriptsDir}"

echo 'Checking to see if the WebApp is up and running...'
command="node --inspect ${scriptsDir}/server-is-responsive.js"
echo 'Running `'${command}'`...'
${command}
exit 0
#/casperjs/bin/casperjs test ${commonPhantomjsCommandLineArguments} /casperjs/user_scripts/test-homepage-is-up.js >/dev/null
#if [ $? -ne 0 ]; then
#    while [ $? -ne 0 ]; do
#        echo 'The WebApp does not appear to be running. Starting 2sec interval polling...'
#        sleep 2
#        /casperjs/bin/casperjs --ignore-ssl-errors=true /casperjs/user_scripts/server-is-responsive.js >/dev/null
#    done
#else
#    echo 'Seems like...'
#fi


echo "Opening an SSH tunnel to the WebAPI to forward port ${webapiHostPort} to ${webapiContainerPort} on the container..."
command="ssh -f -N -T -M -4 -L ${webapiHostPort}:127.0.0.1:${webapiContainerPort} webapi"
eval ${command}
if [ "$?" -eq 0 ]; then
    echo 'Success!'
else
    echo '`'${command}'` failed with status: '"$?"
    exit 1
fi

command="/casperjs/bin/casperjs ${commonCommandLineArguments} ${scriptsDir}/$@"
echo 'Running `'${command}'`...'
${command}



if [[ -f "/root/.ssh/webapp.ctl" ]]; then
    ssh -T -O "exit" webapp
fi
if [[ -f "/root/.ssh/webapi.ctl" ]]; then
    ssh -T -O "exit" webapi
fi

cd "${currentDir}"
echo 'All done. Bye!'
