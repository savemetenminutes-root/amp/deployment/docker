<?php

$imap = imap_open('{insurance-hero_mail_dovecot:143/imap/novalidate-cert}INBOX', 'a@a.a', '123123') or die('Cannot connect: ' . print_r(imap_errors(), true));
$list = imap_list($imap, '{insurance-hero_mail_dovecot:143}', '*');
$numMsg = imap_num_msg($imap);
$pdfResource = PDF_new();
$messages = [];
for ($i = 1; $i <= $numMsg; $i++) {
    $messages[$i] = [
        'overview' => imap_fetch_overview($imap, $i),
        'body' => imap_fetchbody($imap, $i, null),
        'structureRaw' => imap_fetchstructure($imap, $i),
        'structure' => [
            'pdf' => [],
        ],
    ];
    foreach ($messages[$i]['structureRaw']->parts ?? [] as $j => $part) {
        if (strtolower($part->subtype) === 'pdf') {
            $messages[$i]['structure']['pdf'][$j]['raw'] = base64_decode(imap_fetchbody($imap, $i, ($j + 1)));
            $newDoc = PDF_begin_document($pdfResource, null, '');
            //PDF_create_pvf($pdfResource, 'virtualPDF_' . $i . '_' . $j, $messages[$i]['structure']['pdf'][$j]['raw'], '');
            //$doc = PDF_get_buffer($pdfResource);
            //PDF_open_pdi_document($pdfResource, 'virtualPDF_' . $i . '_' . $j, '');
            //$doc = PDF_get_buffer($pdfResource);
            //PDF_begin_document($pdfResource, 'virtualPDF_' . $i . '_' . $j, '');
            $pvf = PDF_create_pvf($pdfResource, 'virtualPDF_' . $i . '_' . $j, $messages[$i]['structure']['pdf'][$j]['raw'], '');
            $pdiDoc = PDF_open_pdi_document($pdfResource, 'virtualPDF_' . $i . '_' . $j, '');

            /* The following standard flavors can be cloned: */
            $supportedflavors = [
                "PDF/A-1a:2005", "PDF/A-1b:2005",
                "PDF/A-2a", "PDF/A-2b", "PDF/A-2u",
                "PDF/A-3a", "PDF/A-3b", "PDF/A-3u",

                "PDF/X-1a:2003",
                "PDF/X-3:2003",
                "PDF/X-4", "PDF/X-4p",
                "PDF/X-5g", "PDF/X-5pg", "PDF/X-5n",

                "PDF/UA-1",
                "none",
            ];

            $optlist = '';
            /*
             * Read PDF/A, PDF/UA and PDF/X version of the input document
             */
            $pdfaversion = PDF_pcos_get_string($pdfResource, $pdiDoc, "pdfa");
            $pdfuaversion = PDF_pcos_get_string($pdfResource, $pdiDoc, "pdfua");
            $pdfxversion = PDF_pcos_get_string($pdfResource, $pdiDoc, "pdfx");
            for ($i = 0; $i < count($supportedflavors); $i++) {
                if ($pdfaversion === $supportedflavors[$i]) {
                    $optlist .= " pdfa=" . $pdfaversion;
                    break;
                }
            }
            if ($i == count($supportedflavors)) {
                echo "Error: Cannot clone " . $pdfaversion . " documents";
            }

            for ($i = 0; $i < count($supportedflavors); $i++) {
                if ($pdfuaversion === $supportedflavors[$i]) {
                    $optlist .= " pdfua=" . $pdfuaversion;
                    break;
                }
            }
            if ($i == count($supportedflavors)) {
                echo "Error: Cannot clone " . $pdfuaversion . " documents";
            }

            for ($i = 0; $i < count($supportedflavors); $i++) {
                if ($pdfxversion === $supportedflavors[$i]) {
                    $optlist .= " pdfx=" . $pdfxversion;
                    break;
                }
            }
            if ($i == count($supportedflavors)) {
                echo "Error: Cannot clone " . $pdfxversion . " documents";
            }

            /*
             * Read language entry of the input document if present
             */

            if (PDF_pcos_get_string($pdfResource, $pdiDoc, "type:/Root/Lang") === "string") {
                $inputlang = PDF_pcos_get_string($pdfResource, $pdiDoc, "/Root/Lang");
                $optlist .= " lang=" . $inputlang;
            } else {
                $inputlang = "";
            }
            PDF_close_pdi_document($pdfResource, $pdiDoc);
            PDF_process_pdi($pdfResource, 'virtualPDF_' . $i . '_' . $j, 1, '');
            //PDF_end_document($pdfResource, '');
            $doc = PDF_get_buffer($pdfResource);
            //PDF_create_pvf($pdfResource, 'virtualPDF_' . $i . '_' . $j, $messages[$i]['structure']['pdf'][$j]['raw'], '');
            //$doc = PDF_get_buffer($pdfResource);
            echo $doc;
        }
    }
}
PDF_delete($pdfResource);
echo $list;