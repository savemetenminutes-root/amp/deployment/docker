'use strict';

const ClassSeleniumXWindow = require('./SeleniumXWindow');
const ClassSeleniumXImageInfo = require('./SeleniumXImageInfo');

class ClassSeleniumXState {
    constructor() {
        this.currentUrl = null;
        this.currentPageSource = null;
        this.htmlElementsCount = null;
        this.pageImageInfo = new ClassSeleniumXImageInfo();

        this.window = new ClassSeleniumXWindow(null, null, null);
        this.dom = null;
    }
}

module.exports = ClassSeleniumXState;