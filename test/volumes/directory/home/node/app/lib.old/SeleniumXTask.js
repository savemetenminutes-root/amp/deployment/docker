'use strict';

class ClassSeleniumXTask {
    get callable() {
        return this._callable;
    }

    set callable(value) {
        this._callable = value;
    }

    get thisReference() {
        return this._thisReference;
    }

    set thisReference(value) {
        this._thisReference = value;
    }

    get args() {
        return this._args;
    }

    set args(value) {
        this._args = value;
    }

    get successMessage() {
        return this._successMessage;
    }

    set successMessage(value) {
        this._successMessage = value;
    }

    get failureMessage() {
        return this._failureMessage;
    }

    set failureMessage(value) {
        this._failureMessage = value;
    }

    constructor(callable, thisReference, args, successMessage, failureMessage) {
        this._callable = callable;
        this._thisReference = thisReference;
        this._args = args;
        this._successMessage = successMessage;
        this._failureMessage = failureMessage;
    }
}

module.exports = ClassSeleniumXTask;