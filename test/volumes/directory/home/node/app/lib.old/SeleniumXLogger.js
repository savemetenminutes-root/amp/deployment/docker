'use strict';

const StaticClassSeleniumX = require('./StaticSeleniumX');
const ClassSeleniumXEventEmitter = require('./SeleniumXEventEmitter');

let BASIC = 1;
let NORMAL = 2;
let VERBOSE = 4;
let DEBUG = 8;

class ClassLogLevel {
    static get BASIC() {
        return BASIC;
    }

    static get NORMAL() {
        return NORMAL;
    }

    static get VERBOSE() {
        return VERBOSE;
    }

    static get DEBUG() {
        return DEBUG;
    }
}

let PLAIN = 1;
let INFO = 2;
let ACTION = 4;
let TEST = 8;
let CHECK = 16;
let PASSED = 32;
let SUCCESS = 64;
let INIT = 128;
let ENVIRONMENT = 256;

class ClassLogType {
    static get PLAIN() {
        return PLAIN;
    }

    static get INFO() {
        return INFO;
    }

    static get ACTION() {
        return ACTION;
    }

    static get TEST() {
        return TEST;
    }

    static get CHECK() {
        return CHECK;
    }

    static get PASSED() {
        return PASSED;
    }

    static get SUCCESS() {
        return SUCCESS;
    }

    static get INIT() {
        return INIT;
    }

    static get ENVIRONMENT() {
        return ENVIRONMENT;
    }
}

class ClassSeleniumXLogger {
    constructor(config, seleniumX) {
        this.config = config;
        this.config.logLevel = this.config.logLevel || ClassSeleniumXLogger.ClassLogLevel.NORMAL;

        if (this.config.logLevel & (ClassSeleniumXLogger.ClassLogLevel.NORMAL | ClassSeleniumXLogger.ClassLogLevel.VERBOSE | ClassSeleniumXLogger.ClassLogLevel.DEBUG)) {
            seleniumX.eventEmitter.onEvents(
                ['pre', 'init'],
                this.log.bind(this, 'Booting the test engine...', ClassSeleniumXLogger.ClassLogType.INIT),
            );

            seleniumX.eventEmitter.onEvents(
                ['post', 'init'],
                this.log.bind(this, 'Done.', ClassSeleniumXLogger.ClassLogType.INIT),
            );

            seleniumX.eventEmitter.onEvents(
                ['post', 'get', 'current', 'url'],
                async (seleniumX) => {
                    let oldUrl = seleniumX.state.currentUrl;
                    let currentUrl = await seleniumX.driver.getCurrentUrl();
                    if (oldUrl !== currentUrl) {
                        this.log('The current URL is now: ' + currentUrl, ClassSeleniumXLogger.ClassLogType.INFO);
                    }
                },
            );

            seleniumX.eventEmitter.onEvents(
                ['pre', 'keypress'],
                (seleniumX) => {
                    this.log('Press any key to continue...');
                },
                ClassSeleniumXEventEmitter.ClassCondition.SUBSET,
            );

            seleniumX.eventEmitter.onEvents(
                ['pre', 'do', 'basic', 'tests'],
                (seleniumX, pageName) => {
                    this.log('Running the basic test on the ' + pageName + ' page...', ClassSeleniumXLogger.ClassLogType.TEST);
                },
                ClassSeleniumXEventEmitter.ClassCondition.SUBSET,
            );

            seleniumX.eventEmitter.onEvents(
                ['test', 'test-registration', 'begin'],
                (seleniumX) => {
                    this.log('Testing the registration page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
                },
            );
        }
    }

    static get ClassLogLevel() {
        return ClassLogLevel;
    }

    static get ClassLogType() {
        return ClassLogType;
    }

    log(message, type) {
        let preText = '';
        let postText = '';
        switch (type) {
            case ClassSeleniumXLogger.ClassLogType.INFO:
                preText = 'Info: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.ACTION:
                preText = 'Action: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.TEST:
                preText = 'TEST: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.CHECK:
                preText = 'Check: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.PASSED:
                preText = 'Passed: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.SUCCESS:
                preText = 'SUCCESS: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.INIT:
                preText = 'Init: ';
                postText = '';
                break;
            case ClassSeleniumXLogger.ClassLogType.ENVIRONMENT:
                preText = '<<<<<<======>>>>>> ';
                postText = ' <<<<<<======>>>>>>';
                break;
            case ClassSeleniumXLogger.ClassLogType.PLAIN:
            default:
                preText = '';
                postText = '';
                break;
        }
        console.log(preText + message + postText);
    }
}

module.exports = ClassSeleniumXLogger;