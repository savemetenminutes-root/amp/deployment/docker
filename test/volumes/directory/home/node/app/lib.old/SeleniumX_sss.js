'use strict';

const StaticClassSeleniumX = require('./StaticSeleniumX');

const ClassSeleniumXEventEmitter = require('./SeleniumXEventEmitter');
const ClassSeleniumXExceptionHandler = require('./SeleniumXExceptionHandler');
const ClassSeleniumXException = require('./SeleniumXException');
const ClassSeleniumXLogger = require('./SeleniumXLogger');
const ClassSeleniumXState = require('./SeleniumXState');
const ClassSeleniumXImageInfo = require('./SeleniumXImageInfo');
const ClassSeleniumXElementFactory = require('./Factory/SeleniumXElementFactory');
const ClassSeleniumXElement = require('./SeleniumXElement');
const ClassSeleniumXWindow = require('./SeleniumXWindow');
const ClassSeleniumXDocument = require('./SeleniumXDocument');
const ClassSeleniumXBody = require('./SeleniumXBody');
const ClassSeleniumXTask = require('./SeleniumXTask');

class ClassSeleniumX {

    constructor() {
        this.eventEmitter = new ClassSeleniumXEventEmitter();
        this.exceptionHandler = new ClassSeleniumXExceptionHandler();
        this.logger = new ClassSeleniumXLogger({}, this);

        this.eventEmitter.emitEvents(['pre', 'init'], [this]);

        this.state = new ClassSeleniumXState();

        this.environment = {
            isWindows: process.platform === 'win32',
        };

        this.builder = new StaticClassSeleniumX.Selenium.Builder();
        this.chromeOptions = new StaticClassSeleniumX.Chrome.Options()
            .addArguments('no-sandbox', 'disable-plugins')
            .windowSize({width: StaticClassSeleniumX.ClassSeleniumXConfig.OUTER_WIDTH, height: StaticClassSeleniumX.ClassSeleniumXConfig.OUTER_HEIGHT})
            //.headless()
        ;
        this.environment.isWindows || this.chromeOptions.setChromeBinaryPath('/usr/bin/chromium');
        this.loggingPreferences = new StaticClassSeleniumX.Logging.Preferences();
        this.loggingPreferences.setLevel(StaticClassSeleniumX.Logging.Type.DRIVER, StaticClassSeleniumX.Logging.Level.DEBUG);
        this.loggingPreferences.setLevel(StaticClassSeleniumX.Logging.Type.BROWSER, StaticClassSeleniumX.Logging.Level.DEBUG);
        this.chromeCapabilities = StaticClassSeleniumX.Capabilities.Capabilities.chrome();
        this.chromeCapabilities.setLoggingPrefs(this.loggingPreferences);
        this.builder.forBrowser(StaticClassSeleniumX.Capabilities.Browser.CHROME)
            .setChromeOptions(
                this.chromeOptions,
            )
            .withCapabilities(this.chromeCapabilities)
        ;

        return (async () => {
            this.driver = await this.builder.build()
                .catch(
                    this.exceptionHandler.throwException.bind(
                        this,
                        [
                            ClassSeleniumXException.ClassSeverity.FATAL,
                            ClassSeleniumXException.ClassType.DRIVER,
                            'Could not build driver.',
                        ]
                    )
                );
            StaticClassSeleniumX.Logging.installConsoleHandler();
            this.logs = this.driver.manage().logs();
            this.availableLogTypes = await this.logs.getAvailableLogTypes();
            this.httpAgent = this.builder.getHttpAgent();
            this.navigation = this.driver.navigate();
            this.executor = this.driver.getExecutor();
            //this.actions = new StaticClassSeleniumX.Input.Actions(this.executor, {async: true});
            //this.actions = this.driver.actions();

            this.eventEmitter.emitEvents(['post', 'init'], [this]);

            return this;
        })();
    }

    async getCurrentUrl() {
        this.eventEmitter.emitEvents(['pre', 'get', 'current', 'url', 'async'], [this]);

        let currentUrl = this.driver.getCurrentUrl();

        this.eventEmitter.emitEvents(['post', 'get', 'current', 'url', 'async'], [this]);

        return currentUrl;
    }

    async getCurrentPageSource() {
        this.eventEmitter.emitEvents(['pre', 'get', 'current', 'page', 'source', 'async'], [this]);

        let currentPageSource = this.driver.getPageSource();

        this.eventEmitter.emitEvents(['post', 'get', 'current', 'page', 'source', 'async'], [this]);

        return currentPageSource;
    }

    async getHtmlElementsCount() {
        this.eventEmitter.emitEvents(['pre', 'get', 'html', 'element', 'count', 'async'], [this]);

        let htmlElementsCount = this.driver.executeScript('return document.getElementsByTagName("*").length;');

        this.eventEmitter.emitEvents(['post', 'get', 'html', 'element', 'count', 'async'], [this]);

        return htmlElementsCount;
    }

    async getWindowInstance() {
        this.eventEmitter.emitEvents(['pre', 'get', 'window', 'instance', 'async'], [this]);

        let windowInstance = this.driver.manage().window();

        this.eventEmitter.emitEvents(['post', 'get', 'window', 'instance', 'async'], [this]);

        return windowInstance;
    }

    async getDocumentInstance() {
        this.eventEmitter.emitEvents(['pre', 'get', 'document', 'instance', 'async'], [this]);

        let documentInstance = await this.driver.executeScript('return document;');

        this.eventEmitter.emitEvents(['post', 'get', 'document', 'instance', 'async'], [this]);

        return documentInstance;
    }

    async getBodyInstance() {
        this.eventEmitter.emitEvents(['pre', 'get', 'body', 'instance', 'async'], [this]);

        let bodyInstance = await this.driver.executeScript('return document.body;');

        this.eventEmitter.emitEvents(['post', 'get', 'body', 'instance', 'async'], [this]);

        return bodyInstance;
    }

    async refreshState() {
        this.eventEmitter.emitEvents(['pre', 'refresh', 'state', 'async'], [this]);

        let resultsTasks = [
            new ClassSeleniumXTask(this.getWindowInstance, this, []),
            new ClassSeleniumXTask(this.getCurrentUrl, this, []),
            new ClassSeleniumXTask(this.getCurrentPageSource, this, []),
            new ClassSeleniumXTask(this.getPageImageInfo, this, [true]),
        ];
        let results = await this.runAsync(resultsTasks);
        this.state.window = new ClassSeleniumXWindow(results[0], await this.getDocumentInstance(), await this.getBodyInstance());
        this.state.currentUrl = results[1];
        this.state.currentPageSource = results[2];
        this.state.pageImageInfo = results[3];

        this.state.htmlElementsCount = await this.getHtmlElementsCount();
        this.state.dom = new StaticClassSeleniumX.ClassSeleniumXJsdom.JSDOM.JSDOM(
            this.state.currentPageSource,
            {
                url: this.state.currentUrl,
                referrer: StaticClassSeleniumX.ClassSeleniumXRoutes.BASE,
                //contentType: "text/html",
                includeNodeLocations: true,
                pretendToBeVisual: true,
                //storageQuota: 10000000,
                //sessionStorage: 5000000,
                //runScripts: "dangerously" | "outside-only",
                resources: new StaticClassSeleniumX.ClassSeleniumXJsdom.ClassCustomResourceLoader({
                    //proxy: "http://127.0.0.1:9001",
                    strictSSL: false,
                    //userAgent: "Mellblomenator/9000",
                    //userAgent: `Mozilla/5.0 (${process.platform || "unknown OS"}) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/${jsdomVersion}`,
                    userAgent: await this.driver.executeScript('return navigator.userAgent;'),
                }),
            }
        );

        this.eventEmitter.emitEvents(['post', 'refresh', 'state', 'async'], [this]);

        return this;
    }

    async navigateAsync(url) {
        this.eventEmitter.emitEvents(['pre', 'navigate', 'async'], [this]);

        console.log('Action: Navigating to ' + url + '');
        return this.driver.get(url)
            .then(
                async(result) => {
                    this.eventEmitter.emitEvents(['post', 'navigate', 'async'], [this]);
                    await this.refreshState();

                    return result;
                },
            )
            .catch(
                function (error) {
                    throw error;
                },
            );
    }

    async navigate(url) {
        this.eventEmitter.emitEvents(['pre', 'navigate', 'sync'], [this]);

        console.log('Action: Navigating to ' + url + '');
        return await this.driver.get(url)
            .then(
                async(result) => {
                    this.eventEmitter.emitEvents(['post', 'navigate', 'sync'], [this]);
                    await this.refreshState();

                    return result;
                },
            )
            .catch(
                function (error) {
                    throw error;
                }
            );
    }

    async navigateSourceAsync(url) {
        this.eventEmitter.emitEvents(['pre', 'navigate', 'source', 'async'], [this]);

        return this.navigateAsync('view-source:' + url)
            .then(
                result => {
                    this.eventEmitter.emitEvents(['post', 'navigate', 'source', 'async'], [this]);

                    return result;
                },
            )
            .catch(
                error => {
                    throw error;
                }
            );
    };

    async navigateSource(url) {
        this.eventEmitter.emitEvents(['pre', 'navigate', 'source', 'sync'], [this]);

        await this.navigate('view-source:' + url);

        this.eventEmitter.emitEvents(['post', 'navigate', 'source', 'sync'], [this]);

        return this;
        //console.log(await driver.getTitle());
        //console.log(await driver.getStatus());
        //await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
        //await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
    };

    async existsElementByCssSelector(css, url) {
        this.eventEmitter.emitEvents(['pre', 'exists', 'element', 'by', 'css', 'selector', 'async'], [this]);

        if (url && (url !== this.driver.getCurrentUrl())) {
            await this.driver.get(url);
        }

        let existsElement = true;
        try {
            await this.driver.findElement(StaticClassSeleniumX.Selenium.By.css(css));
        } catch (e) {
            existsElement = false;
        }

        this.eventEmitter.emitEvents(['post', 'exists', 'element', 'by', 'css', 'selector', 'async'], [this]);

        return existsElement;
    }

    async existsElementById(id, url) {
        this.eventEmitter.emitEvents(['pre', 'exists', 'element', 'by', 'id', 'async'], [this]);

        if (url && (url !== this.driver.getCurrentUrl())) {
            await this.driver.get(url);
        }

        let existsElement = true;
        try {
            await this.driver.findElement(StaticClassSeleniumX.Selenium.By.id(id));
        } catch (e) {
            existsElement = false;
        }

        this.eventEmitter.emitEvents(['post', 'exists', 'element', 'by', 'id', 'async'], [this]);

        return existsElement;
    }

    async existsElementAByLinkText(linkText, url) {
        this.eventEmitter.emitEvents(['pre', 'exists', 'element', 'by', 'a', 'link', 'text', 'async'], [this]);

        if (url && (url !== this.driver.getCurrentUrl())) {
            await this.driver.get(url);
        }

        let existsElement = true;
        try {
            await this.driver.findElement(StaticClassSeleniumX.Selenium.By.linkText(linkText));
        } catch (e) {
            existsElement = false;
        }

        this.eventEmitter.emitEvents(['post', 'exists', 'element', 'by', 'a', 'link', 'text', 'async'], [this]);

        return existsElement;
    }

    async existsElementAsync(by, url) {
        this.eventEmitter.emitEvents(['pre', 'exists', 'element', 'async'], [this]);

        if (url && (url !== this.driver.getCurrentUrl())) {
            await this.driver.get(url);
        }

        let existsElement = true;
        try {
            await this.driver.findElement(by);
        } catch (e) {
            existsElement = false;
        }

        this.eventEmitter.emitEvents(['post', 'exists', 'element', 'async'], [this]);

        return existsElement;
    }

    async findElementAsync(by, message) {
        this.eventEmitter.emitEvents(['pre', 'find', 'element', 'async'], [this]);

        if(message) {
            console.log(message);
        }

        try {
            let instance = await this.driver.findElement(by);

            this.eventEmitter.emitEvents(['post', 'find', 'element', 'async'], [this]);

            return await ClassSeleniumXElementFactory.create(this, instance);
        } catch(e) {
            return null;
        }
    }

    async findElementsAsync(by, message) {
        this.eventEmitter.emitEvents(['pre', 'find', 'elements', 'async'], [this]);

        if(message) {
            console.log(message);
        }

        try {
            let elements = await this.driver.findElements(by);

            let returnValue = [];
            for (let i in elements) {
                if (elements.hasOwnProperty(i)) {
                    returnValue.push(await ClassSeleniumXElementFactory.create(this, elements[i]));
                }
            }

            this.eventEmitter.emitEvents(['post', 'find', 'elements', 'async'], [this]);

            return returnValue;
        } catch(e) {
            return [];
        }
    }

    async findChildOfElementAsync(parentElement, by, message) {
        this.eventEmitter.emitEvents(['pre', 'find', 'child', 'of', 'element', 'async'], [this]);

        if(message) {
            console.log(message);
        }

        try {
            let instance = await parentElement.instance.findElement(by);

            this.eventEmitter.emitEvents(['post', 'find', 'child', 'of', 'element', 'async'], [this]);

            return await ClassSeleniumXElementFactory.create(this, instance);
        } catch(e) {
            return null;
        }
    }

    async findChildrenOfElementAsync(element, by, message) {
        this.eventEmitter.emitEvents(['pre', 'find', 'children', 'of', 'element', 'async'], [this]);

        if(message) {
            console.log(message);
        }

        try {
            let elements = await element.instance.findElements(by);

            let returnValue = [];
            for (let i in elements) {
                if (elements.hasOwnProperty(i)) {
                    returnValue.push(await ClassSeleniumXElementFactory.create(this, elements[i]));
                }
            }

            this.eventEmitter.emitEvents(['post', 'find', 'children', 'of', 'element', 'async'], [this]);

            return returnValue;
        } catch(e) {
            return [];
        }
    }

    async click(element) {
        this.eventEmitter.emitEvents(['pre', 'click', 'sync'], [this]);

        return await element.instance
            .click()
            .then(
                async(result) => {
                    await this.refreshState();
                    this.eventEmitter.emitEvents(['post', 'click', 'sync'], [this]);

                    return result;
                },
            )
            .catch(
                (error) => {
                    throw error;
                }
            );
    }

    /**
     * Check out WebElement.sendKeys for an explanation of how to use key modifiers CTRL, ALT, SHIFT, etc...
     */
    async sendKeys(element, ...args) {
        this.eventEmitter.emitEvents(['pre', 'send', 'keys', 'async'], [this]);

        return await element.instance
            .sendKeys(...args)
            .then(
                async(result) => {
                    await this.refreshState();
                    this.eventEmitter.emitEvents(['post', 'send', 'keys', 'async'], [this]);

                    return this;
                },
            )
            .catch(
                function (error) {
                    throw error;
                }
            );
    }

    async waitUntilUrlIs(url, delay) {
        return this.driver.wait(StaticClassSeleniumX.Until.urlIs(url), delay)
            .then(
                async(result) => {
                    await this.refreshState();

                    return result;
                }
            );
    }

    async waitUntilElementLocated(by, delay) {
        this.eventEmitter.emitEvents(['pre', 'wait', 'until', 'element', 'located'], [this]);

        let instance = await this.driver.wait(StaticClassSeleniumX.Until.elementLocated(by), delay);
        await this.refreshState();

        let returnValue = null;

        if(instance) {
            returnValue = await ClassSeleniumXElementFactory.create(this, instance);
        }

        this.eventEmitter.emitEvents(['post', 'wait', 'until', 'element', 'located'], [this]);

        return returnValue;
    }

    async waitUntilStalenessOf(element, delay) {
        this.eventEmitter.emitEvents(['pre', 'wait', 'until', 'staleness', 'of'], [this]);

        let stale = await this.driver.wait(StaticClassSeleniumX.Until.stalenessOf(element.instance), delay);
        await this.refreshState();

        let returnValue = false;

        if(stale) {
            element.state.stale = true;

            returnValue = true;
        }

        this.eventEmitter.emitEvents(['post', 'wait', 'until', 'staleness', 'of'], [this]);

        return returnValue;
    }

    async runAsync(tasks) {
        this.eventEmitter.emitEvents(['pre', 'run', 'async'], [this]);

        let results = null;
        try {
            results = await Promise.all(
                tasks.map(task => {
                    this.eventEmitter.emitEvents(['pre', 'apply', 'task', 'async'], [this]);

                    let result;
                    try {
                        result = task.callable.apply(task.thisReference, task.args);
                    } catch (e) {
                        throw e;
                    }

                    this.eventEmitter.emitEvents(['post', 'apply', 'task', 'async'], [this]);

                    return result;
                }),
            );
        } catch(e) {
            // TODO: MK - Implement
        }

        this.eventEmitter.emitEvents(['post', 'run', 'async'], [this]);

        return results;
    }

    async formTest(elements, submitButton, validateCallback) {
        this.eventEmitter.emitEvents(['pre', 'form', 'test', 'sync'], [this]);

        await this.runAsync(
            elements.map((element) => {
                let elementField = element.shift();
                if(typeof element[0] === 'function') {
                    return new ClassSeleniumXTask(element[0], this, [elementField]);
                } else {
                    return new ClassSeleniumXTask(this.sendKeys, this, [elementField, ...element]);
                }
            })
        );
        await this.click(submitButton);

        this.eventEmitter.emitEvents(['post', 'form', 'test', 'sync'], [this]);

        return validateCallback();
    }

    async getElementHtml(element) {
        this.eventEmitter.emitEvents(['pre', 'get', 'element', 'html', 'sync'], [this]);

        let returnValue = await element.getAttribute('outerHTML');

        this.eventEmitter.emitEvents(['post', 'get', 'element', 'html', 'sync'], [this]);

        return returnValue;
    }

    async takeScreenshot(fullPage, scroll) {
        this.eventEmitter.emitEvents(['pre', 'take', 'screenshot', 'async'], [this]);

        let base64Img = require('base64-img');
        if(fullPage) {
            const stream = require('stream');
            const fs = require('fs');

            let totalHeight = await this.driver.executeScript('return document.body.offsetHeight;');
            let windowHeight = await this.driver.executeScript('return window.outerHeight;');
            let rawBase64Data = [];

            for (let i = 0; i <= (totalHeight / windowHeight); i++) {
                await this.driver.executeScript('window.scrollTo(0, window.outerHeight*' + i + ')');
                rawBase64Data[rawBase64Data.length] = Buffer.from(await this.driver.takeScreenshot(scroll), 'base64');
            }

            let targetDirectory = this.environment.isWindows ? process.env.HOMEDRIVE + process.env.HOMEPATH + '/Pictures' : '/home/node/storage/screenshots';
            let targetFilenameNoExtension = 'screenshot' + ClassSeleniumX.getTimestampForFilename();
            let targetFilename = targetFilenameNoExtension + '.png';
            let targetFile = targetDirectory + '/' + targetFilename;
            let concatBuffer = Buffer.concat(rawBase64Data);

            let stdout = new stream.Writable();
            let stderr = new stream.Writable();
            /*
            stderr.on(
                'data',
                function(data) {
                    console.log(data);
                }
            );
            stderr.on(
                'end',
                function(blabla) {
                    console.log(blabla);
                }
            );
            */

            let proc = await this.procOpen('convert' + ' png:-'.repeat(rawBase64Data.length) + ' -gravity center -append png:-', concatBuffer, /*stdout, stderr*/);

            console.log('Action: Saving screenshot to: ' + targetFile);

            fs.writeFileSync(targetFile, proc.stdout);
        } else {
            let rawBase64Data = await this.driver.takeScreenshot(scroll);

            let targetDirectory = this.environment.isWindows ? process.env.HOMEDRIVE + process.env.HOMEPATH + '/Pictures' : '/home/node/storage/screenshots';
            let targetFilenameNoExtension = 'screenshot' + ClassSeleniumX.getTimestampForFilename();
            let targetFilename = targetFilenameNoExtension + '.png';
            let targetFile = targetDirectory + '/' + targetFilename;

            console.log('Action: Saving screenshot to: ' + targetFile);

            base64Img.imgSync('data:image/png;base64,' + rawBase64Data, targetDirectory, targetFilenameNoExtension, function (error, filepath) {
                throw error;
            });
        }

        this.eventEmitter.emitEvents(['post', 'take', 'screenshot', 'async'], [this]);

        return this;
    }

    async getPageImageInfo(scroll) {
        this.eventEmitter.emitEvents(['pre', 'get', 'page', 'image', 'info', 'async'], [this]);

        const PNG = require('pngjs').PNG;
        //await this.driver.get('about:blank');
        let base64Data = await this.driver.takeScreenshot(scroll);
        let pngBinary = new Buffer.from(base64Data, 'base64');
        let image = PNG.sync.read(pngBinary);
        let imageInfo = new ClassSeleniumXImageInfo();
        imageInfo.image = image;

        // this.pack();
        imageInfo.width = image.width;
        imageInfo.height = image.height;
        imageInfo.totalPixels = image.width * image.height;
        for (let y = 0; y < image.height; y++) {
            for (let x = 0; x < image.width; x++) {
                let idx = (image.width * y + x) << 2;
                if(!imageInfo.uniqueColorsTransparency[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2] + ',' + imageInfo.image.data[idx+3]]) {
                    imageInfo.uniqueColorsTransparency[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2] + ',' + imageInfo.image.data[idx+3]] = {
                        r: imageInfo.image.data[idx],
                        g: imageInfo.image.data[idx+1],
                        b: imageInfo.image.data[idx+2],
                        a: imageInfo.image.data[idx+3],
                        count: 1,
                    };
                } else {
                    imageInfo.uniqueColorsTransparency[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2] + ',' + imageInfo.image.data[idx+3]].count++;
                }
                if(!imageInfo.uniqueColors[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2]]) {
                    imageInfo.uniqueColors[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2]] = {
                        r: imageInfo.image.data[idx],
                        g: imageInfo.image.data[idx+1],
                        b: imageInfo.image.data[idx+2],
                        count: 1,
                    };
                } else {
                    imageInfo.uniqueColors[imageInfo.image.data[idx] + ',' + imageInfo.image.data[idx+1] + ',' + imageInfo.image.data[idx+2]].count++;
                }

                if(imageInfo.image.data[idx] === 255 && imageInfo.image.data[idx+1] === 255 && imageInfo.image.data[idx+2] === 255) {
                    imageInfo.whitePixels++;
                }
                if(imageInfo.image.data[idx] === 0 && imageInfo.image.data[idx+1] === 0 && imageInfo.image.data[idx+2] === 0) {
                    imageInfo.blackPixels++;
                }
                if(imageInfo.image.data[idx] === 255 && imageInfo.image.data[idx+1] === 0 && imageInfo.image.data[idx+2] === 0) {
                    imageInfo.redPixels++;
                }
                if(imageInfo.image.data[idx] === 0 && imageInfo.image.data[idx+1] === 255 && imageInfo.image.data[idx+2] === 0) {
                    imageInfo.greenPixels++;
                }
                if(imageInfo.image.data[idx] === 0 && imageInfo.image.data[idx+1] === 0 && imageInfo.image.data[idx+2] === 255) {
                    imageInfo.bluePixels++;
                }
                if(imageInfo.image.data[idx+3] === 0) {
                    imageInfo.transparentPixels++;
                }

                // and reduce opacity
                //imageInfo.image.data[idx+3] = imageInfo.image.data[idx+3] >> 1;
            }
        }

        /*
        let numberOfOccurrences = 0;
        let j = 0;
        for(let i = 0; i < pngBinary.length; i++) {
            if (pngBinary[i] !== ClassSeleniumX.whitePixelBinarySequence[j]) {
                j = 0;
            } else {
                j++;
            }
            if(j === ClassSeleniumX.whitePixelBinarySequence.length) {
                numberOfOccurrences++;
                j = 0;
            }
        }

        return (numberOfOccurrences*ClassSeleniumX.whitePixelBinarySequence.length)/pngBinary.length;
        */
        this.eventEmitter.emitEvents(['post', 'get', 'page', 'image', 'info', 'async'], [this]);

        return imageInfo;
    }

    async doBasicTests(pageName) {
        this.eventEmitter.emitEvents(['pre', 'do', 'basic', 'tests', 'async'], [this, pageName]);

        if(this.state.currentPageSource.length < 1000) {
            process.exitCode = 1;
            throw new Error('>>>>>> Error: The ' + pageName + ' page source code is suspiciously short. Number of characters: ' + this.state.currentPageSource.length);
        }
        console.log('Passed: The ' + pageName + ' page source code is ' + this.state.currentPageSource.length + ' characters long.');

        if(!this.validateHtmlSourceBasic(this.state.currentPageSource)) {
            process.exitCode = 1;
            throw new Error('>>>>>> Error: The ' + pageName + ' page source code does not contain proper html opening/closing tag(s).');
        }
        console.log('Passed: The ' + pageName + ' page contains an opening and closing html tag.');

        if(this.state.htmlElementsCount < 10) {
            process.exitCode = 1;
            throw new Error('>>>>>> Error: The ' + pageName + ' page has <10 HTML elements. Number of HTML elements: ' + this.state.currentPageSource.length);
        }
        console.log('Passed: The ' + pageName + ' page contains ' + this.state.htmlElementsCount + ' HTML elements.');

        if(this.state.pageImageInfo.getWhitenessPercentage() > 90) {
            throw new Error('>>>>>> Error: The ' + pageName + ' page is >90% white. It might be erroring out resulting in a (almost) blank page.');
        }
        console.log('Passed: The ' + pageName + ' page is ' + this.state.pageImageInfo.getWhitenessPercentage().toFixed(2) + '% white.');
        console.log('Info: The ' + pageName + ' page contains ' + this.state.pageImageInfo.getUniqueColorsCount() + ' unique color(s).');

        this.eventEmitter.emitEvents(['post', 'do', 'basic', 'tests', 'async'], [this]);

        return this;
    }

    validateHtmlSourceBasic(source) {
        this.eventEmitter.emitEvents(['pre', 'validate', 'html', 'source', 'basic', 'sync'], [this]);

        let returnValue = /<html[\s\S]+<\/html>/.test(source);

        this.eventEmitter.emitEvents(['post', 'validate', 'html', 'source', 'basic', 'sync'], [this]);

        return returnValue;
    }

    validateAllResultsSimple(tasks, results) {
        this.eventEmitter.emitEvents(['pre', 'validate', 'all', 'results', 'simple', 'sync'], [this]);

        results.forEach(
            (element, index) => {
                if(results[index]) {
                    if(tasks[index].successMessage) {
                        this.logger.log(tasks[index].successMessage, ClassSeleniumXLogger.ClassLogType.PASSED);
                    }
                } else {
                    if(tasks[index].failureMessage) {
                        this.exceptionHandler.throwException(
                            ClassSeleniumXException.ClassSeverity.FATAL,
                            ClassSeleniumXException.ClassType.TEST,
                            tasks[index].failureMessage
                        );
                    }
                }
            }
        );

        this.eventEmitter.emitEvents(['post', 'validate', 'all', 'results', 'simple', 'sync'], [this]);
    }

    validateAllResults(tasks, results, callable) {
        this.eventEmitter.emitEvents(['pre', 'validate', 'all', 'results', 'sync'], [this]);

        results.forEach(
            (element, index) => {
                if(callable(element, index)) {
                    if(tasks[index].successMessage) {
                        this.logger.log(tasks[index].successMessage, ClassSeleniumXLogger.ClassLogType.PASSED);
                    }
                } else {
                    if(tasks[index].failureMessage) {
                        this.exceptionHandler.throwException(
                            ClassSeleniumXException.ClassSeverity.FATAL,
                            ClassSeleniumXException.ClassType.TEST,
                            tasks[index].failureMessage
                        );
                    }
                }
            }
        );

        this.eventEmitter.emitEvents(['post', 'validate', 'all', 'results', 'sync'], [this]);
    }

    static getTimestampForFilename() {
        let now = new Date();
        return now.getUTCFullYear() + '' + ((now.getUTCMonth() + 1) + '').padStart(2, '0') + (now.getUTCDate() + '').padStart(2, '0') + '_' + (now.getUTCHours() + '').padStart(2, '0') + (now.getUTCMinutes() + '').padStart(2, '0') + (now.getUTCSeconds() + '').padStart(2, '0');
    }

    async procOpen(command, input, output, error) {
        this.eventEmitter.emitEvents(['pre', 'proc', 'open', 'async'], [this]);

        const {streamWrite, streamEnd, onExit} = require('@rauschma/stringio');
        const {spawn, spawnSync} = require('child_process');

        let options = {};
        options.shell = '/bin/bash';
        options.input = input || null;
        options.stdio = [
            null,
        ];
        if(output) {
            options.stdio.push(output || null);
        }
        if(error) {
            options.stdio.push(error || null);
        }
        let sink = spawnSync(
                command,
                [], // command arguments
                options,
            );

        this.eventEmitter.emitEvents(['post', 'proc', 'open', 'async'], [this]);

        return sink;
    }

    async waitForKeypress() {
        this.eventEmitter.emitEvents(['pre', 'keypress', 'async'], [this]);

        process.stdin.setRawMode(true);
        // Not working properly. Pressing a key doesn't resolve the promise.
        return new Promise(resolve => process.stdin.once('data', () => {
            process.stdin.setRawMode(false);

            this.eventEmitter.emitEvents(['post', 'keypress', 'async'], [this]);
            resolve();
        }));
    }

    quit() {
        this.eventEmitter.emitEvents(['pre', 'quit', 'async'], [this]);

        if(process.env.XSERVER && (process.env.XSERVER === '1')) {
            return this.waitForKeypress()
                .then(
                    () => {
                        return this.driver.quit()
                            .then(
                                result => {
                                    this.eventEmitter.emitEvents(['post', 'quit', 'async'], [this]);

                                    return result;
                                },
                            )
                            .catch(
                                function (error) {
                                    this.exceptionHandler.throwException(
                                        ClassSeleniumXException.ClassSeverity.FATAL,
                                        ClassSeleniumXException.ClassType.TEST,
                                        error.message,
                                    );
                                }
                            );
                    }
            );
            /*
            console.log('Press any key to exit...');

            process.stdin.setRawMode(true);
            process.stdin.resume();
            process.stdin.on('data', process.exit.bind(process, 0));
            */
        }

        return this.driver.quit()
            .then(
                result => {
                    this.eventEmitter.emitEvents(['post', 'quit', 'async'], [this]);

                    return result;
                },
            )
            .catch(
                function (error) {
                    this.exceptionHandler.throwException(
                        ClassSeleniumXException.ClassSeverity.FATAL,
                        ClassSeleniumXException.ClassType.TEST,
                        error.message,
                    );
                }
            );
    }

    async getStyledCheckboxCheckedState(element) {
        let styledCheckboxCheckMarkComputedCss =
            await this
                .driver
                .executeScript(
                    'return {' +
                    '   opacity: window.getComputedStyle(arguments[0], ":after").getPropertyValue("opacity"),' +
                    '   visibility: window.getComputedStyle(arguments[0], ":after").getPropertyValue("visibility"),' +
                    '};',
                    element.instance
                );

        return styledCheckboxCheckMarkComputedCss.opacity === '1' || styledCheckboxCheckMarkComputedCss.visibility === 'visible';
    }

    async loadRoute(targetRoute, targetPageName, requiresAuthentication) {
        if (this.state.currentUrl !== targetRoute) {
            await this.navigate(targetRoute);
        }

        if (requiresAuthentication && this.state.currentUrl === StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN) {
            this.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The ' + targetPageName + ' route redirects to the login page (' + targetRoute + ' -> ' + this.state.currentUrl + '). Cannot continue the ' + targetPageName + ' page tests. Aborting.',
            );
        }

        await this.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.css('.ember-view.liquid-container'), 20000);
        if (!await this.existsElementByCssSelector('.ember-view.liquid-container')) {
            this.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The ' + targetPageName + ' route (' + this.state.currentUrl + ') does not render the page properly as the container with classes .ember-view.liquid-container could not be found after 20 seconds. Cannot continue the ' + targetPageName + ' page tests. Aborting.',
            );
        }
    }

    getRouteRelative(route) {
        if(route.indexOf(StaticClassSeleniumX.ClassSeleniumXRoutes.BASE) !== 0) {
            this.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.RUNTIME,
                'Invalid route passed to seleniumX.getRoutePath as it does not start with the base URL.',
            );
        }

        return route.substr(StaticClassSeleniumX.ClassSeleniumXRoutes.BASE.length);
    }

    shutdownWithError(error) {
        this.eventEmitter.emitEvents(['pre', 'shutdown', 'with', 'error', 'async'], [this]);

        process.exitCode = 1;
        console.log(error);
        //console.log(error.message);
        console.log(':( Failure: Test failed.');
        //console.log('seleniumX is ' + typeof seleniumX);

        this.eventEmitter.emitEvents(['post', 'shutdown', 'with', 'error', 'async'], [this]);

        this.quit(); // terminates the session and disposes of the driver effectively ending the process
    }

    shutdownWithSuccess() {
        this.eventEmitter.emitEvents(['pre', 'shutdown', 'with', 'success', 'async'], [this]);

        console.log(':) Success: The test was passed successfully.');
        //console.log('seleniumX is ' + typeof seleniumX);

        this.eventEmitter.emitEvents(['post', 'shutdown', 'with', 'success', 'async'], [this]);

        this.quit(); // terminates the session and disposes of the driver effectively ending the process
    }
}

module.exports = ClassSeleniumX;