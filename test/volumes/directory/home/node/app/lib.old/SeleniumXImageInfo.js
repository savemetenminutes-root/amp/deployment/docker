'use strict';

class ClassSeleniumXImageInfo
{
    constructor() {
        this.image = null;
        this.uniqueColors = [];
        this.uniqueColorsTransparency = [];
        this.whitePixels = 0;
        this.blackPixels = 0;
        this.redPixels = 0;
        this.greenPixels = 0;
        this.bluePixels = 0;
        this.transparentPixels = 0;
        this.width = 0;
        this.height = 0;
        this.totalPixels = 0;
    }

    getUniqueColorsCount() {
        let count = 0;
        for (let i in this.uniqueColors) {
            if(this.uniqueColors.hasOwnProperty(i)) {
                count++;
            }
        }
        return count;
    }
    getWhitenessPercentage() {
        return ((this.whitePixels / this.totalPixels)*100);
    }
}

module.exports = ClassSeleniumXImageInfo;