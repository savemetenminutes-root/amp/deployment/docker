'use strict';

const Exception = require('./Exception');
const ExceptionHandler = require('./ExceptionHandler');
const EventEmitter = require('./EventEmitter');
const Task = require('./Task');
const Message = require('./Message/Message');
const MessageException = require('./Message/MessageException');
const Output = require('./Output');

class System
{
    static get Exception() {
        return Exception;
    }

    static get ExceptionHandler() {
        return ExceptionHandler;
    }

    static get EventEmitter() {
        return EventEmitter;
    }

    static get Task() {
        return Task;
    }

    static get Message() {
        return Message;
    }

    static get Output() {
        return Output;
    }

    get exceptionHandler() {
        return this._exceptionHandler;
    }

    set exceptionHandler(value) {
        this._exceptionHandler = value;
    }

    get eventEmitter() {
        return this._eventEmitter;
    }

    set eventEmitter(value) {
        this._eventEmitter = value;
    }

    get output() {
        return this._output;
    }

    set output(value) {
        this._output = value;
    }

    get environment() {
        return this._environment;
    }

    set environment(value) {
        this._environment = value;
    }

    constructor() {
        this._exceptionHandler = new ExceptionHandler();
        this._eventEmitter = new EventEmitter();
        this._output = new Output();

        this._environment = {
            isWindows: process.platform === 'win32',
        };

        this.eventEmitter.onEvents(
            ['pre', 'init'],
            this.processMessage(new Message(System, Message.Type.INIT, 'Booting the test engine...')),
        );

        this.eventEmitter.onEvents(
            ['post', 'init'],
            this.processMessage(new Message(System, Message.Type.INIT, 'Done.')),
        );
    }

    processMessage(message) {
        //this.output.out(this, message.type, message.properties);
    }

    addCommonEventListeners() {


        this.eventEmitter.onEvents(
            ['pre', 'get', 'current', 'url'],
            async () => {
                let oldUrl = this.state.currentUrl;
                let currentUrl = await this.driver.getCurrentUrl();
                if (oldUrl !== currentUrl) {
                    this.out(ClassSeleniumXOutputType.STATE, 'The current URL is now: ' + currentUrl);
                }
            },
        );

        this.eventEmitter.onEvents(
            ['pre', 'keypress'],
            () => {
                this.out(ClassSeleniumXOutputType.INPUT, 'Press any key to continue...');
            },
            EventEmitter.ClassCondition.SUBSET,
        );

        this.eventEmitter.onEvents(
            ['pre', 'do', 'basic', 'tests'],
            (pageName) => {
                this.out(ClassSeleniumXOutputType.TEST, 'Running the basic test on the ' + pageName + ' page...');
            },
            EventEmitter.ClassCondition.SUBSET,
        );
    }
}