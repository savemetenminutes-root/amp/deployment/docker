'use strict';

const Selenium = require('selenium-webdriver');
const Capabilities = require('selenium-webdriver/lib/capabilities');
const Chrome = require('selenium-webdriver/chrome');
const Logging = require('selenium-webdriver/lib/logging');
const Input = require('selenium-webdriver/lib/input');
const Until = require('selenium-webdriver/lib/until');
const WebDriver = require('selenium-webdriver/lib/webdriver');

const ClassSeleniumXConfig = require('../../config/SeleniumXConfig');
const ClassSeleniumXRoutes = require('../../config/SeleniumXRoutes');

class StaticClassSeleniumX {
    static get Selenium() {
        return Selenium;
    }

    static get WebDriver() {
        return WebDriver;
    }

    static get Until() {
        return Until;
    }

    static get Input() {
        return Input;
    }

    static get Capabilities() {
        return Capabilities;
    }

    static get Chrome() {
        return Chrome;
    }

    static get Logging() {
        return Logging;
    }

    static get ClassSeleniumXConfig() {
        return ClassSeleniumXConfig;
    }

    static get ClassSeleniumXRoutes() {
        return ClassSeleniumXRoutes;
    }
}

module.exports = StaticClassSeleniumX;