'use strict';

const State = require('./State');
const Selector = require('./Selector');

class Element
{
    static get State() {
        return State;
    }

    static get Selector() {
        return Selector;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get selectors() {
        return this._selectors;
    }

    set selectors(value) {
        this._selectors = value;
    }

    get behavior() {
        return this._behavior;
    }

    set behavior(value) {
        this._behavior = value;
    }

    constructor(name, selectors, behavior) {
        this._name = name;
        this._selectors = selectors;
        this._behavior = behavior;
    }
}

exports = Element;