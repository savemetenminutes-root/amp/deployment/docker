'use strict';

const Message = require('./Message');

class MessageException extends Message
{
    get severity() {
        return this._severity;
    }

    set severity(value) {
        this._severity = value;
    }

    constructor(type, text, severity) {
        super(type, text);
        this._severity = severity;
    }
}

exports = MessageException;