'use strict';

const Route = require('./Route');
const Page = require('./Page');
const Element = require('./Element');

class Web
{
    static get Route() {
        return Route;
    }

    static get Page() {
        return Page;
    }

    static get Element() {
        return Element;
    }
}