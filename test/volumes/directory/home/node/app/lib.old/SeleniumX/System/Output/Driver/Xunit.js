'use strict';

const ClassSeleniumXOutput = require('./SeleniumXOutput');

class ClassSeleniumXOutputXunit {
    constructor(config) {
        this.config = config;
    }

    out(type, messages) {
        messages.forEach(
            (message) => {
                let preText = '';
                let postText = '';
                switch (type) {
                    case ClassSeleniumXOutputType.INFO:
                        preText = 'Info: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ACTION:
                        preText = 'Action: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.TEST:
                        preText = 'TEST: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.CHECK:
                        preText = 'Check: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.PASSED:
                        preText = 'Passed: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.SUCCESS:
                        preText = 'SUCCESS: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.INIT:
                        preText = 'Init: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ENVIRONMENT:
                        preText = '<<<ENV>>> ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.PLAIN:
                    default:
                        preText = '';
                        postText = '';
                        break;
                }
                console.log(preText + message + postText);
            }
        )
    }
}

module.exports = ClassSeleniumXOutputXunit;