'use strict';

const Exception = require('./Exception');

class ExceptionHandler
{
    static throwException(severity, type, message, code, callback, stack) {
        throw new Exception(severity, type, message, code, callback, stack);
    }
}

exports = ExceptionHandler;