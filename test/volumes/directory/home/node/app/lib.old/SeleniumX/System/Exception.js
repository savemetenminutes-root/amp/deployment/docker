'use strict';

const NOTICE = 1;
const WARNING = 2;
const RECOVERABLE = 4;
const FATAL = 8;
class ClassSeverity {
    static get NOTICE() {
        return NOTICE;
    }

    static get WARNING() {
        return WARNING;
    }

    static get RECOVERABLE() {
        return RECOVERABLE;
    }

    static get FATAL() {
        return FATAL;
    }
}

const RUNTIME = 1;
const DRIVER = 2;
const BROWSER = 4;
const TEST = 8;
class ClassType {
    static get ENGINE() {
        return ENGINE;
    }

    static get RUNTIME() {
        return RUNTIME;
    }

    static get DRIVER() {
        return DRIVER;
    }

    static get BROWSER() {
        return BROWSER;
    }

    static get TEST() {
        return TEST;
    }
}

class ClassSeleniumXException extends Error
{
    constructor(severity, type, message, code, callback, stack) {
        super();

        this._severity = severity;
        this._type = type;
        this._message = message;
        this._code = code;
        this._callback = callback;
        this._stack = stack;
    }

    get severity() {
        return this._severity;
    }

    get type() {
        return this._type;
    }

    get message() {
        return this._message;
    }

    get code() {
        return this._code;
    }

    get callback() {
        return this._callback;
    }

    get stack() {
        return this._stack;
    }

    static get ClassSeverity() {
        return ClassSeverity;
    }

    static get ClassType() {
        return ClassType;
    }
}

module.exports = ClassSeleniumXException;