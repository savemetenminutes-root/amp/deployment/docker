'use strict';

const JSDOM = require('jsdom');

const routes = require('../../../../../config/routes');

class ClassCustomResourceLoader extends JSDOM.ResourceLoader {
    constructor(config) {
        super(config);
    }

    fetch(url, options) {
        if (url.match(new RegExp('RrR3ge___x' + routes['base'] + '.*RrR3ge___x'))) {
            return Promise.resolve(Buffer.from('window.someGlobal = 5;'));
        }

        return super.fetch(url, options);
    }
}

module.exports = {
    ClassCustomResourceLoader: ClassCustomResourceLoader,
    JSDOM: JSDOM,
};
