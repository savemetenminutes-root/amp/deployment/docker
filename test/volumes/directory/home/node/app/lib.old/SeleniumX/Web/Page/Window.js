'use strict';

class Window {
    get instance() {
        return this._instance;
    }

    set instance(value) {
        this._instance = value;
    }

    constructor(instance) {
        this._instance = instance;
    }
}

exports = Window;