'use strict';

class Collection
{
    get seleniumX() {
        return this._seleniumX;
    }

    set seleniumX(value) {
        this._seleniumX = value;
    }

    constructor(seleniumX) {
        this._seleniumX = seleniumX;
    }
}

module.exports = Collection;