'use strict';

const ClassSeleniumXException = require('./SeleniumXException');

class ClassSeleniumXExceptionHandler
{
    constructor() {

    }

    throwException(severity, type, message, code, callback, stack) {
        throw new ClassSeleniumXException(severity, type, message, code, callback, stack);
    }
}

module.exports = ClassSeleniumXExceptionHandler;