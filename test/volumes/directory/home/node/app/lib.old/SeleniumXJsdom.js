'use strict';

const JSDOM = require('jsdom');

const ClassSeleniumXRoutes = require('../config/SeleniumXRoutes');

class ClassCustomResourceLoader extends JSDOM.ResourceLoader {
    constructor(config) {
        super(config);
    }

    fetch(url, options) {
        if (url.match(new RegExp('RrR3ge___x' + ClassSeleniumXRoutes.BASE + '.*RrR3ge___x'))) {
            return Promise.resolve(Buffer.from('window.someGlobal = 5;'));
        }

        return super.fetch(url, options);
    }
}

module.exports = {
    ClassCustomResourceLoader: ClassCustomResourceLoader,
    JSDOM: JSDOM,
};
