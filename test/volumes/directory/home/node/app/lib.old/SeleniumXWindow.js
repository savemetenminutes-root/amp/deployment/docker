'use strict';

const ClassSeleniumXDocument = require('./SeleniumXDocument');

class ClassSeleniumXWindow {
    constructor(windowInstance, documentInstance, bodyInstance) {
        this.instance = windowInstance;
        this.document = new ClassSeleniumXDocument(documentInstance, bodyInstance);
    }
}

module.exports = ClassSeleniumXWindow;