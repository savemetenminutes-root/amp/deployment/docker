class ClassU
{
    cdump() {
        var casper = require('casper').create();
        casper.utils = require('utils');
        var properties = {
            '': 'iterate',
            'global': 'iterate',
            'phantom': 'iterate',
            //'casper': 'module-dump', // casperjs casper module
            //'clientutils': 'module-dump', // casperjs clientutils module
            //'colorizer': 'module-dump', // casperjs colorizer module
            //'mouse': 'module-dump', // casperjs mouse module
            //'tester': 'module-dump', // casperjs tester module
            //'utils': 'module-dump', // casperjs utils module
            //'webpage': 'module-dump', // phantomjs webpage module
            //'child_process': 'module-dump', // phantomjs child process module
            //'fs': 'module-dump', // phantomjs file system module
            //'system': 'module-dump', // phantomjs system module
            //'webserver': 'module-dump', // phantomjs webserver module
        };
        for (var property in properties) {
            if (properties.hasOwnProperty(property)) {
                var path = property.split('.');
                if (properties[property] === 'module-dump') {
                    casper.utils.dump(require(property));
                } else {
                    var currentElement = window;
                    var currentKey = path.shift();
                    while (path.length && (typeof (currentKey) !== 'undefined') && (currentKey !== '')) {
                        currentElement = currentElement[currentKey];
                        currentKey = path.shift();
                    }
                    casper.echo((property !== '' ? property : 'window'), 'PARAMETER');
                    if (properties[property] === 'dump') {
                        casper.utils.dump(currentElement);
                    } else {
                        for (var i in currentElement) {
                            if (currentElement.hasOwnProperty(i)) {
                                casper.echo('    ' + i);
                            }
                        }
                    }
                }
            }
        }
    }

    dumpself() {
        var properties = {
            '': 'iterate',
            'options': 'iterate',
            'requestUrl': 'dump',
            'requestData': 'dump',
            'currentUrl': 'dump',
            'currentHTTPStatus': 'dump',
            'currentResponse': 'dump',
            'resources': 'dump',
            'result': 'dump',
            'navigationRequested': 'dump',
            'page': 'iterate',
            'step': 'dump',
            '_events': 'dump',
        };
        for (var property in properties) {
            if (properties.hasOwnProperty(property)) {
                var path = property.split('.');
                var currentElement = this;
                var currentKey = path.shift();
                while (path.length && (typeof (currentKey) !== 'undefined') && (currentKey !== '')) {
                    currentElement = currentElement[currentKey];
                    currentKey = path.shift();
                }
                this.echo('casper' + (property !== '' ? '.' : '') + property, 'PARAMETER');
                if (properties[property] === 'dump') {
                    this.utils.dump(currentElement);
                } else {
                    for (var i in currentElement) {
                        if (currentElement.hasOwnProperty(i)) {
                            this.echo('    ' + i);
                        }
                    }
                }
            }
        }
    }

    vdump(variable, path, level, file, line) {
        let type = typeof variable[1];
        level = level ? level : 1;
        let currentPath = [];
        if (path) {
            for (let e in path) {
                if (path.hasOwnProperty(e)) {
                    currentPath.push(path[e]);
                }
            }
        }
        currentPath.push(variable[0]);
        //console.log(currentFilename + ':');
        let indent = '';
        for (let i = 1; i < level; i++) {
            indent += '    ';
        }
        if (level === 1) {
            console.log('DUMP @ ' + file + '(' + line + ') -----------------------------');
        }
        if (type === 'object') {
            console.log(indent + currentPath.join('.') + ': ' + '(' + type + ') {');
            for (let j in variable[1]) {
                if (variable[1].hasOwnProperty(j)) {
                    this.vdump([j, variable[1][j]], currentPath, level + 1, null, null);
                }
            }
            console.log(indent + '}');
        } else {
            if (type === 'function') {
                console.log(indent + currentPath.join('.') + ': ' + type);
            } else {
                console.log(indent + currentPath.join('.') + ': ' + type + '(' + variable[1] + ')');
            }
        }
        if (level === 1) {
            console.log('------------------------------------------------------------------------');
        }
    }

    static currentFile() {
        var system = require('system');
        return system.args[3];
    }
}

module.exports = ClassU;