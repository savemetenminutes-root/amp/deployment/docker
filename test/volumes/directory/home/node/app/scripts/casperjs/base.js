//phantom.injectJs('./functions.js'); // JS can also be imported using the phantomjs instance
var require = patchRequire(require);

var utils = require('utils');
var ClassUtils = require('utils').Utils;
class U extends ClassUtils {
    constructor(config) {
        super();
    }

    cdump() {
        var casper = require('casper').create();
        casper.utils = require('utils');
        var properties = {
            '': 'iterate',
            'global': 'iterate',
            'phantom': 'iterate',
            //'casper': 'module-dump', // casperjs casper module
            //'clientutils': 'module-dump', // casperjs clientutils module
            //'colorizer': 'module-dump', // casperjs colorizer module
            //'mouse': 'module-dump', // casperjs mouse module
            //'tester': 'module-dump', // casperjs tester module
            //'utils': 'module-dump', // casperjs utils module
            //'webpage': 'module-dump', // phantomjs webpage module
            //'child_process': 'module-dump', // phantomjs child process module
            //'fs': 'module-dump', // phantomjs file system module
            //'system': 'module-dump', // phantomjs system module
            //'webserver': 'module-dump', // phantomjs webserver module
        };
        for (var property in properties) {
            if (properties.hasOwnProperty(property)) {
                var path = property.split('.');
                if (properties[property] === 'module-dump') {
                    casper.utils.dump(require(property));
                } else {
                    var currentElement = window;
                    var currentKey = path.shift();
                    while (path.length && (typeof (currentKey) !== 'undefined') && (currentKey !== '')) {
                        currentElement = currentElement[currentKey];
                        currentKey = path.shift();
                    }
                    casper.echo((property !== '' ? property : 'window'), 'PARAMETER');
                    if (properties[property] === 'dump') {
                        casper.utils.dump(currentElement);
                    } else {
                        for (var i in currentElement) {
                            if (currentElement.hasOwnProperty(i)) {
                                casper.echo('    ' + i);
                            }
                        }
                    }
                }
            }
        }
    }

    dumpself() {
        var properties = {
            '': 'iterate',
            'options': 'iterate',
            'requestUrl': 'dump',
            'requestData': 'dump',
            'currentUrl': 'dump',
            'currentHTTPStatus': 'dump',
            'currentResponse': 'dump',
            'resources': 'dump',
            'result': 'dump',
            'navigationRequested': 'dump',
            'page': 'iterate',
            'step': 'dump',
            '_events': 'dump',
        };
        for (var property in properties) {
            if (properties.hasOwnProperty(property)) {
                var path = property.split('.');
                var currentElement = this;
                var currentKey = path.shift();
                while (path.length && (typeof (currentKey) !== 'undefined') && (currentKey !== '')) {
                    currentElement = currentElement[currentKey];
                    currentKey = path.shift();
                }
                this.echo('casper' + (property !== '' ? '.' : '') + property, 'PARAMETER');
                if (properties[property] === 'dump') {
                    this.utils.dump(currentElement);
                } else {
                    for (var i in currentElement) {
                        if (currentElement.hasOwnProperty(i)) {
                            this.echo('    ' + i);
                        }
                    }
                }
            }
        }
    }

    static vdump(line, variable, level, path) {
        var system = require('system');
        var currentFilename = system.args[3];
        var type = typeof variable[1];
        level = level ? level : 1;
        var currentPath = [];
        if (path) {
            for (var e in path) {
                if (path.hasOwnProperty(e)) {
                    currentPath.push(path[e]);
                }
            }
        }
        currentPath.push(variable[0]);
        //console.log(currentFilename + ':');
        var indent = '';
        for (var i = 1; i < level; i++) {
            indent += '    ';
        }
        if (level === 1) {
            console.log('DUMP @ ' + currentFilename + '(' + line + ') -----------------------------');
        }
        if (type === 'object') {
            console.log(indent + currentPath.join('.') + ': ' + '(' + type + ') {');
            for (var j in variable[1]) {
                if (variable[1].hasOwnProperty(j)) {
                    this.vdump(null, [j, variable[1][j]], level + 1, currentPath);
                }
            }
            console.log(indent + '}');
        } else {
            if (type === 'function') {
                console.log(indent + currentPath.join('.') + ': ' + type);
            } else {
                console.log(indent + currentPath.join('.') + ': ' + type + '(' + variable[1] + ')');
            }
        }
        if (level === 1) {
            console.log('------------------------------------------------------------------------');
        }
    }

    static currentFile() {
        var system = require('system');
        return system.args[3];
    }
}

var ClassCasper = require('casper').Casper;
class C extends ClassCasper {
    constructor() {
        super(
            {
                //timeout: 5000, // script timeout
                //stepTimeout: 3000,
                clientScripts: [
                    //'includes/jquery.js',      // These two scripts will be injected in remote
                    //'includes/underscore.js'   // DOM on every request
                ],
                pageSettings: {
                    loadImages: true,
                    loadResources: true, // custom, provided by createCasper() - skips loading of all assets requested by the page
                    loadPlugins: false,
                    resourceTimeout: 4000,
                },
                logLevel: "debug",
                verbose: true,
            }
        );

        this.utils = new U;
    }
}

module.exports.createCasper = function(config) {
    var casper = new C;
    // http://google.com:81 // test timeout url
    // https://my-s.adgoisdjoijfdsoigjhaftpflichthelden.de:16200/ // test error url

    casper.on('error', function (msg, backtrace) {
        //this.echo('Resource ' + request.url + ' timed out after ' + this.options.pageSettings.resourceTimeout + 'ms', 'RED_BAR');
        //casper.page = casper.newPage(); // cancel the current request and move on. without this the script hangs waiting for the resource indefinitely
        //this.die('Could not load ' + request.url, 1);
        this.echo('Uncaught error: ' + msg);
    });
    /*
    casper.options.timeout = config.scriptTimeoutMs;
    */
    if (config.timeout) {
        casper.on('timeout', function () {
            this.die('The script timed out after ' + this.options.timeout + 'ms');
        });
    }
    /*
    casper.options.stepTimeout = config.stepTimeoutMs;
    */
    if (config.stepTimeout) {
        casper.options.onStepTimeout = function () {
            this.echo('The step which loads resource ' + this.requestUrl + ' timed out after ' + this.options.stepTimeout + 'ms', 'RED_BAR');
        };
    }

    casper.on('resource.error', function (resourceError) {
        if(resourceError.url !== '') { // fix for the loadResources=false functionality
            this.echo('Resource ' + request.url + ' failed with: ' + request.errorString + '(' + request.errorCode + ')', 'RED_BAR');
            //casper.page = casper.newPage(); // cancel the current request and move on. without this the script hangs waiting for the resource indefinitely
            this.die('Could not load ' + request.url, 1);
        }
    });
    casper.on('resource.timeout', function (request) {
        this.echo('Resource ' + request.url + ' timed out after ' + this.options.pageSettings.resourceTimeout + 'ms', 'RED_BAR');
        //casper.page = casper.newPage(); // cancel the current request and move on. without this the script hangs waiting for the resource indefinitely
        this.die('Could not load ' + request.url, 1);
    });
    if (!config.loadResources) {
        casper.on('resource.requested', function (requestData, request) {
            //casper.echo('Requested: ' + requestData.url);
            //casper.echo('Skipped loading: ' + requestData.url);
            // fetch only the requested page HTML (first request)
            if (requestData.url !== homeUrl) {
                //casper.utils.dump(request);
                request.abort(); // this always triggers a resource.error event
            }
        });
    }

    casper.ping = function(url) {
        var pinger = require('casper').create({
            //timeout: 5000, // script timeout
            //stepTimeout: 3000,
            clientScripts: [
                //'includes/jquery.js',      // These two scripts will be injected in remote
                //'includes/underscore.js'   // DOM on every request
            ],
            pageSettings: {
                loadImages: true,
                loadPlugins: false,
                resourceTimeout: 4000,
            },
            logLevel: "debug",
            verbose: true,
        });

        pinger.utils = xutils;
        pinger.on('error', function (msg, backtrace) {
            //this.echo('Resource ' + request.url + ' timed out after ' + this.options.pageSettings.resourceTimeout + 'ms', 'RED_BAR');
            //casper.page = casper.newPage(); // cancel the current request and move on. without this the script hangs waiting for the resource indefinitely
            //this.die('Could not load ' + request.url, 1);
            this.echo('Uncaught error: ' + msg);
        });

        pinger.on('resource.error', function (resourceError) {
            if(resourceError.url !== '') { // fix for the loadResources=false functionality
                //this.currentPage.resourcesErrors.push(resourceError);
                this.echo('Resource ' + request.url + ' failed with: ' + request.errorString + '(' + request.errorCode + ')', 'RED_BAR');
                //casper.page = casper.newPage(); // cancel the current request and move on. without this the script hangs waiting for the resource indefinitely
                //this.die('Could not load ' + request.url, 1);
            }
        });
        pinger.on('resource.timeout', function (request) {
            //this.currentPage.resourcesErrors.push(request);
            this.echo('Resource ' + request.url + ' timed out after ' + this.options.pageSettings.resourceTimeout + 'ms', 'RED_BAR');
        });
        pinger.on('resource.requested', function (requestData, request) {
            //casper.echo('Requested: ' + requestData.url);
            //casper.echo('Skipping resource: ' + requestData.url);
            //this.utils.vdump(244, ['this.page', this.page]);
            //this.echo('requestData.url: ' + requestData.url + ' <=> ' + 'this.page.url: ' + this.page.url);
            if (this.page.url) {
                this.echo('Skipping resource: ' + requestData.url);
                request.abort();
                this.currentPage.resourcesSkipped.push(
                    {
                        'request': request,
                        'requestData': requestData,
                    }
                );
                //casper.utils.dump(request);
                request.abort();
                // Resource  failed with: Protocol "" is unknown(301)
            }
        });
        pinger.on('page.resource.requested', function (requestData, request) {
            this.echo('Pinging: ' + requestData.url);
                //this.utils.dump(this);
            this.currentPage = {};
            this.currentPage.resourcesSkipped = [];
            this.currentPage.resourcesErrors = [];
            this.currentPage.resourcesTimeouts = [];
        });
        pinger.on('page.resource.received', function (responseData) {
            //this.utils.dump(responseData);
            this.echo('Page ' + url + ' loaded.');
            this.echo('Response stage: ' + responseData.stage);
            this.echo('Body length (characters): ' + this.page.content.length);
            this.echo('Status code: ' + responseData.status);
            this.echo('Resources:');
            //this.utils.vdump(267, ['this', this]);
            //this.utils.vdump(267, ['this.currentPage', this.currentPage]);
            this.echo('    skipped: ' + this.currentPage.resourcesSkipped.length);
            this.echo('    errors: ' + this.currentPage.resourcesErrors.length);
            this.echo('    timeouts: ' + this.currentPage.resourcesTimeouts.length);
        });
        pinger.on('page.created', function() {
            //this.echo('PAGE CREATED');
        });
        var s = new Date();
        pinger.start(url, function() {
            var e = new Date();
            this.echo('The ping operation took ' + (e.getTime() - s.getTime()) + 'ms to complete.');
        });
        pinger.run();
    };

    return casper;
};





/*
casper.on('page.created', function() {
    casper.page.onResourceRequested = function(requestData, request) {
        casper.echo('Aborting request for resource ' + requestData.url);
        request.abort();
    };
});
casper.on('resource.error', function(resourceError) {
    casper.utils.dump(resourceError);
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('page.resource.requested', function(requestData, request) {
    casper.echo('Page requested: ' + requestData.url);
    if(requestData.url !== homeUrl) {
        casper.utils.dump(request);
        request.abort(); // fetch only the requested page HTML (first request)
    }
});
casper.options.onLoadError = function(c, requestUrl, status) {
    casper.echo(requestUrl + ' loading failed with status: ' + status);
};
casper.on('error', function(resourceError) {
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('load.failed', function(resourceError) {
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('resource.error', function(resourceError) {
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('page.error', function(resourceError) {
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('step.error', function(resourceError) {
    casper.echo(resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ')');
});
casper.on('step.complete', function(parameter) {
    casper.echo('Step complete.');
});
casper.on('resource.timeout', function(requestData, request) {
    casper.echo('Res ' + requestData.url + ' timed out');
});
casper.on('page.created', function() {
    casper.page.onResourceTimeout = function(request) {
        casper.echo('Resource ' + request.url + ' timed out after ' + casper.options.pageSettings.resourceTimeout + 'ms');
    }
});
var casper = require('casper').create({
    clientScripts:  [
        //'includes/jquery.js',      // These two scripts will be injected in remote
        //'includes/underscore.js'   // DOM on every request
    ],
    pageSettings: {
        loadImages:  false,
        loadPlugins: false,
        resourceTimeout: resourceTimeoutMs,
    },
    page: {
        onResourceTimeout: function(request) {
            casper.echo('Resource ' + request.url + ' timed out after ' + resourceTimeoutMs + 'ms');
        }
    },
//stepTimeout: stepTimeoutMs,
logLevel: "debug",
    verbose: true
});
*/