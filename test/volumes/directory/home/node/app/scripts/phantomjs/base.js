module.exports.dump = function(line, variable, level, path) {
    var system = require('system');
    var currentFilename = system.args[0];
    var type = typeof variable[1];
    level = level ? level : 1;
    var currentPath = [];
    if(path) {
        for (var e in path) {
            if (path.hasOwnProperty(e)) {
                currentPath.push(path[e]);
            }
        }
    }
    currentPath.push(variable[0]);
    //console.log(currentFilename + ':');
    var indent = '';
    for(var i = 1; i < level; i++) {
        indent += '    ';
    }
    if(level === 1) {
        console.log('DUMP @ ' + currentFilename + '(' + line + ') -----------------------------');
    }
    if(type !== 'object') {
        if(type === 'function') {
            console.log(indent + currentPath.join('.') + ': ' + type);
        } else {
            console.log(indent + currentPath.join('.') + ': ' + type + '(' + variable[1] + ')');
        }
    } else {
        console.log(indent + currentPath.join('.') + ': ' + '(' + type + ') {');
        for(var j in variable[1]) {
            if(variable[1].hasOwnProperty(j)) {
                this.dump(null, [j, variable[1][j]], level + 1, currentPath);
            }
        }
        console.log(indent + '}');
    }
    if(level === 1) {
        console.log('------------------------------------------------------------------------');
    }
};

module.exports.ping = function(url) {
    if(!url) {
        console.log('Error: `base.ping(url)` - you must specify an URL.');
        exit(1);
    }

    var page = require('webpage').create();
    page.viewportSize = {
        width: 1920,
        height: 1080,
    };
    page.settings.userAgent = 'Phantom.js bot';
    page.onResourceRequested = function(requestData, request) {
        //base.dump(52, ['request', request]);
        //this.requests[request.id] = request;
        if(requestData.url !== this.url) {
            console.log('Skipping resource: ' + requestData.url);
            request.abort(); // This always triggers an onResourceError event
        }
    };
    page.onResourceError = function(resourceError) {
        if(resourceError.url !== '') {
            console.log('Resource ' + resourceError.url + ' failed with: ' + resourceError.errorString + '(' + resourceError.errorCode + ').');
        }
    };
    page.onResourceTimeout = function(request) {
        console.log('Resource ' + request.url + ' timed out.');
    };
    page.settings.resourceTimeout = 2000;
    var s = new Date;
    page.requests = [];
    page.open(url, function(status) {
        //base.dump(17, ['this', this]);
        var e = new Date;
        if (status === "success") {
            console.log('Fetched ' + page.url + ' in ' + (e.getTime() - s.getTime()) + 'ms');
            console.log(this.content);
            //base.dump(81, ['page', page]);
            phantom.exit(0);
            //var framesNames = page.framesName;
            //base.dump(41, ['framesNames', framesNames]);
            /*
            return window.setTimeout((function() {
                JSON.decode(page.renderBase64());
                return next(status, url, file);
            }), 200);
            */
        } else {
            console.log('Failed fetching ' + this.url + ' with status (' + status + ')');
            phantom.exit(1);
        }
    });
};