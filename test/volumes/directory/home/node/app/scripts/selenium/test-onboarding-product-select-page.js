const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingProductSelectPage {
    static async main(seleniumX) {
        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT,
            'onboarding product select',
            true,
        );

        await seleniumX.doBasicTests('onboarding product select');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select-page', 'begin'], [seleniumX]);

        let onboardingProductSelectPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pair'),
                    'Check: Looking for the personal liability product link on the onboarding product select page...',
                ],
                'Found the personal liability product link on the onboarding product select page.',
                //'>>>>>> Error: No personal liability product link found on the onboarding product select page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-fill8'),
                    'Check: Looking for the dog liability product link on the onboarding product select page...',
                ],
                'Found the dog liability product link on the onboarding product select page.',
                //'>>>>>> Error: No dog liability product link found on the onboarding product select page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pferde'),
                    'Check: Looking for the horse liability product link on the onboarding product select page...',
                ],
                'Found the horse liability product link on the onboarding product select page.',
                //'>>>>>> Error: No horse liability product link found on the onboarding product select page.',
            ),
        ];
        let onboardingProductSelectPageElements = await seleniumX.runAsync(
            onboardingProductSelectPageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingProductSelectPageElementsTasks,
            onboardingProductSelectPageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding product select page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding product select page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingProductSelectPage;