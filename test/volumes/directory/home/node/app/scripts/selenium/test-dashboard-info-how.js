const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardInfoHow {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INFO_HOW,
            'dashboard info how',
            true,
        );

        await seleniumX.doBasicTests('dashboard info how');

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-how', 'content', 'begin'], [seleniumX]);

        let inviteButton = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.linkText('JETZT HELDEN CODE TEILEN'));
        await seleniumX.click(inviteButton);

        if(await seleniumX.getCurrentUrl() !== StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INVITE) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'Button "JETZT HELDEN CODE TEILEN" from info how page does not redirect to the Dashboard Invite page!',
            );
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-how', 'content', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-how', 'content', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Info How page functionality is working properly.', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-how', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated that the Dashboard Info How page functionality is working properly.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardInfoHow;

