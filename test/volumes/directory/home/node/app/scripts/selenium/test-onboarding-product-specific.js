let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingProductSpecific {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SPECIFIC,
            'onboarding product specific',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'begin'], [seleniumX]);

        let onboardingProductSpecificPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Wer soll versichert werden?')]"),
                    null,
                ],
                null,
                '>>>>>> Error: The heading is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Wie heißt dein Pferd?')]"),
                    null,
                ],
                null,
                '>>>>>> Error: The horse name text field label is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Wie heißt dein Pferd?')]/../input[contains(@type, 'text')]"),
                    null,
                ],
                null,
                '>>>>>> Error: The horse name text field is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Weiteres Pferd hinzufügen'),
                    null,
                ],
                null,
                '>>>>>> Error: The add another horse button is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('WEITER'),
                    null,
                ],
                null,
                '>>>>>> Error: No submit buttons found on the onboarding product specific page.',
            ),
        ];
        let onboardingProductSpecificPageElements = await seleniumX.runAsync(
            onboardingProductSpecificPageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingProductSpecificPageElementsTasks,
            onboardingProductSpecificPageElements
        );

        let emptyDataValidationTest = await seleniumX.formTest(
            [
                [onboardingProductSpecificPageElements[2], StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE],
            ],
            onboardingProductSpecificPageElements[4][0],
            () => {
                seleniumX.eventEmitter.emitEvents(['test-onboarding-product-specific', 'action', 'click', 'submit', 'button', 'empty', 'data', 'onboarding product specific', 'page'], [seleniumX]);
                return seleniumX.driver.wait(new StaticClassSeleniumX.WebDriver.Condition('to see if the onboarding product specific form validation works with empty values',
                    async() => {
                        let existsElement = await seleniumX.existsElementAsync(
                            StaticClassSeleniumX.Selenium.By.xpath(
                                "//div[contains(string(), 'Bitte gib die Namen deiner Pferde ein.')]",
                            ),
                        );
                        return !!existsElement;
                    }
                ), 2000);
            }
        );

        await seleniumX.sendKeys(
            onboardingProductSpecificPageElements[2],
            StaticClassSeleniumX.Selenium.Key.CONTROL,
            'a',
            StaticClassSeleniumX.Selenium.Key.NULL,
            StaticClassSeleniumX.Selenium.Key.DELETE,
            'test horse 0'
        );
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'action', 'click', 'add another horse', 'button', 'onboarding product specific', 'page'], [seleniumX]);
        await seleniumX.click(onboardingProductSpecificPageElements[3]);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'check', 'wait', 'for', 'test horse 0', 'list', 'item', 'onboarding product specific', 'page'], [seleniumX]);
        let testHorse0ListItem = await seleniumX.driver.wait(
            new StaticClassSeleniumX.WebDriver.Condition(
                'to see if the test horse 0 list item will appear',
                async() => {
                    let existsElement = await seleniumX.existsElementAsync(
                        StaticClassSeleniumX.Selenium.By.xpath(
                            "//ul[contains(@class, 'provide-list')]/li[contains(string(), 'test horse 0')]",
                        ),
                    );
                    if(existsElement) {
                        return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//ul[contains(@class, 'provide-list')]/li[contains(string(), 'test horse 0')]"));
                    }

                    return false;
                }
            ),
            2000
        );
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'passed', 'wait', 'for', 'test horse 0', 'list', 'item', 'onboarding product specific', 'page'], [seleniumX]);

        /*
        await seleniumX.sendKeys(
            onboardingProductSpecificPageElements[2],
            StaticClassSeleniumX.Selenium.Key.CONTROL,
            'a',
            StaticClassSeleniumX.Selenium.Key.NULL,
            StaticClassSeleniumX.Selenium.Key.DELETE,
            'test horse 1'
        );
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'action', 'click', 'add another horse', 'button', 'onboarding product specific', 'page'], [seleniumX]);
        await seleniumX.click(onboardingProductSpecificPageElements[3]);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'check', 'wait', 'for', 'test horse 1', 'list', 'item', 'onboarding product specific', 'page'], [seleniumX]);
        let testHorse1ListItem = await seleniumX.driver.wait(
            new StaticClassSeleniumX.WebDriver.Condition(
                'to see if the test horse 1 list item will appear',
                async() => {
                    let existsElement = await seleniumX.existsElementAsync(
                        StaticClassSeleniumX.Selenium.By.xpath(
                            "//ul[contains(@class, 'provide-list')]/li[contains(string(), 'test horse 1')]",
                        ),
                    );
                    if(existsElement) {
                        return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//ul[contains(@class, 'provide-list')]/li[contains(string(), 'test horse 1')]"));
                    }

                    return false;
                }
            ),
            2000
        );
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'passed', 'wait', 'for', 'test horse 1', 'list', 'item', 'onboarding product specific', 'page'], [seleniumX]);
        */

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'action', 'click', 'submit', 'button', 'valid', 'data', 'onboarding product specific', 'page'], [seleniumX]);
        await seleniumX.click(onboardingProductSpecificPageElements[4][0]);

        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1), 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding product specific page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'action', 'click', 'submit', 'button', 'empty', 'data', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the submit button after providing valid data on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'action', 'click', 'add another horse', 'button', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the add another horse button on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'check', 'wait', 'for', 'test horse 0', 'list', 'item', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Waiting for the test horse 0 list item to appear after clicking on the add another horse button on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'passed', 'wait', 'for', 'test horse 0', 'list', 'item', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The test horse 0 list item appears correctly after clicking on the add another horse button on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'check', 'wait', 'for', 'test horse 1', 'list', 'item', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Waiting for the test horse 1 list item to appear after clicking on the add another horse button on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'passed', 'wait', 'for', 'test horse 1', 'list', 'item', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The test horse 1 list item appears correctly after clicking on the add another horse button on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'action', 'click', 'submit', 'button', 'valid', 'data', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the submit button after providing valid data on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding product specific page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingProductSpecific;