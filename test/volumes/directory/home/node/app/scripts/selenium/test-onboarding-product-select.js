let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');

const path = require('path');

class TestOnboardingProductSelect {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT,
            'onboarding product select',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select', 'begin'], [seleniumX]);

        let onboardingProductSelectPagePersonalLiabilityProductLinkTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pair'),
                    null,
                ],
                null,
                //'>>>>>> Error: No personal liability product link found on the onboarding select product page.'
            ),
        ];
        let onboardingProductSelectPagePersonalLiabilityProductLink = await seleniumX.runAsync(
            onboardingProductSelectPagePersonalLiabilityProductLinkTasks
        );
        /*
        seleniumX.validateAllResultsSimple(
            onboardingProductSelectPagePersonalLiabilityProductLinkTasks,
            onboardingProductSelectPagePersonalLiabilityProductLink
        );
        */
        if(onboardingProductSelectPagePersonalLiabilityProductLink[0]) {
            seleniumX.eventEmitter.emitEvents(['test-onboarding-product-select', 'action', 'click', 'personal liability product', 'link', 'onboarding product select', 'page'], [seleniumX]);
            await seleniumX.click(onboardingProductSelectPagePersonalLiabilityProductLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1, 2000);


            let onboardingStep1GoBackToProductSelectPageLinkTasks = [
                new ClassSeleniumXTask(
                    seleniumX.findElementAsync,
                    seleniumX,
                    [
                        StaticClassSeleniumX.Selenium.By.css('i.icon-profile'),
                        null,
                    ],
                    null,
                    //'>>>>>> Error: No go back to product select page link found on the onboarding step1 page.'
                ),
            ];
            let onboardingStep1GoBackToProductSelectPageLink = await seleniumX.runAsync(
                onboardingStep1GoBackToProductSelectPageLinkTasks
            );
            seleniumX.validateAllResultsSimple(
                onboardingStep1GoBackToProductSelectPageLinkTasks,
                onboardingStep1GoBackToProductSelectPageLink
            );
            seleniumX.eventEmitter.emitEvents(['test-onboarding-product-select', 'action', 'click', 'go back', 'link', 'onboarding step1', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep1GoBackToProductSelectPageLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT, 2000);
        }

        let onboardingProductSelectPageDogLiabilityProductLinkTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-fill8'),
                    null,
                ],
                null,
                //'>>>>>> Error: No dog liability product link found on the onboarding select product page.'
            ),
        ];
        let onboardingProductSelectPageDogLiabilityProductLink = await seleniumX.runAsync(
            onboardingProductSelectPageDogLiabilityProductLinkTasks
        );
        /*
        seleniumX.validateAllResultsSimple(
            onboardingProductSelectPageDogLiabilityProductLinkTasks,
            onboardingProductSelectPageDogLiabilityProductLink
        );
        */
        if(onboardingProductSelectPageDogLiabilityProductLink[0]) {
            seleniumX.eventEmitter.emitEvents(['test-onboarding-product-select', 'action', 'click', 'dog liability product', 'link', 'onboarding product select', 'page'], [seleniumX]);
            await seleniumX.click(onboardingProductSelectPageDogLiabilityProductLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_CONFIRM_RACE, 2000);

            let onboardingConfirmRaceNoSorryLinkTasks = [
                new ClassSeleniumXTask(
                    seleniumX.findElementAsync,
                    seleniumX,
                    [
                        StaticClassSeleniumX.Selenium.By.linkText('NEIN, SORRY'),
                        null,
                    ],
                    null,
                    '>>>>>> Error: The no sorry button was not found on product select page on the onboarding confirm race page.'
                ),
            ];
            let onboardingConfirmRaceNoSorryLink = await seleniumX.runAsync(
                onboardingConfirmRaceNoSorryLinkTasks
            );
            seleniumX.validateAllResultsSimple(
                onboardingConfirmRaceNoSorryLinkTasks,
                onboardingConfirmRaceNoSorryLink
            );
            seleniumX.eventEmitter.emitEvents(['test-onboarding-confirm-race', 'action', 'click', 'no sorry', 'link', 'confirm race', 'page'], [seleniumX]);
            await seleniumX.click(onboardingConfirmRaceNoSorryLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT, 2000);
        }

        let onboardingProductSelectPageHorseLiabilityProductLinkTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pferde'),
                    null,
                ],
                null,
                //'>>>>>> Error: No horse liability product link found on the onboarding select product page.'
            ),
        ];
        let onboardingProductSelectPageHorseLiabilityProductLink = await seleniumX.runAsync(
            onboardingProductSelectPageHorseLiabilityProductLinkTasks
        );
        /*
        seleniumX.validateAllResultsSimple(
            onboardingProductSelectPageHorseLiabilityProductLinkTasks,
            onboardingProductSelectPageHorseLiabilityProductLink
        );
        */
        if(onboardingProductSelectPageHorseLiabilityProductLink[0]) {
            seleniumX.eventEmitter.emitEvents(['test-onboarding-product-select', 'action', 'click', 'horse liability product', 'link', 'onboarding product select', 'page'], [seleniumX]);
            await seleniumX.click(onboardingProductSelectPageHorseLiabilityProductLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SPECIFIC, 2000);

            let onboardingStep1GoBackToProductSelectPageLinkTasks = [
                new ClassSeleniumXTask(
                    seleniumX.findElementAsync,
                    seleniumX,
                    [
                        StaticClassSeleniumX.Selenium.By.css('i.icon-profile'),
                        null,
                    ],
                    null,
                    '>>>>>> Error: No go back to product select page link found on the onboarding step1 page.'
                ),
            ];
            let onboardingStep1GoBackToProductSelectPageLink = await seleniumX.runAsync(
                onboardingStep1GoBackToProductSelectPageLinkTasks
            );
            seleniumX.validateAllResultsSimple(
                onboardingStep1GoBackToProductSelectPageLinkTasks,
                onboardingStep1GoBackToProductSelectPageLink
            );
            seleniumX.eventEmitter.emitEvents(['test-onboarding-product-specific', 'action', 'click', 'go back', 'link', 'onboarding product specific', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep1GoBackToProductSelectPageLink[0]);
            await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT, 2000);
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding product select page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-select', 'action', 'click', 'personal liability product', 'link', 'onboarding product select', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the personal liability product link on the onboarding product select page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-select', 'action', 'click', 'go back', 'link', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the go back to onboarding product select link on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-select', 'action', 'click', 'dog liability product', 'link', 'onboarding product select', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the dog liability product link on the onboarding product select page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-confirm-race', 'action', 'click', 'no sorry', 'link', 'confirm race', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the no sorry button on the onboarding confirm race page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-select', 'action', 'click', 'horse liability product', 'link', 'onboarding product select', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the horse liability product link on the onboarding product select page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-specific', 'action', 'click', 'go back', 'link', 'onboarding product specific', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the go back to onboarding product select link on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding product select page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingProductSelect;