let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');

const path = require('path');

class TestOnboardingProductSelectPersonalLiability {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT,
            'onboarding product select',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select-personal-liability', 'begin'], [seleniumX]);

        let onboardingProductSelectPagePersonalLiabilityProductLinkTasks =             [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pair'),
                    null,
                ],
                null,
                '>>>>>> Error: No personal liability product link found on the onboarding select product page.'
            ),
        ];
        let onboardingProductSelectPagePersonalLiabilityProductLink = await seleniumX.runAsync(
            onboardingProductSelectPagePersonalLiabilityProductLinkTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingProductSelectPagePersonalLiabilityProductLinkTasks,
            onboardingProductSelectPagePersonalLiabilityProductLink
        );
        seleniumX.eventEmitter.emitEvents(['test-onboarding-product-select-personal-liability', 'action', 'click', 'personal liability product', 'link', 'onboarding product select', 'page'], [seleniumX]);
        await seleniumX.click(onboardingProductSelectPagePersonalLiabilityProductLink[0]);
        await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1, 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-select-personal-liability', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select-personal-liability', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding product select page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-product-select-personal-liability', 'action', 'click', 'personal liability product', 'link', 'onboarding product select', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the personal liability product link on the onboarding product select page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-select-personal-liability', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding product select personal liability has been selected successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingProductSelectPersonalLiability;