const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXEventEmitter = require('../../lib/SeleniumXEventEmitter');
const path = require('path');

let seleniumX;

class SuiteDashboard {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        if(process.env.LOGIN_USER_EMAIL) {
            seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'before', 'test-no-authentication-login'], [seleniumX, process.env.LOGIN_USER_EMAIL]);
            let TestNoAuthenticationLogin = require('./test-no-authentication-login');
            seleniumX = await TestNoAuthenticationLogin.boot(seleniumX);
            seleniumX = await TestNoAuthenticationLogin.main(seleniumX);
            seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-no-authentication-login'], [seleniumX]);
            seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
        } else {
            if(process.env.NEW_USER_EMAIL) {
                seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'before', 'test-no-authentication-registration'], [seleniumX, process.env.NEW_USER_EMAIL]);
                let TestNoAuthenticationRegistration = require('./test-no-authentication-registration');
                seleniumX = await TestNoAuthenticationRegistration.boot(seleniumX);
                seleniumX = await TestNoAuthenticationRegistration.main(seleniumX);
                seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-no-authentication-registration'], [seleniumX]);
                seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
            } else {
                seleniumX.exceptionHandler.throwException(
                    ClassSeleniumXException.ClassSeverity.FATAL,
                    ClassSeleniumXException.ClassType.ENGINE,
                    'No login or signup user emails provided. The dashboard suite cannot continue. Aborting.',
                );
            }
        }

        let TestDashboardLandingPage = require('./test-dashboard-landing-page');
        seleniumX = await TestDashboardLandingPage.boot(seleniumX);
        seleniumX = await TestDashboardLandingPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-landing-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestDashboardAccountSettingsPage = require('./test-dashboard-account-settings-page');
        seleniumX = await TestDashboardAccountSettingsPage.boot(seleniumX);
        seleniumX = await TestDashboardAccountSettingsPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-account-settings-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestDashboardAccountSettings = require('./test-dashboard-account-settings');
        seleniumX = await TestDashboardAccountSettings.boot(seleniumX);
        seleniumX = await TestDashboardAccountSettings.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-account-settings'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestSettingsPage = require('./test-dashboard-settings-page');
        seleniumX = await TestSettingsPage.boot(seleniumX);
        seleniumX = await TestSettingsPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-settings-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);


        let TestSettings = require('./test-dashboard-settings');
        seleniumX = await TestSettings.boot(seleniumX);
        seleniumX = await TestSettings.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-settings'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await this.clickToDashboard(seleniumX);
        let testDashboardInfoQuestionsPageButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('ANTWORTEN FINDEN'));
        await seleniumX.click(testDashboardInfoQuestionsPageButton)
        let TestDashboardInfoQuestionsPage = require('./test-dashboard-info-questions-page');
        seleniumX = await TestDashboardInfoQuestionsPage.boot(seleniumX);
        seleniumX = await TestDashboardInfoQuestionsPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-info-questions-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await this.clickToDashboard(seleniumX);
        let testDashboardStatusPageButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('HELDEN STATUS ANZEIGEN'));
        await seleniumX.click(testDashboardStatusPageButton)
        let TestDashboardStatusPage = require('./test-dashboard-status-page');
        seleniumX = await TestDashboardStatusPage.boot(seleniumX);
        seleniumX = await TestDashboardStatusPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-status-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await this.clickToDashboard(seleniumX);
        let testDashboardInvitePageButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('LADE DEINE FREUNDE EIN'));
        await seleniumX.click(testDashboardInvitePageButton);
        let TestDashboardInvitePage = require('./test-dashboard-invite-page');
        seleniumX = await TestDashboardInvitePage.boot(seleniumX);
        seleniumX = await TestDashboardInvitePage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dasboard-invite-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestDashboardInvite = require('./test-dashboard-invite');
        seleniumX = await TestDashboardInvite.boot(seleniumX);
        seleniumX = await TestDashboardInvite.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-invite'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await this.clickToDashboard(seleniumX);
        let testDashboardInfoHowPageButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('WIE FUNKTIONIERT´S?'));
        await seleniumX.click(testDashboardInfoHowPageButton);
        let TestDashboardInfoHowPage = require('./test-dashboard-info-how-page');
        seleniumX = await TestDashboardInfoHowPage.boot(seleniumX);
        seleniumX = await TestDashboardInfoHowPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-info-how-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestDashboardInfoHow = require('./test-dashboard-info-how');
        seleniumX = await TestDashboardInfoHow.boot(seleniumX);
        seleniumX = await TestDashboardInfoHow.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-dashboard', 'main', 'after', 'test-dashboard-info-how'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);


        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'main', 'filename'],
            (seleniumX, filename) => {
                seleniumX.logger.log('');
                seleniumX.logger.log(filename, ClassSeleniumXLogger.ClassLogType.ENVIRONMENT);
            },
            ClassSeleniumXEventEmitter.ClassCondition.SUBSET,
        );

        return seleniumX;
    }

    static async clickToDashboard(seleniumX) {
        let homeLogoLink = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@alt,'HAFTPFLICHT HELDEN')]/.."));
        await seleniumX.click(homeLogoLink);
    }
}

(async () => {
    seleniumX =
        seleniumX ||
        await new ClassSeleniumX();
    seleniumX = SuiteDashboard.boot(seleniumX)
        .then(
            (seleniumX) => {
                return SuiteDashboard.main(seleniumX)
                    .then(
                        (seleniumX) => {
                            !seleniumX || seleniumX.shutdownWithSuccess();
                        }
                    )
                    .catch(
                        async (error) => {
                            console.log(error);
                            !seleniumX || seleniumX.shutdownWithError(error);
                        }
                    );
            }
        );
})();