const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardInfoQuestionsPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INFO_QUESTIONS));

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-questions', 'begin'], [seleniumX]);

        let infoQuestionsLinkElementsTask = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.partialLinkText('Wir freuen uns auf deinen Anruf'),
                    'Check: Looking for the "Kontakt" link on the Dashboard Info Questions page...',
                ],
                'Found the "Kontakt" link on Info Questions page.',
                '>>>>>> Error: The "Kontakt" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.partialLinkText('Unser Versicherungspartner'),
                    'Check: Looking for the "Partner" link on the Dashboard Info Questions page...',
                ],
                'Found the "Partner" link on the Dashboard Info Questions page.',
                '>>>>>> Error: The "Partner" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.partialLinkText('Entdecke den Helden Schutz'),
                    'Check: Looking for the "Leistungsübersicht & Dokumente" link on the Dashboard Info Questions page...',
                ],
                'Found the "Leistungsübersicht & Dokumente" link on the Info Questions page.',
                '>>>>>> Error: The "Leistungsübersicht & Dokumente" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.partialLinkText('Du willst es selbst nachlesen?'),
                    'Check: Looking for the "Antworten ﬁnden" link on the Dashboard Info Questions page...',
                ],
                'Found the "Antworten ﬁnden" link on the Dashboard Info Questions page.',
                '>>>>>> Error: The "Antworten ﬁnden" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('AGB'),
                    'Check: Looking for the "Rechtliches AGB" link on the Dashboard Info Questions page...',
                ],
                'Found the "Rechtliches AGB" link on Info Questions page.',
                '>>>>>> Error: The "Rechtliches AGB" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Impressum'),
                    'Check: Looking for the "Rechtliches Impressum" link on the Dashboard Info Questions page...',
                ],
                'Found the "Rechtliches Impressum" link on the Dashboard Info Questions page.',
                '>>>>>> Error: The "Rechtliches Impressum" link is missing on the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Datenschutz'),
                    'Check: Looking for the "Rechtliches Datenschutz" link on the Dashboard Info Questions page...',
                ],
                'Found the "Rechtliches Datenschutz" link on the Dashboard Info Questions page.',
                '>>>>>> Error: The "Rechtliches Datenschutz" link is missing on the Dashboard Info Questions page.',
            ),
        ];
        let infoQuestionsLinkElements = await seleniumX.runAsync(infoQuestionsLinkElementsTask);
        seleniumX.validateAllResultsSimple(infoQuestionsLinkElementsTask, infoQuestionsLinkElements);

        let infoQuestionsIconElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-phone'),
                    'Check: Looking for the Phone icon on the Dashboard Info Questions page...',
                ],
                'Found the Phone icon on the Dashboard Info Questions page.',
                '>>>>>> Error: The Phone icon is missing from the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-handsup'),
                    'Check: Looking for the Handsup icon on the Dashboard Info Questions page...',
                ],
                'Found the Handsup icon on the Dashboard Info Questions page.',
                '>>>>>> Error: The Handsup icon is missing from the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-shield'),
                    'Check: Looking for Shield icon on Info Questions page...',
                ],
                'Found the Shield icon on the Dashboard Info Questions page.',
                '>>>>>> Error: The Shield icon is missing from the Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-bulb'),
                    'Check: Looking for the Bulb icon on the Dashboard Info Questions page...',
                ],
                'Found the Bulb icon on the Dashboard Info Questions page.',
                '>>>>>> Error: The Bulb Phone icon is missing from the Dashboard Info Questions page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-dollar'),
                    'Check: Looking for the Dollar icon on the Dashboard Info Questions page...',
                ],
                'Found the Dollar icon on the Dashboard Info Questions page.',
                '>>>>>> Error: The Dollar icon is missing from the Dashboard Info Questions page.',
            ),
        ];
        let infoQuestionsIconElements = await seleniumX.runAsync(infoQuestionsIconElementsTasks);
        seleniumX.validateAllResultsSimple(infoQuestionsIconElementsTasks, infoQuestionsIconElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-questions', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-questions-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Info Questions page (' + seleniumX.state.currentUrl + ') contains all Input label elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-questions', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Info Questions page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardInfoQuestionsPage;

