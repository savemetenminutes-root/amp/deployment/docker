const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardAccountSettingsPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_ACCOUNT_SETTINGS,
            'dashboard account settings',
            true,
        );

        await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.css('[type="text"]'), 15000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-account-settings-page', 'begin'], [seleniumX]);

        let accountSettingsPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="text"]'),
                    'Check: Looking for the Username Field on the Dashboard Account Settings page...',
                ],
                'Found the Username Field on the Dashboard Account Settings page.',
                '>>>>>> Error: The Username Field is missing on the Dashboard Account Settings page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="password"]'),
                    'Check: Looking for the Password Field on the Dashboard Account Settings page...',
                ],
                'Found the Password Field on the Dashboard Account Settings page.',
                '>>>>>> Error: The Password Field is missing on the Dashboard Account Settings page.',
            ),
        ];
        let accountSettingsPageElements = await seleniumX.runAsync(accountSettingsPageElementsTasks);
        seleniumX.validateAllResultsSimple(accountSettingsPageElementsTasks, accountSettingsPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-account-settings', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Account Settings page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Account Settings page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardAccountSettingsPage;

