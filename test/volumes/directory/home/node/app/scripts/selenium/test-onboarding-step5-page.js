const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep5Page {
    static async main(seleniumX) {
        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP5,
            'onboarding step5',
            true,
        );

        await seleniumX.doBasicTests('onboarding step5');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5-page', 'begin'], [seleniumX]);

        let onboardingStep5PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Deine Beauftragung.')]"),
                    'Check: Looking for the heading on the onboarding step5 page...',
                ],
                'Found the heading on the onboarding step5 page.',
                '>>>>>> Error: No heading found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Bedingungswerk'),
                    'Check: Looking for the liability contract conditions link on the onboarding step5 page...',
                ],
                'Found the liability contract conditions link on the onboarding step5 page.',
                '>>>>>> Error: No liability contract conditions link found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Produktinformationsblatt'),
                    'Check: Looking for the product information link on the onboarding step5 page...',
                ],
                'Found the product information link on the onboarding step5 page.',
                '>>>>>> Error: No product information link found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('AGB'),
                    'Check: Looking for the terms of service link on the onboarding step5 page...',
                ],
                'Found the terms of service link on the onboarding step5 page.',
                '>>>>>> Error: No terms of service link found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Datenschutzerklärung'),
                    'Check: Looking for the data protection link on the onboarding step5 page...',
                ],
                'Found the data protection link on the onboarding step5 page.',
                '>>>>>> Error: No data protection link found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Beauftragung'),
                    'Check: Looking for the commissioning link on the onboarding step5 page...',
                ],
                'Found the commissioning link on the onboarding step5 page.',
                '>>>>>> Error: No commissioning link found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.name('selection2'),
                    'Check: Looking for the hidden checkboxes on the onboarding step5 page...',
                ],
                'Found the hidden checkboxes on the onboarding step5 page.',
                '>>>>>> Error: No hidden checkboxes found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    'Check: Looking for the styled checkboxes on the onboarding step5 page...',
                ],
                'Found the styled checkboxes on the onboarding step5 page.',
                '>>>>>> Error: No styled checkboxes found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//button[contains(string(), 'ZUR UNTERSCHRIFT')]"),
                    'Check: Looking for the submit button on the onboarding step5 page...',
                ],
                'Found the submit button on the onboarding step5 page.',
                '>>>>>> Error: No submit button found on the onboarding step5 page.',
            ),
        ];
        let onboardingStep5PageElements = await seleniumX.runAsync(onboardingStep5PageElementsTasks);
        seleniumX.validateAllResultsSimple(
            onboardingStep5PageElementsTasks,
            onboardingStep5PageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding step5 page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding step5 page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep5Page;