let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep3 {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP3,
            'onboarding step3',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'begin'], [seleniumX]);

        let onboardingStep3PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Dein jährlicher Beitrag.')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No heading found on the onboarding step3 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//a[contains(string(), 'Helden Code eingeben')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No use voucher button found on the onboarding step3 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('WEITER'),
                    null,
                ],
                null,
                '>>>>>> Error: No submit buttons found on the onboarding step3 page.',
            ),
        ];
        let onboardingStep3PageElements = await seleniumX.runAsync(
            onboardingStep3PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep3PageElementsTasks,
            onboardingStep3PageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'action', 'click', 'use voucher', 'button', 'onboarding step3', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep3PageElements[1]);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'check', 'wait', 'for', 'use voucher', 'label', 'and', 'text', 'input', 'onboarding step3', 'page'], [seleniumX]);
        let useVoucherLabelParent = await seleniumX.driver.wait(new StaticClassSeleniumX.WebDriver.Condition('to see if the voucher code text input will appear', async() => {
            let existsElement = await seleniumX.existsElementAsync(
                StaticClassSeleniumX.Selenium.By.xpath(
                    "//label[contains(string(), 'Helden Code eingeben')]/..",
                ),
            );
            if(existsElement) {
                return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Helden Code eingeben')]/.."));
            }

            return false;
        }), 2000);
        let formElementUseVoucher = await seleniumX.findChildOfElementAsync(useVoucherLabelParent, StaticClassSeleniumX.Selenium.By.css('input[type="text"]'), 'Check: Looking for the use voucher text input on the onboarding step3 page...');
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'passed', 'wait', 'for', 'use voucher', 'label', 'and', 'text', 'input', 'onboarding step3', 'page'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'action', 'click', 'submit', 'button', 'onboarding step3', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep3PageElements[2][0]);

        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP4), 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step3 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'action', 'click', 'use voucher', 'button', 'onboarding step3', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the use voucher button on the onboarding step3 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'check', 'wait', 'for', 'use voucher', 'label', 'and', 'text', 'input', 'onboarding step3', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Waiting for the use voucher label and text input to appear after clicking on the use voucher button on the onboarding step3 page.', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'passed', 'wait', 'for', 'use voucher', 'label', 'and', 'text', 'input', 'onboarding step3', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The use voucher label and text input appear correctly after clicking on the use voucher button on the onboarding step3 page.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'action', 'click', 'submit', 'button', 'onboarding step3', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the submit button on the onboarding step3 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step3 page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep3;