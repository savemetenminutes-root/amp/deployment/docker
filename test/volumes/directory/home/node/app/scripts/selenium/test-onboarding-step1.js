let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep1 {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1,
            'onboarding step1',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1', 'begin'], [seleniumX]);

        let onboardingStep1FormElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Ab Sofort')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No start contract immediately radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Datum')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No specify contract starting date radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Vertragswechsel')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No terminate existing contract radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.name('selection2'),
                    null,
                ],
                null,
                '>>>>>> Error: No user record verification hidden checkbox found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    null
                ],
                null,
                '>>>>>> Error: No user record verification styled checkbox span found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//button[contains(string(), 'WEITER')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No submit button found on the onboarding step1 page.',
            ),
        ];
        let onboardingStep1FormElements = await seleniumX.runAsync(onboardingStep1FormElementsTasks);
        seleniumX.validateAllResultsSimple(
            onboardingStep1FormElementsTasks,
            onboardingStep1FormElements
        );

        if(!await seleniumX.getStyledCheckboxCheckedState(onboardingStep1FormElements[4])) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1', 'action', 'click', 'check', 'styled', 'checkbox', 'user', 'record', 'verification', 'onboarding step1', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep1FormElements[4]);
        }
        seleniumX.eventEmitter.emitEvents(['test-onboarding-step1', 'action', 'click', 'submit', 'button', 'onboarding step1', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep1FormElements[5]);
        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP2), 2000);

        let onboardingStep2GoBackLinksTasks = [
            new ClassSeleniumXTask(
                seleniumX.waitUntilElementLocated,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//a[contains(@href,'" + seleniumX.getRouteRelative(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1) + "')]"),
                    4000,
                ],
                null,
                '>>>>>> Error: No go back links found on the onboarding step2 page.',
            ),
        ];
        let onboardingStep2GoBackLinks = await seleniumX.runAsync(onboardingStep2GoBackLinksTasks);
        seleniumX.validateAllResultsSimple(onboardingStep2GoBackLinksTasks, onboardingStep2GoBackLinks);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1', 'action', 'click', 'go', 'back', 'link', 'onboarding step2', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep2GoBackLinks[0]);

        await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1, 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step1 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1', 'action', 'click', 'check', 'styled', 'checkbox', 'user', 'record', 'verification', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the user record verification checkbox in order to check it on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-step1', 'action', 'click', 'submit', 'button', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the submit button on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1', 'action', 'click', 'go', 'back', 'link', 'onboarding step2', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the go back link on the onboarding step2 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step1 page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep1;