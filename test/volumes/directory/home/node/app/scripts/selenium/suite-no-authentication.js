const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXEventEmitter = require('../../lib/SeleniumXEventEmitter');
const ClassSeleniumXException = require('../../lib/SeleniumXException');
const path = require('path');

let seleniumX;

class SuiteOnboarding {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestNoAuthenticationHomeSourceBasic = require('./test-no-authentication-home-source-basic');
        seleniumX = await TestNoAuthenticationHomeSourceBasic.boot(seleniumX);
        seleniumX = await TestNoAuthenticationHomeSourceBasic.main(seleniumX);

        await seleniumX.navigate(StaticClassSeleniumX.ClassSeleniumXRoutes.HOME);

        if (seleniumX.state.currentUrl !== StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The home page does not redirect to the login page.',
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'redirect', 'home', 'login', 'passed'], [seleniumX]);

        const TestNoAuthenticationLoginPage = require('./test-no-authentication-login-page');
        seleniumX = await TestNoAuthenticationLoginPage.boot(seleniumX);
        seleniumX = await TestNoAuthenticationLoginPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'main', 'after', 'test-no-authentication-login-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'action', 'click', 'registration', 'link', 'login', 'page'], [seleniumX]);
        await seleniumX.click(await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Jetzt registrieren.')));
        await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.SIGNUP, 2000);

        const TestNoAuthenticationRegistrationPage = require('./test-no-authentication-registration-page');
        seleniumX = await TestNoAuthenticationRegistrationPage.boot(seleniumX);
        seleniumX = await TestNoAuthenticationRegistrationPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'main', 'after', 'test-no-authentication-registration-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'action', 'click', 'go_back', 'link', 'registration', 'page'], [seleniumX]);
        await seleniumX.click(await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Jetzt anmelden.')));
        await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN, 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'after', 'click', 'go_back', 'login', 'page', 'contains', 'html', 'elements'], [seleniumX]);
        let loginPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Passwort vergessen?'),
                    'Check: Looking for the forgot password page link after going back to the login page...',
                ],
                'Found the forgot password page link after going back to the login page.',
                '>>>>>> Error: The forgot password page link is missing after going back to the login page.',
            ),
        ];
        let loginPageElements = await seleniumX.runAsync(loginPageElementsTasks);
        seleniumX.validateAllResultsSimple(
            loginPageElementsTasks,
            loginPageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'action', 'click', 'forgot_password', 'link', 'login', 'page'], [seleniumX]);
        await seleniumX.click(await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Passwort vergessen?')));
        await seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.FORGOT, 2000);

        let TestNoAuthenticationForgotPasswordPage = require('./test-no-authentication-forgot-password-page');
        seleniumX = await TestNoAuthenticationForgotPasswordPage.boot(seleniumX);
        seleniumX = await TestNoAuthenticationForgotPasswordPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-no-authentication', 'main', 'after', 'test-no-authentication-forgot-password-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'main', 'filename'],
            (seleniumX, filename) => {
                seleniumX.logger.log('');
                seleniumX.logger.log(filename, ClassSeleniumXLogger.ClassLogType.ENVIRONMENT);
            },
            ClassSeleniumXEventEmitter.ClassCondition.SUBSET,
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-no-authentication', 'redirect', 'home', 'login', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('The home page URL redirects to the login page properly (' + StaticClassSeleniumX.ClassSeleniumXRoutes.HOME + ' -> ' + seleniumX.state.currentUrl + ').', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-no-authentication', 'action', 'click', 'registration', 'page', 'link', 'login', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the registration page link on the login page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-no-authentication', 'action', 'click', 'go_back', 'link', 'registration', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the go back link on the registration page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-no-authentication', 'after', 'click', 'go_back', 'login', 'page', 'contains', 'html', 'elements'],
            (seleniumX) => {
                seleniumX.logger.log('Checking whether after going back to the login page (' + seleniumX.state.currentUrl + ') certain HTML elements exist...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-no-authentication', 'action', 'click', 'forgot_password', 'link', 'login', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the forgot password link on the login page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        return seleniumX;
    }
}

(async () => {
    seleniumX =
        seleniumX ||
        await new ClassSeleniumX();
    seleniumX = SuiteOnboarding.boot(seleniumX)
        .then(
            (seleniumX) => {
                return SuiteOnboarding.main(seleniumX)
                    .then(
                        (seleniumX) => {
                            !seleniumX || seleniumX.shutdownWithSuccess();
                        }
                    )
                    .catch(
                        async (error) => {
                            console.log(error);
                            !seleniumX || seleniumX.shutdownWithError(error);
                        }
                    );
            }
        );
})();