const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const path = require('path');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardSettingsPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main'], [seleniumX, __filename]);

        await seleniumX.navigate(StaticClassSeleniumX.ClassSeleniumXRoutes.CONTRACT_SELECT);

        let privateIcon = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath('//form/div/div/div[1]/ul/li[1]/a/i'));

        await seleniumX.click(privateIcon);

        let settingsButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('ANZEIGEN'));
        await seleniumX.click(settingsButton);

        await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.css('[type="text"]'), 15000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-address', 'begin'], [seleniumX]);

        let settingsAddressTabLabelParentElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Vorname')]/.."),
                    'Check: Looking for "Vorname" label on Settings Address page...',
                ],
                'Found "Vorname" Label on Settings Address page.',
                '>>>>>> Error: "Vorname" Label is missing on Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Nachname')]/.."),
                    'Check: Looking for "Nachname" label on Settings Address page...',
                ],
                'Found "Nachname" Label on Settings Address page.',
                '>>>>>> Error: "Nachname" Label is missing on Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Adresse')]/.."),
                    'Check: Looking for "Adresse" label on Settings Address page...',
                ],
                'Found "Adresse" Label on Settings Address page.',
                '>>>>>> Error: "Adresse" Label is missing on Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'PLZ')]/.."),
                    'Check: Looking for "PLZ" label on Settings Address page...',
                ],
                'Found "PLZ" Label on Settings Address page.',
                '>>>>>> Error: "PLZ" Label is missing on Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Ort')]/.."),
                    'Check: Looking for "Ort" label on Settings Address page...',
                ],
                'Found "Ort" Label on Settings Address page.',
                '>>>>>> Error: "Ort" Label is missing on Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Mobilnummer')]/.."),
                    'Check: Looking for "Mobilnummer" label on Settings Address page...',
                ],
                'Found "Mobilnummer" Label on Settings Address page.',
                '>>>>>> Error: "Mobilnummer" Label is missing on Settings Address page.'
            )
        ];


        let settingsAddressTabLabelParentElements = await seleniumX.runAsync(settingsAddressTabLabelParentElementsTasks);
        seleniumX.validateAllResultsSimple(settingsAddressTabLabelParentElementsTasks, settingsAddressTabLabelParentElements);

        let settingsAddressTabFormElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[0],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "Vorname" text input on Settings Address page....',
                ],
                'Found the "Vorname" text input on the Settings Address page.',
                '>>>>>> Error: The "Vorname" text input is missing on the Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[1],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "Nachname" text input on Settings Address page....',
                ],
                'Found the "Nachname" text input on the Settings Address page.',
                '>>>>>> Error: The "Nachname" text input is missing on the Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[2],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "Adresse" text input on Settings Address page....',
                ],
                'Found the "Adresse" text input on the Settings Address page.',
                '>>>>>> Error: The "Adresse" text input is missing on the Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[3],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "PLZ" text input on Settings Address page....',
                ],
                'Found the "PLZ" text input on the Settings Address page.',
                '>>>>>> Error: The "PLZ" text input is missing on the Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[4],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "Ort" text input on Settings Address page....',
                ],
                'Found the "Ort" text input on the Settings Address page.',
                '>>>>>> Error: The "Ort" text input is missing on the Settings Address page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                settingsAddressTabLabelParentElements[5],
                [
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the "Mobilnummer" text input on Settings Address page....',
                ],
                'Found the "Mobilnummer" text input on the Settings Address page.',
                '>>>>>> Error: The "Mobilnummer" text input is missing on the Settings Address page.'
            ),
        ];

        let settingsAddressTabFormElements = await seleniumX.runAsync(settingsAddressTabFormElementsTasks);
        seleniumX.validateAllResultsSimple(settingsAddressTabFormElementsTasks, settingsAddressTabFormElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-address', 'success'], [seleniumX]);

        //Peyment Tab
        let paymentTab = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Zahlungsart'));
        await seleniumX.click(paymentTab);
        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-payment', 'begin'], [seleniumX]);

        let settingsPaymentTabElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Paypal Konto')]"),
                    'Check: Looking for "Paypal Konto" label in Settings Payment page...',
                ],
                'Passed: Found "Paypal Konto" Label in Settings Payment page.',
                '>>>>>> Error: "Paypal Konto" Label is missing in Settings Payment page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Zahlmethode ändern'),
                    'Check: Looking for "Zahlmethode ändern" Link in Settings Payment page...',
                ],
                'Found "Zahlmethode ändern" Link in Settings Payment page.',
                '>>>>>> Error: "Zahlmethode ändern" Link is missing in Settings Payment page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="text"]'),
                    'Check: Looking for email Input in Settings Payment page...',
                ],
                'Found email Input in Settings Payment page.',
                '>>>>>> Error: email Input is missing in Settings Payment page.'
            ),
        ];


        let settingsPaymentTabElements = await seleniumX.runAsync(settingsPaymentTabElementsTasks);
        seleniumX.validateAllResultsSimple(settingsPaymentTabElementsTasks, settingsPaymentTabElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-payment', 'success'], [seleniumX]);

        //Contract Tab
        let contractTab = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Vertrag'));
        await seleniumX.click(contractTab);
        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-contract-select', 'begin'], [seleniumX]);

        let settingsContractTabElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Vertragsnummer')]"),
                    'Check: Looking for "Vertragsnummer" label in Settings Contract page...',
                ],
                'Passed: Found "Vertragsnummer" Label in Settings Contract page.',
                '>>>>>> Error: "Vertragsnummer" Label is missing in Settings Contract page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Vertragsbeginn')]"),
                    'Check: Looking for "Vertragsbeginn" label in Settings Contract page...',
                ],
                'Passed: Found "Vertragsbeginn" Label in Settings Contract page.',
                '>>>>>> Error: "Vertragsbeginn" Label is missing in Settings Contract page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Nächste Fälligkeit')]"),
                    'Check: Looking for "Nächste Fälligkeit" label in Settings Contract page...',
                ],
                'Passed: Found "Nächste Fälligkeit" Label in Settings Contract page.',
                '>>>>>> Error: "Nächste Fälligkeit" Label is missing in Settings Contract page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Bedingungswerk anschauen')]"),
                    'Check: Looking for "Bedingungswerk anschauen" label in Settings Contract page...',
                ],
                'Passed: Found "Bedingungswerk anschauen" Label in Settings Contract page.',
                '>>>>>> Error: "Bedingungswerk anschauen" Label is missing in Settings Contract page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'VERTRAG KÜNDIGEN')]"),
                    'Check: Looking for "VERTRAG KÜNDIGEN" label in Settings Contract page...',
                ],
                'Passed: Found "VERTRAG KÜNDIGEN" Label in Settings Contract page.',
                '>>>>>> Error: "VERTRAG KÜNDIGEN" Label is missing in Settings Contract page.'
            ),
        ];

        let settingsContractTabElements = await seleniumX.runAsync(settingsContractTabElementsTasks);
        seleniumX.validateAllResultsSimple(settingsContractTabElementsTasks, settingsContractTabElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-contract-select', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-settings-address', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Settings page (Address tab) (' + seleniumX.state.currentUrl + ') contains all Inputs label elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-settings-payment', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Settings page (Payment tab) (' + seleniumX.state.currentUrl + ') contains all html elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );
        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-contract-select', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Settings page (Contract tab) (' + seleniumX.state.currentUrl + ') contains  all html elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-contract-select', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Settings page( Address tab).', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-account-payment', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Settings page( payment tab).', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );
        seleniumX.eventEmitter.onEvents(
            ['test', 'test-account-contract', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Settings page( Contract tab).', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardSettingsPage;

