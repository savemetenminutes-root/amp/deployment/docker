const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestOnboardingStep3Page {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP3,
            'onboarding step3',
            true,
        );

        await seleniumX.doBasicTests('onboarding step3');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3-page', 'begin'], [seleniumX]);

        let onboardingStep3PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Dein jährlicher Beitrag.')]"),
                    'Check: Looking for the heading on the onboarding step3 page...',
                ],
                'Found the heading on the onboarding step3 page.',
                '>>>>>> Error: The heading is missing from the onboarding step3 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//a[contains(string(), 'Helden Code eingeben')]"),
                    'Check: Looking for the use voucher button on the onboarding step3 page...',
                ],
                'Found the use voucher button on the onboarding step3 page.',
                '>>>>>> Error: The use voucher button is missing from the onboarding step3 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('WEITER'),
                    'Check: Looking for submit buttons on the onboarding step3 page...',
                ],
                'Found submit buttons on the onboarding step3 page.',
                '>>>>>> Error: No submit buttons found on the onboarding step3 page.',
            ),
        ];
        let onboardingStep3PageElements = await seleniumX.runAsync(
            onboardingStep3PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep3PageElementsTasks,
            onboardingStep3PageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step3-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding step3 page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step3-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding step3 page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestOnboardingStep3Page;