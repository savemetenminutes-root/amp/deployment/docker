const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestNoAuthenticationRegistrationPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.SIGNUP,
            'registration',
        );

        await seleniumX.doBasicTests('registration');

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration-page', 'begin'], [seleniumX]);

        let registrationPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Jetzt anmelden.'),
                    'Check: Looking for the go back link on the registration page...',
                ],
                'Found the go back link on the registration page.',
                '>>>>>> Error: The go back link is missing on the registration page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="email"]'),
                    'Check: Looking for email input form fields on the registration page...',
                ],
                'Found email input form fields on the registration page.',
                '>>>>>> Error: No email input form fields found on the registration page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="password"]'),
                    'Check: Looking for password input form fields on the registration page...',
                ],
                'Found password input form fields on the registration page.',
                '>>>>>> Error: No password input form fields found on the registration page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="submit"]'),
                    'Check: Looking for submit buttons on the registration page...',
                ],
                'Found submit buttons on the registration page.',
                '>>>>>> Error: No submit buttons found on the registration page.',
            ),
        ];
        let registrationPageElements = await seleniumX.runAsync(registrationPageElementsTasks);
        seleniumX.validateAllResultsSimple(
            registrationPageElementsTasks,
            registrationPageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the registration page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the registration page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestNoAuthenticationRegistrationPage;