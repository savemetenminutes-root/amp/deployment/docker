const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestOnboardingStep4Page {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP4,
            'onboarding step4',
            true,
        );

        await seleniumX.doBasicTests('onboarding step4');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-page', 'begin'], [seleniumX]);

        let linkChoosePaymentMethod = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Zahlmethode ändern'), 'Check: Looking for the choose payment method link on the onboarding step4 page...');
        if(linkChoosePaymentMethod) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-page', 'action', 'active', 'payment', 'method', 'click', 'select payment method'], [seleniumX]);
            await seleniumX.click(linkChoosePaymentMethod);
        }

        let containerPaymentMethods = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('ul.payment-list '), 'Check: Looking for the payment methods container on the onboarding step4 page...');
        if(!containerPaymentMethods) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The payment methods container is missing from the onboarding step4 page.',
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-page', 'passed', 'payment', 'methods', 'container', 'found', 'onboarding step4', 'page'], [seleniumX]);

        let onboardingStep4PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the SEPA logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-sepa.svg')]"),
                            );
                            if(existsElement) {
                                return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-sepa.svg')]"));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                'Found the SEPA logo image on the onboarding step4 page.',
                '>>>>>> Error: The SEPA logo image is missing from the onboarding step4 page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the PayPal logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-paypal.png')]"),
                            );
                            if(existsElement) {
                                return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-paypal.png')]"));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                'Found the PayPal logo image on the onboarding step4 page.',
                '>>>>>> Error: The PayPal logo image is missing from the onboarding step4 page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the VISA/Mastercard logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-visa-mastercard.png')]"),
                            );
                            if(existsElement) {
                                return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-visa-mastercard.png')]"));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                'Found the VISA/Mastercard logo image on the onboarding step4 page.',
                '>>>>>> Error: The VISA/Mastercard logo image is missing from the onboarding step4 page.'
            ),
        ];
        let onboardingStep4PageElements = await seleniumX.runAsync(
            onboardingStep4PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep4PageElementsTasks,
            onboardingStep4PageElements
        );

        let radioButtonsPaymentMethods = await seleniumX.findChildrenOfElementAsync(containerPaymentMethods, StaticClassSeleniumX.Selenium.By.css('input[type="radio"]'), 'Check: Looking for the payment methods radio buttons on the onboarding step4 page...')
        if(typeof radioButtonsPaymentMethods !== 'object' || radioButtonsPaymentMethods.length < 3) {
            let radioButtonsCount = 0;
            if(typeof radioButtonsPaymentMethods === 'object') {
                radioButtonsCount = radioButtonsPaymentMethods.length;
            }
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The payment methods container holds <3 radio buttons on the onboarding step4 page. Counted ' + radioButtonsCount + '.',
            );
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding step4 page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-page', 'action', 'active', 'payment', 'method', 'click', 'select payment method'],
            (seleniumX) => {
                seleniumX.logger.log('There is an active selected payment method remembered in the application local storage state.', ClassSeleniumXLogger.ClassLogType.INFO);
                seleniumX.logger.log('Clicked on the choose payment method link on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-page', 'passed', 'payment', 'methods', 'container', 'found', 'onboarding step4', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Found the payment methods container on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestOnboardingStep4Page;