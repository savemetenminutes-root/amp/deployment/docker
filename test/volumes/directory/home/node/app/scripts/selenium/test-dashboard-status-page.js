const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardStatusPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-status-page', 'begin'], [seleniumX]);

        let statusPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="submit"]'),
                    'Check: Looking for the "FREUNDE EINLADEN" button on the status page...',
                ],
                'Found the "FREUNDE EINLADEN" button on the status page.',
                '>>>>>> Error: The "FREUNDE EINLADEN" button is missing on the status page.',
            ),
        ];
        let statusPageElements = await seleniumX.runAsync(statusPageElementsTasks);
        seleniumX.validateAllResultsSimple(statusPageElementsTasks, statusPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-status-page', 'content', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-status-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Status page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-status-page', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Status page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardStatusPage;

