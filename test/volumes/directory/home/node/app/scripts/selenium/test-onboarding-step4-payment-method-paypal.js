const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestOnboardingStep4PaymentMethodSepa {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP4,
            'onboarding step4',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'begin'], [seleniumX]);

        let linkChoosePaymentMethod = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Zahlmethode ändern'));
        if(linkChoosePaymentMethod) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'action', 'active', 'payment', 'method', 'click', 'select payment method'], [seleniumX]);
            await seleniumX.click(linkChoosePaymentMethod);
        }

        let onboardingStep4PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the SEPA logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-sepa.svg')]"),
                            );
                            if(existsElement) {
                                let sepaPaymentMethodLogoParent = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-sepa.svg')]/../.."));
                                return await seleniumX.findChildOfElementAsync(sepaPaymentMethodLogoParent, StaticClassSeleniumX.Selenium.By.css('input[type="radio"]+label.radio-label>span.radio'));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                null,
                '>>>>>> Error: The SEPA logo image is missing from the onboarding step4 page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the PayPal logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-paypal.png')]"),
                            );
                            if(existsElement) {
                                let paypalPaymentMethodLogoParent = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-paypal.png')]/../.."));
                                return await seleniumX.findChildOfElementAsync(paypalPaymentMethodLogoParent, StaticClassSeleniumX.Selenium.By.css('input[type="radio"]+label.radio-label>span.radio'));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                null,
                '>>>>>> Error: The PayPal logo image is missing from the onboarding step4 page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.driver.wait,
                seleniumX.driver,
                [
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the VISA/Mastercard logo image will appear',
                        async() => {
                            let existsElement = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-visa-mastercard.png')]"),
                            );
                            if(existsElement) {
                                let ccPaymentMethodLogoParent = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//img[contains(@src,'/images/logo-visa-mastercard.png')]/../.."));
                                return await seleniumX.findChildOfElementAsync(ccPaymentMethodLogoParent, StaticClassSeleniumX.Selenium.By.css('input[type="radio"]+label.radio-label>span.radio'));
                            }

                            return false;
                        }
                    ),
                    20000
                ],
                null,
                '>>>>>> Error: The VISA/Mastercard logo image is missing from the onboarding step4 page.'
            ),
        ];
        let onboardingStep4PageElements = await seleniumX.runAsync(
            onboardingStep4PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep4PageElementsTasks,
            onboardingStep4PageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'action', 'click', 'radio', 'button', 'payment', 'method', 'sepa', 'onboarding step4', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep4PageElements[0]);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'check', 'wait', 'for', 'payment', 'method', 'sepa', 'form', 'elements', 'onboarding step4', 'page'], [seleniumX]);
        let onboardingStep4PaymentMethodSepaFormElementsTasks = [
            new ClassSeleniumXTask(async() => true, seleniumX, [], null, null,),
            new ClassSeleniumXTask(async() => true, seleniumX, [], null, null,),
            new ClassSeleniumXTask(async() => true, seleniumX, [], null, null,),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'IBAN')]/../input"),
                    null,
                ],
                null,
                '>>>>>> Error: The IBAN text input is missing from the onboarding step4 page after clicking the SEPA payment method radio button.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Kontoinhaber')]/../input"),
                    null,
                ],
                null,
                '>>>>>> Error: The account holder text input is missing from the onboarding step4 page after clicking the SEPA payment method radio button.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    null,
                ],
                null,
                '>>>>>> Error: The SEPA agreement styled checkbox is missing from the onboarding step4 page after clicking the SEPA payment method radio button.',
            ),
            /*
            // Not working with Selenium
            new ClassSeleniumXTask(
                seleniumX.findElementAsync, seleniumX, [StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox::after'),
                'Check: Looking for the SEPA agreement styled checkbox check mark on the onboarding step4 page after clicking the SEPA payment method radio button...'],
                'Passed: Found the SEPA agreement styled checkbox check mark on the onboarding step4 page after clicking the SEPA payment method radio button.',
                '>>>>>> Error: The SEPA agreement styled checkbox check mark is missing from the onboarding step4 page after clicking the SEPA payment method radio button.'
            ),
            */
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('button[type="submit"]'),
                    null,
                ],
                null,
                '>>>>>> Error: The submit button is missing from the onboarding step4 page after clicking the SEPA payment method radio button.',
            ),
        ];
        let onboardingStep4PaymentMethodSepaFormElements = await seleniumX.runAsync(
            onboardingStep4PaymentMethodSepaFormElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep4PaymentMethodSepaFormElementsTasks,
            onboardingStep4PaymentMethodSepaFormElements
        );
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'passed', 'wait', 'for', 'payment', 'method', 'sepa', 'form', 'elements', 'onboarding step4', 'page'], [seleniumX]);

        if(!await seleniumX.getStyledCheckboxCheckedState(onboardingStep4PaymentMethodSepaFormElements[5])) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'action', 'click', 'check', 'styled', 'checkbox', 'sepa', 'agreement', 'onboarding step4', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep4PaymentMethodSepaFormElements[5]);
        }
        let validFormTest = await seleniumX.formTest(
            [
                [onboardingStep4PaymentMethodSepaFormElements[3], StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE, 'DE89370400440532013000'],
            ],
            onboardingStep4PaymentMethodSepaFormElements[6],
            () => {
                seleniumX.eventEmitter.emitEvents(['test-onboarding-step4-payment-method-sepa', 'action', 'click', 'payment', 'method', 'sepa', 'iban', 'submit', 'button', 'valid', 'data', 'onboarding step4', 'page'], [seleniumX]);
                return seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP5, 10000)
                    .then(
                        (result) => result
                    );
            }
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step4-payment-method-sepa', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step4 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'action', 'active', 'payment', 'method', 'click', 'select payment method'],
            (seleniumX) => {
                seleniumX.logger.log('There is an active selected payment method remembered in the application local storage state.', ClassSeleniumXLogger.ClassLogType.INFO);
                seleniumX.logger.log('Clicked on the choose payment method link on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'check', 'wait', 'for', 'payment', 'method', 'sepa', 'form', 'elements', 'onboarding step4', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Waiting for the SEPA payment method form elements to appear after clicking on the SEPA payment method radio button on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'passed', 'wait', 'for', 'payment', 'method', 'sepa', 'form', 'elements', 'onboarding step4', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The SEPA payment method form elements appear correctly after clicking on the SEPA payment method radio button on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'action', 'click', 'check', 'styled', 'checkbox', 'sepa', 'agreement', 'onboarding step4', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked on the SEPA agreement styled checkbox on the onboarding step4 page to check it in order to test the form validation.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-step4-payment-method-sepa', 'action', 'click', 'payment', 'method', 'sepa', 'iban', 'submit', 'button', 'valid', 'data', 'onboarding step4', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Submitted the SEPA payment method form with valid data on the onboarding step4 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step4-payment-method-sepa', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step4 payment method SEPA has been selected successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestOnboardingStep4PaymentMethodSepa;