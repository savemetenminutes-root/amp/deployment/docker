'use strict';

const Logger = require('./Driver/Logger');
const Xunit = require('./Driver/Xunit');

class Output
{
    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    get config() {
        return this._config;
    }
    set config(value) {
        this._config = value;
    }

    get driver() {
        return this._driver;
    }
    set driver(value) {
        this._driver = value;
    }

    constructor(gias, config, driver) {
        this._gias = gias;
        this._config = config;
        this._driver = driver || new Xunit({});
    }

    out(results) {
        switch(this.driver.constructor.name) {
            case 'Xunit':
                this.driver.out(results);
                break;
            case Logger:

            default:
                break;
        }
    }
}

module.exports = Output;