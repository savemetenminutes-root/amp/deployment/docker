'use strict';

const Route = require('./Route');
const Browser = require('./Browser');
const Page = require('./Page');
const Element = require('./Element');

class Web
{
    static get Route() {
        return Route;
    }

    static get Browser() {
        return Browser;
    }

    static get Page() {
        return Page;
    }

    static get Element() {
        return Element;
    }
}

module.exports = Web;