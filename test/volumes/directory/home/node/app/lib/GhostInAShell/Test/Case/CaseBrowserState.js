'use strict';

const PageUndefinedDecorator = require('../../Web/Page/PageUndefinedDecorator');
const ElementUndefinedDecorator = require('../../Web/Element/ElementUndefinedDecorator');

class CaseBrowserState
{
    get currentPage() {
        return this._currentPage;
    }
    set currentPage(value) {
        this._currentPage = value;
    }

    get currentElement() {
        return this._currentElement;
    }
    set currentElement(value) {
        this._currentElement = value;
    }

    constructor(currentPage, currentElement) {
        this._currentPage = currentPage;
        this._currentElement = currentElement;
    }
}

module.exports = CaseBrowserState;