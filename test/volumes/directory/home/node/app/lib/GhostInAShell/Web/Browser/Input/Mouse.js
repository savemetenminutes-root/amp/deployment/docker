'use strict';

class Origin
{
    static get VIEWPORT() {
        return 1;
    }

    static get POINTER() {
        return 2;
    }

    static get ELEMENT() {
        return 4;
    }
}

class Mouse
{
    static get Origin() {
        return Origin;
    }

    static async addEventListenerMousePosition(browser) {
        await browser.addEventListenerMousePosition();
    }

    static async getCurrentMousePosition(browser) {
        return await browser.getCurrentMousePosition();
    }
}

module.exports = Mouse;