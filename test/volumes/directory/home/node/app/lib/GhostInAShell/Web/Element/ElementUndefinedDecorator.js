'use strict';

const Element = require('.');

class ElementUndefinedDecorator extends Element
{
    constructor(name, selectors, behavior, webElement) {
        super(name, selectors, behavior, webElement);
        this._name = '- Undefined Element -';
        this._selectors = [];
        this._behavior = null;
        this._webElement = null;
    }

    async clear(browser) {
        return this;
    }

    async sendKeys(browser, ...keys) {
        return this;
    }

    async click(browser) {
        return this;
    }

    async uncheck(browser) {
        return this;
    }
}

module.exports = ElementUndefinedDecorator;