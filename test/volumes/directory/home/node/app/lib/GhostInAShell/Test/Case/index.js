'use strict';

class Case
{
    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get validationCallable() {
        return this._validationCallable;
    }
    set validationCallable(value) {
        this._validationCallable = value;
    }

    get callables() {
        return this._callables;
    }
    set callables(value) {
        this._callables = value;
    }

    constructor(gias, name, validationCallable, callables) {
        this._gias = gias;
        this._name = name;
        this._validationCallable = validationCallable || Case.validateDefault;
        this._callables = callables || [];
    }

    async run() {
        const now = new Date();
        let results = {
            name: this.name,
            items: [],
        };
        for(let callableIndex in this.callables) {
            if(this.callables.hasOwnProperty(callableIndex)) {
                let result = await this.callables[callableIndex]();
                //console.log('Case: run: ');
                //console.log(result);
                results.items[callableIndex] = {
                    callable: this.callables[callableIndex],
                    result: result,
                };
            }
        }
        results.time = ((new Date()).getTime() - now.getTime()) / 1000;
        results.validation = this.validationCallable(results);
        results.status = 'passed';

        return results;
    }

    static validateDefault(results) {
        return true;
    }
}

module.exports = Case;