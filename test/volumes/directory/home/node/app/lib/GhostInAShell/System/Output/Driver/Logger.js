'use strict';

//const Type = require('../Type');

class LogLevel {
    static get BASIC() {
        return 1;
    }

    static get NORMAL() {
        return 2;
    }

    static get VERBOSE() {
        return 4;
    }

    static get DEBUG() {
        return 8;
    }
}

class Logger {
    constructor(config) {
        this.config = config;
        this.config.logLevel = this.config.logLevel || Logger.LogLevel.NORMAL;
    }

    static get LogLevel() {
        return LogLevel;
    }

    out(type, messages) {
        messages.forEach(
            (message) => {
                let preText = '';
                let postText = '';
                switch (type) {
                    case ClassSeleniumXOutputType.INFO:
                        preText = 'Info: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ACTION:
                        preText = 'Action: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.TEST:
                        preText = 'TEST: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.CHECK:
                        preText = 'Check: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.PASSED:
                        preText = 'Passed: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.SUCCESS:
                        preText = 'SUCCESS: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.INIT:
                        preText = 'Init: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ENVIRONMENT:
                        preText = '<<<===>>> ';
                        postText = ' <<<===>>>';
                        break;
                    case ClassSeleniumXOutputType.PLAIN:
                    default:
                        preText = '';
                        postText = '';
                        break;
                }
                console.log(preText + message + postText);
            }
        )
    }
}

module.exports = Logger;