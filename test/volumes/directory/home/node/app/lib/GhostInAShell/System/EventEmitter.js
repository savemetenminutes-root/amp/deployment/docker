'use strict';

const Events = require('events');

class EventEmitter extends Events
{
    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    constructor(gias) {
        super();
        this._gias = gias;

        this.on(
            'tests_complete',
            this.gias.out.bind(this.gias)
        );
    }
}

module.exports = EventEmitter;