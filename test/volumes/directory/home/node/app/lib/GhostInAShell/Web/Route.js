'use strict';

const ExceptionHandler = require('../System/ExceptionHandler');
const Exception = require('../System/Exception');

class Route
{
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }

    get requiresAuth() {
        return this._requiresAuth;
    }
    set requiresAuth(value) {
        this._requiresAuth = !!value || false;
    }

    constructor(url, requiresAuth) {
        this._url = url;
        this._requiresAuth = !!requiresAuth || false;
    }

    static getRouteUrlRelative(route, base) {
        if(route.url.indexOf(base.url) !== 0) {
            ExceptionHandler.throwException(
                null,
                Exception.Severity.FATAL,
                Exception.Type.RUNTIME,
                'Invalid route passed to getRouteRelative as it does not start with the base URL.',
            );
        }

        return route.url.substr(base.url.length);
    }
}

module.exports = Route;