'use strict';

class Selector
{
    static get Type() {
        return {
            CSS: 'css',
            XPATH: 'xpath',
            LINK_TEXT: 'linkText',
            ID: 'id',
            NAME: 'name',
        };
    }

    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }

    get value() {
        return this._value;
    }
    set value(value) {
        this._value = value;
    }

    get collectionIndex() {
        return this._collectionIndex;
    }
    set collectionIndex(value) {
        this._collectionIndex = value;
    }

    constructor(type, value, collectionIndex) {
        this._type = type;
        this._value = value;
        this._collectionIndex = collectionIndex;
    }
}

module.exports = Selector;