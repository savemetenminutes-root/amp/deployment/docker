'use strict';

//const System = require('./System');
const EventEmitter = require('./System/EventEmitter');
const Web = require('./Web');
const Browser = require('./Web/Browser');
const Element = require('./Web/Element');
const Selector = require('./Web/Element/Selector');
const Output = require('./System/Output');
const Test = require('./Test');

class GhostInAShell {
    static get System() {
        return System;
    }

    static get Test() {
        return Test;
    }

    static get Web() {
        return Web;
    }

    static get Browser() {
        return Browser;
    }

    get config() {
        return this._config;
    }
    set config(value) {
        this._config = value;
    }

    get eventEmitter() {
        return this._eventEmitter;
    }
    set eventEmitter(value) {
        this._eventEmitter = value;
    }

    get browser() {
        return this._browser;
    }
    set browser(value) {
        this._browser = value;
    }

    get pages() {
        return this._pages;
    }
    set pages(value) {
        this._pages = value;
    }

    get test() {
        return this._test;
    }
    set test(value) {
        this._test = value;
    }

    get output() {
        return this._output;
    }
    set output(value) {
        this._output = value;
    }

    get environment() {
        return this._environment;
    }
    set environment(value) {
        this._environment = value;
    }

    constructor(config, pages, test) {
        this._config = config;
        this._eventEmitter = new EventEmitter(this);
        this.eventEmitter.on(
            'debug_promise',
            async function(callable, thisRef, args) {
                let a = await callable.apply(thisRef, args);
                console.log(a);
            }
        );
        this._pages = pages;
        this._test = test || { // TODO: MK - use the Test class
            assemblies: [],
        };
        this._output = new Output(this);
        this._environment = {
            isWindows: process.platform === 'win32',
        };

        return (async() => {
            this._browser = await new Browser(this, this.pages);

            return this;
        })();
    }

    addTestAssembly(name, validationCallable, collections) {
        this
            .test
            .assemblies
            .push(new GhostInAShell.Test.Assembly(this, name, validationCallable, collections));

        return this;
    }

    addTestCollection(name, validationCallable, cases) {
        this
            .test
            .assemblies[this.test.assemblies.length-1]
            .collections
            .push(new GhostInAShell.Test.Collection(this, name, validationCallable, cases));

        return this;
    }

    addTestCollectionLoadPage(name) {
        this.addTestCollection('Load Page: ' + name)
            .addTestCase(
                'Load Route: ' + this.browser.pages[name].route.url,
                null,
                [
                    async () => {
                        //console.log('gias.addTestCollectionLoadPage("' + name + '")');
                        if(!this.browser.state.currentPage || this.browser.state.currentPage.name !== name) {
                            //console.log('gias.addTestCollectionLoadPage - navigating ' + (this.browser.state.currentPage ? this.browser.state.currentPage.name : 'about:blank') + ' -> ' + name + ' to the page...');
                            let now = new Date();
                            await this.browser.loadPage(name);
                            //console.log('gias.addTestCollectionLoadPage("' + name + '") - navigation complete!. Took ' + (((new Date()).getTime() - now.getTime()) / 1000) + ' seconds.');
                        }

                        return this.browser.state.currentPage;
                    },
                ]
            )
            .addTestCaseBrowser('Load Ember Liquid Container')
                .waitUntilElementInDomInstance(
                    new Element(
                        'Ember Liquid Container',
                        [
                            new Selector(Selector.Type.CSS, 'div.liquid-container'),
                        ],
                    ),
                    20000
                )
        ;

        return this;
    }

    addTestCollectionBasic() {
        if(false) {
            let cases = [];

            const pageSource = this.browser.getPageSource();
            if (pageSource.length < 1000) {
                throw new Error('>>>>>> Error: The ' + this.gias.browser.state.currentPage.name + ' page source code is suspiciously short. Number of characters: ' + pageSource.length);
            }
            console.log('Passed: The ' + this.gias.browser.state.currentPage.name + ' page source code is ' + pageSource.length + ' characters long.');

            /*
            if (!Collection.validateHtmlSourceBasic(pageSource)) {
                throw new Error('>>>>>> Error: The ' + this.gias.browser.state.currentPage.name + ' page source code does not contain proper html opening/closing tag(s).');
            }
            console.log('Passed: The ' + this.gias.browser.state.currentPage.name + ' page contains an opening and closing html tag.');
            */

            const htmlElementsCount = this.gias.browser.getHtmlElementsCount();
            if (htmlElementsCount < 10) {
                throw new Error('>>>>>> Error: The ' + this.gias.browser.state.currentPage.name + ' page has <10 HTML elements. Number of HTML elements: ' + htmlElementsCount);
            }
            console.log('Passed: The ' + this.gias.browser.state.currentPage.name + ' page contains ' + htmlElementsCount + ' HTML elements.');

            /*
            if(this.state.pageImageInfo.getWhitenessPercentage() > 90) {
                throw new Error('>>>>>> Error: The ' + browser.state.currentPage.name + ' page is >90% white. It might be erroring out resulting in a (almost) blank page.');
            }
            console.log('Passed: The ' + browser.state.currentPage.name + ' page is ' + this.state.pageImageInfo.getWhitenessPercentage().toFixed(2) + '% white.');
            console.log('Info: The ' + browser.state.currentPage.name + ' page contains ' + this.state.pageImageInfo.getUniqueColorsCount() + ' unique color(s).');
            */

            return this.addCollection('Basic', cases);
        }

        return this;
    }

    addTestCollectionEnumeratePageElements(pageName) {
        this
            .addTestCollection('Enumerate Page Elements')
                .addTestCaseBrowser('Whether all defined page elements exist')
                    .enumeratePageElements(pageName)
        ;

        return this;
    }

    addTestCase(name, validationCallable, callables) {
        this
            .test
            .assemblies[this.test.assemblies.length-1]
            .collections[this.test.assemblies[this.test.assemblies.length-1].collections.length-1]
            .cases
            .push(new GhostInAShell.Test.Case(this, name, validationCallable, callables));

        return this;
    }

    addTestCases(name, validationCallable, callables) {
        return this.addTestCase(name, validationCallable, callables);
    }

    addTestCaseBrowser(name, validationCallable, callables) {
        const testCaseBrowser = new GhostInAShell.Test.CaseBrowser(this, name, validationCallable, callables);

        this
            .test
            .assemblies[this.test.assemblies.length-1]
            .collections[this.test.assemblies[this.test.assemblies.length-1].collections.length-1]
            .cases
            .push(testCaseBrowser);

        return testCaseBrowser;
    }

    async runTests() {
        let results = {
            items: [],
        };
        for(let assemblyIndex in this.test.assemblies) {
            let result = await this.test.assemblies[assemblyIndex].run();
            if(this.test.assemblies.hasOwnProperty(assemblyIndex)) {
                results.items[assemblyIndex] = {
                    assembly: this.test.assemblies[assemblyIndex],
                    result: result,
                };
            }
        }

        this.eventEmitter.emit('tests_complete', results);

        return results;
    }

    out(results) {
        this.output.out(results);
    }

    static getTimestampForFilename() {
        let now = new Date();
        return now.getUTCFullYear() + '' + ((now.getUTCMonth() + 1) + '').padStart(2, '0') + (now.getUTCDate() + '').padStart(2, '0') + '_' + (now.getUTCHours() + '').padStart(2, '0') + (now.getUTCMinutes() + '').padStart(2, '0') + (now.getUTCSeconds() + '').padStart(2, '0');
    }

    async procOpen(command, input, output, error) {
        const {streamWrite, streamEnd, onExit} = require('@rauschma/stringio');
        const {spawn, spawnSync} = require('child_process');

        let options = {};
        options.shell = '/bin/bash';
        options.input = input || null;
        options.stdio = [
            null,
        ];
        if(output) {
            options.stdio.push(output || null);
        }
        if(error) {
            options.stdio.push(error || null);
        }
        let sink = spawnSync(
                command,
                [], // command arguments
                options,
            );

        return sink;
    }

    async waitForKeypress() {
        process.stdin.setRawMode(true);
        // Not working properly. Pressing a key doesn't resolve the promise.
        return new Promise(resolve => process.stdin.once('data', () => {
            process.stdin.setRawMode(false);

            resolve();
        }));
    }

    async quit() {
        if(process.env.GHOST_IN_A_SHELL_BROWSER_KEEP_WINDOW_OPEN && (process.env.GHOST_IN_A_SHELL_BROWSER_KEEP_WINDOW_OPEN === '1')) {
            await this.waitForKeypress();

            return await this.browser.quit();
            /*
            console.log('Press any key to exit...');

            process.stdin.setRawMode(true);
            process.stdin.resume();
            process.stdin.on('data', process.exit.bind(process, 0));
            */
        }

        return await this.browser.quit()
    }

    async shutdownWithError(error) {
        process.exitCode = 1;
        console.log(error);
        //console.log(error.message);
        console.log(':( Failure: Test failed.');
        //console.log('seleniumX is ' + typeof seleniumX);

        await this.quit(); // terminates the session and disposes of the driver effectively ending the process
    }

    async shutdownWithSuccess() {
        //console.log(':) Success: The test was passed successfully.');
        //console.log('seleniumX is ' + typeof seleniumX);

        await this.quit(); // terminates the session and disposes of the driver effectively ending the process
    }
}

module.exports = GhostInAShell;