'use strict';

//const State = require('./State');
//const Window = require('./Window');
//const Dom = require('./Dom');

class Page
{
    static get State() {
        return State;
    }

    static get Window() {
        return Window;
    }

    static get Dom() {
        return Dom;
    }

    get configFilename() {
        return this._configFilename;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get route() {
        return this._route;
    }
    set route(value) {
        this._route = value;
    }

    get elements() {
        return this._elements;
    }
    set elements(value) {
        this._elements = value;
    }

    constructor(configFilename, name, route, elements) {
        this._configFilename = configFilename;
        this._name = name;
        this._route = route;
        this._elements = elements || [];
    }
}

module.exports = Page;