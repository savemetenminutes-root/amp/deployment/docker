'use strict';

const PageUndefinedDecorator = require('../../Web/Page/PageUndefinedDecorator');

class BrowserState
{
    get currentPage() {
        return this._currentPage;
    }
    set currentPage(value) {
        this._currentPage = value;
    }

    constructor(currentPage) {
        this._currentPage = currentPage || new PageUndefinedDecorator();
    }
}

module.exports = BrowserState;