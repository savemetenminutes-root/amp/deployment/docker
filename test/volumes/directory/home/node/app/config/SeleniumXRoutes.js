'use strict';

const baseUrl = 'https://my-d.haftpflichthelden.de:16200';

class ClassSeleniumXRoutes {
    static get BASE() {
        return baseUrl;
    }
    static get HOME() {
        return ClassSeleniumXRoutes.BASE + '/';
    }
    static get LOGIN() {
        return ClassSeleniumXRoutes.BASE + '/login';
    }
    static get SIGNUP() {
        return ClassSeleniumXRoutes.BASE + '/signup';
    }
    static get FORGOT() {
        return ClassSeleniumXRoutes.BASE + '/forgot';
    }
    static get ONBOARDING_PRODUCT_SELECT() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/product-select';
    }
    static get ONBOARDING_CONFIRM_RACE() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/confirm-race';
    }
    static get ONBOARDING_PRODUCT_SPECIFIC() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/product-specific';
    }
    static get ONBOARDING_STEP1() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/step1';
    }
    static get ONBOARDING_STEP2() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/step2';
    }
    static get ONBOARDING_STEP3() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/step3';
    }
    static get ONBOARDING_STEP4() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/step4';
    }
    static get ONBOARDING_STEP5() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/step5';
    }
    static get ONBOARDING_SIGNATURE() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/signature';
    }
    static get ONBOARDING_WELCOME() {
        return ClassSeleniumXRoutes.BASE + '/onboarding/welcome';
    }
    static get CONTRACT_SELECT() {
        return ClassSeleniumXRoutes.BASE + '/contract-select';
    }
    static get DASHBAORD_SETTINGS() {
        return ClassSeleniumXRoutes.BASE + '/settings';
    }
    static get DASHBOARD_INFO_QUESTIONS() {
        return ClassSeleniumXRoutes.BASE + '/info/questions';
    }
    static get DASHBOARD_INFO_HOW() {
        return ClassSeleniumXRoutes.BASE + '/info/how';
    }
    static get DASHBOARD_STATUS() {
        return ClassSeleniumXRoutes.BASE + '/status';
    }
    static get DASHBOARD_INVITE() {
        return ClassSeleniumXRoutes.BASE + '/invite';
    }
    static get DASHBOARD_ACCOUNT_SETTINGS() {
        return ClassSeleniumXRoutes.BASE + '/account-settings';
    }
    static get DASHBOARD_CHANGE_PAYMENT() {
        return ClassSeleniumXRoutes.BASE + '/change-payment';
    }
    static get DASHBOARD_CONFIRM_CANCEL() {
        return ClassSeleniumXRoutes.BASE + '/confirm-cancel';
    }
}

module.exports = ClassSeleniumXRoutes;