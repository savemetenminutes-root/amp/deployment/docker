'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'FirstName Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vorname')]/.."),
        ],
    ),
    new Element(
        'FirstName Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vorname')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'LastName Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Nachname')]/.."),
        ],
    ),
    new Element(
        'LasttName Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Nachname')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Address Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Adresse')]/.."),
        ],
    ),
    new Element(
        'Address Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Adresse')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Address Input',
        [
            new Selector(Selector.Type.CSS, 'input[type="text"]'),
        ],
    ),
    new Element(
        'Post Code Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'PLZ')]/.."),
        ],
    ),
    new Element(
        'Post Code Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'PLZ')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'City Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Ort')]/.."),
        ],
    ),
    new Element(
        'City Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Ort')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Mobile Number Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Mobilnummer')]/.."),
        ],
    ),
    new Element(
        'Mobile Number Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Mobilnummer')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Partner Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Partner/in')]/.."),
        ],
    ),
    new Element(
        'Partner Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Partner/in')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Add Parent Link',
        [
            new Selector(Selector.Type.LINK_TEXT, "//label[contains(string(), 'Partner/in eintragen')]"),
        ],
    ),





    //Payment Tab
    new Element(
        'Paypal Account Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Paypal Konto')]"),
        ],
    ),
    new Element(
        'Change the payment method Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Zahlmethode ändern'),
        ],
    ),
    new Element(
        'Paypal Account Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Paypal Konto')]/../input[contains(@type, 'text')]"),
        ],
    ),




    //Account Tab
    new Element(
        'Contract Number Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vertragsnummer')]/.."),
        ],
    ),
    new Element(
        'Contract Number Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vertragsnummer')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Contract Start Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vertragsbeginn')]/.."),
        ],
    ),
    new Element(
        'Contract Start Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Vertragsbeginn')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'Next Due Date Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Nächste Fälligkeit')]/.."),
        ],
    ),
    new Element(
        'Next Due Date Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Nächste Fälligkeit')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'View the conditioner Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Bedingungswerk anschauen'),
        ],
    ),
    new Element(
        'View the conditioner Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Bedingungswerk anschauen'),
        ],
    ),
    new Element(
        'Terminate Contract Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'VERTRAG KÜNDIGEN'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Settings',
    routes['dashboard-settings'],
    elements,
);