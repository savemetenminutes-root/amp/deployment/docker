'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Contact Link',
        [
            new Selector(Selector.Type.PARTIAL_LINK_TEXT, 'Wir freuen uns auf deinen Anruf'),
        ],
    ),
    new Element(
        'Partner Link',
        [
            new Selector(Selector.Type.PARTIAL_LINK_TEXT, 'Unser Versicherungspartner'),
        ],
    ),
    new Element(
        'Documents Link',
        [
            new Selector(Selector.Type.PARTIAL_LINK_TEXT, 'Entdecke den Helden Schutz'),
        ],
    ),
    new Element(
        'Find Answers Link',
        [
            new Selector(Selector.Type.PARTIAL_LINK_TEXT, 'Du willst es selbst nachlesen?'),
        ],
    ),
    new Element(
        'Terms Legal Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'AGB'),
        ],
    ),
    new Element(
        'Imprint Legal Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Impressum'),
        ],
    ),
    new Element(
        'Privacy Legal Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Datenschutz'),
        ],
    ),
    new Element(
        'Phone Icon',
        [
            new Selector(Selector.Type.CSS, 'i.icon-phone'),
        ],
    ),
    new Element(
        'Handsup Icon',
        [
            new Selector(Selector.Type.CSS, 'i.icon-handsup'),
        ],
    ),
    new Element(
        'Shield Icon',
        [
            new Selector(Selector.Type.CSS, 'i.icon-shield'),
        ],
    ),
    new Element(
        'Bulb Icon',
        [
            new Selector(Selector.Type.CSS, 'i.icon-bulb'),
        ],
    ),
    new Element(
        'Dollar Icon',
        [
            new Selector(Selector.Type.CSS, 'i.icon-dollar'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Info Questions',
    routes['dashboard-info-questions'],
    elements,
);