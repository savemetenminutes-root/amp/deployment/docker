'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Go Back Link',
        [
            new Selector(Selector.Type.CSS, 'i.icon-profile'),
        ],
    ),
    new Element(
        'Heading',
        [
            new Selector(Selector.Type.XPATH, '//h2[contains(string(), "Wer soll versichert werden?")]'),
        ],
    ),
    new Element(
        'Horse Name Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Wie heißt dein Pferd?")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Add Another Horse Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Weiteres Pferd hinzufügen'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'WEITER'),
        ],
    ),

    new Element(
        'No Horse Name Provided Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Bitte gib die Namen deiner Pferde ein.")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Product Specific',
    routes['onboarding-product-specific'],
    elements,
);