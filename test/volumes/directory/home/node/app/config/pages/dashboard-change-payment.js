'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'SEPA Image',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-sepa.svg')]"),
        ],
    ),
    new Element(
        'SEPA Checkbox',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-sepa.svg')]/../span[contains(@class, 'radio')]"),
        ],
    ),
    new Element(
        'Paypal Image',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-paypal.png')]"),
        ],
    ),
    new Element(
        'Paypal Checkbox',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-paypal.svg')]/../span[contains(@class, 'radio')]"),
        ],
    ),
    new Element(
        'Visa Image',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-visa-mastercard.png')]"),
        ],
    ),
    new Element(
        'Visa Checkbox',
        [
            new Selector(Selector.Type.XPATH, "//img[contains(@src, '/images/logo-visa-mastercard.svg')]/../span[contains(@class, 'radio')]"),
        ],
    ),



    // Inside SEPA Payment
    new Element(
        'SEPA Enter IBAN Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Kontonummer/BLZ statt IBAN eingeben'),
        ],
    ),
    new Element(
        'SEPA IBAN Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'IBAN')]"),
        ],
    ),
    new Element(
        'SEPA IBAN Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'IBAN')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'SEPA Account Owner Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Kontoinhaber')]"),
        ],
    ),
    new Element(
        'SEPA Account Owner Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Kontoinhaber')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'SEPA Issue Label',
        [
            new Selector(Selector.Type.XPATH, "//span[contains(string(), 'Ich erteile das SEPA-Lastschriftmandat zum Einzug der fälligen Beiträge von meinem Konto.')]"),
        ],
    ),
    new Element(
        'SEPA Issue Checkbox',
        [
            new Selector(Selector.Type.XPATH, "//span[contains(@class, 'checkbox')]"),
        ],
    ),
    new Element(
        'SEPA Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
    new Element(
        'Empty IBAN Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Bitte gib deine Bankdaten ein.")]'),
        ],
    ),
    new Element(
        'Invalid IBAN Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Deine IBAN sieht nicht richtig aus.")]'),
        ],
    ),
    new Element(
        'Must Agree To The SEPA Terms Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Bitte erteile uns das SEPA Mandat. ")]'),
        ],
    ),



    // Inside VISA Payment
    new Element(
        'VISA Credit Card Number Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Kreditkartennummer')]"),
        ],
    ),
    new Element(
        'VISA IBAN Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Kreditkartennummer')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'VISA Valid Until Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Gültig bis (Monat/Jahr)')]"),
        ],
    ),
    new Element(
        'VISA Valid Until Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'Gültig bis (Monat/Jahr)')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'VISA CVV2 Label',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'CVV2')]"),
        ],
    ),
    new Element(
        'VISA CVV2 Input',
        [
            new Selector(Selector.Type.XPATH, "//label[contains(string(), 'CVV2')]/../input[contains(@type, 'text')]"),
        ],
    ),
    new Element(
        'VISA Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
    new Element(
        'VISA Invalid Bank Details Error Message',
        [
            new Selector(Selector.Type.XPATH, "//div[contains(string(), 'Bitte gib deine Kreditkarteninformationen ein.')]"),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'WEITER'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Change Payment',
    routes['dashboard-change-payment'],
    elements,
);