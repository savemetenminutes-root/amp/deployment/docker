'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Email Input Field',
        [
            new Selector(Selector.Type.CSS, '[type="email"]'),
        ],
    ),
    new Element(
        'Password Input Field',
        [
            new Selector(Selector.Type.CSS, '[type="password"]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
    new Element(
        'Go Back To The Previous Page Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Jetzt anmelden.'),
        ],
    ),

    new Element(
        'Empty Email Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir benötigen deine E-Mail Adresse.")]'),
        ],
    ),
    new Element(
        'Invalid Email Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Deine E-Mail Adresse sieht nicht richtig aus.")]'),
        ],
    ),
    new Element(
        'Empty Password Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir benötigen dein Passwort zum einloggen.")]'),
        ],
    ),
    new Element(
        'Invalid Password Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Dein Passwort muss mindestens 8 Zeichen lang sein.")]'),
        ],
    ),
    new Element(
        'Email Has Already Been Registered Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Diese E-Mail Adresse existiert bereits bei uns. Willst Du dich lieber einloggen?")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Signup',
    routes['signup'],
    elements,
);