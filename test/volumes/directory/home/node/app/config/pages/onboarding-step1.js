'use strict';

const Route = require('../../lib/GhostInAShell/Web/Route');
const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const ElementStyledCheckbox = require('../../lib/GhostInAShell/Web/Element/ElementStyledCheckbox');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    /*
    // Different based on the selected product type
    new Element(
        'Heading',
        [
            new Selector(Selector.Type.XPATH, '//h2[contains(string(), "Wann soll der Vertrag")]'),
        ],
    ),
    */
    new Element(
        'Go Back Link',
        [
            new Selector(Selector.Type.CSS, 'i.icon-profile'),
        ],
    ),
    new Element(
        'Start Contract Immediately Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Ab Sofort")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new Element(
        'Set Contract Start Date Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Datum")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new Element(
        'Cancel Existing Contract Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Vertragswechsel")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new ElementStyledCheckbox(
        'User Record Verification Checkbox',
        [
            new Selector(Selector.Type.XPATH, "//span[contains(string(), '\"Ich hatte in den letzten 5 Jahren nicht mehr als 2 Schäden und bin nicht bei einem anderen Versicherer gekündigt worden.\"')]/../span[contains(@class, \"checkbox\")]"),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Step1',
    routes['onboarding-step1'],
    elements,
);