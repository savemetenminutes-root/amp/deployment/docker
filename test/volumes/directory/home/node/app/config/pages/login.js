'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Email Input Field',
        [
            new Selector(Selector.Type.CSS, '[type="email"]'),
        ],
    ),
    new Element(
        'Password Input Field',
        [
            new Selector(Selector.Type.CSS, '[type="password"]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.ID, 'login-button'),
        ],
    ),
    new Element(
        'Facebook Submit Button',
        [
            new Selector(Selector.Type.ID, 'fb-login-button'),
        ],
    ),
    new Element(
        'Register Now Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Jetzt registrieren.'),
        ],
    ),
    new Element(
        'Forgot Password Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Passwort vergessen?'),
        ],
    ),

    new Element(
        'Empty Email Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir benötigen deine E-Mail Adresse.")]'),
        ],
    ),
    new Element(
        'Invalid Email Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Deine E-Mail Adresse sieht nicht richtig aus.")]'),
        ],
    ),
    new Element(
        'Empty Password Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir benötigen dein Passwort zum einloggen.")]'),
        ],
    ),
    new Element(
        'Login Is Incorrect Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Deine Logindaten sind leider nicht korrekt.")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Login',
    routes['login'],
    elements,
);