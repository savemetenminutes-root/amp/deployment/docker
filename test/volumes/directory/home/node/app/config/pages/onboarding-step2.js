'use strict';

const Route = require('../../lib/GhostInAShell/Web/Route');
const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Go Back To The Previous Page Link',
        [
            new Selector(Selector.Type.XPATH, '//a[contains(@href, "' + Route.getRouteUrlRelative(routes['onboarding-step1'], routes['base']) + '")]'),
        ],
    ),
    new Element(
        'Heading',
        [
            new Selector(Selector.Type.XPATH, '//h2[contains(string(), "Wer bist du?")]'),
        ],
    ),
    new Element(
        'First Name Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Vorname")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty First Name Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deinen Vornamen")]'),
        ],
    ),
    new Element(
        'Last Name Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Nachname")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty Last Name Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deinen Nachnamen")]'),
        ],
    ),
    new Element(
        'Street Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Straße")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty Street Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deine Straße.")]'),
        ],
    ),
    new Element(
        'Address Number Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Hausnummer")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty Address Number Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deine Hausnummer.")]'),
        ],
    ),
    new Element(
        'Postal Code Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Postleitzahl")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty Postal Code Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deine PLZ.")]'),
        ],
    ),
    new Element(
        'City Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Ort")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty City Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deine Stadt.")]'),
        ],
    ),
    new Element(
        'Phone Number Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Mobilnummer")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Empty Phone Number Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(text(), "Wir brauchen deine Mobilfunknummer.")]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Step2',
    routes['onboarding-step2'],
    elements,
);