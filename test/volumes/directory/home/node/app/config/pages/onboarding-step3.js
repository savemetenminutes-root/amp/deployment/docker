'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Heading',
        [
            new Selector(Selector.Type.XPATH, '//h2[contains(string(), "Dein jährlicher Beitrag.")]'),
        ],
    ),
    new Element(
        'Use Voucher Button',
        [
            new Selector(Selector.Type.XPATH, '//a[contains(string(), "Helden Code eingeben")]'),
        ],
    ),
    new Element(
        'Voucher Code Input Field Label',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Helden Code eingeben")]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'WEITER'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Step3',
    routes['onboarding-step3'],
    elements,
);