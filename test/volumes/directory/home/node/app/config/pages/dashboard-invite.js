'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
    new Element(
        'Textarea Field',
        [
            new Selector(Selector.Type.CSS, 'textarea'),
        ],
    ),
    new Element(
        'Facebook Icon',
        [
            new Selector(Selector.Type.lINK_TEXT, 'Facebook'),
        ],
    ),
    new Element(
        'Twitter Icon',
        [
            new Selector(Selector.Type.lINK_TEXT, 'Twitter'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Invite',
    routes['dashboard-invite'],
    elements,
);