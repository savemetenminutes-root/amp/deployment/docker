'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Private Contract Icon',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon icon-adresse")]'),
        ],
    ),
    new Element(
        'Dog Contract Icon',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon icon-fill8")]'),
        ],
    ),
    new Element(
        'Horse Contract Icon',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon icon-pferde")]'),
        ],
    ),
    new Element(
        'Add Contract Icon',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon icon-add2 gray")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Contract Select',
    routes['dashboard-contract-select'],
    elements,
);