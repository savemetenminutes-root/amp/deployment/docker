'use strict';

const GhostInAShell = require('./lib/GhostInAShell');
const Browser = require('./lib/GhostInAShell/Web/Browser');
const config = require('./config/config');
const pages = require('./config/pages');

let gias;

try {
    (async() => {
        const Imap = require('imap');
        const inspect = require('util').inspect;

        let imap = new Imap({
          //user: process.env.LOGIN_USER_EMAIL,
          user: 'a@a.a',
          password: '123123',
          host: 'insurance-hero_mail_dovecot',
          port: 143,
          tls: true
        });

        function openInbox(cb) {
          imap.openBox('INBOX', true, cb);
        }

        imap.once('ready', function() {
          openInbox(function(err, box) {
            if (err) throw err;
            let f = imap.seq.fetch('1:3', {
              bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
              struct: true
            });
            f.on('message', function(msg, seqno) {
              console.log('Message #%d', seqno);
              let prefix = '(#' + seqno + ') ';
              msg.on('body', function(stream, info) {
                let buffer = '';
                stream.on('data', function(chunk) {
                  buffer += chunk.toString('utf8');
                });
                stream.once('end', function() {
                  console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                });
              });
              msg.once('attributes', function(attrs) {
                console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));
              });
              msg.once('end', function() {
                console.log(prefix + 'Finished');
              });
            });
            f.once('error', function(err) {
              console.log('Fetch error: ' + err);
            });
            f.once('end', function() {
              console.log('Done fetching all messages!');
              imap.end();
            });
          });
        });

        imap.once('error', function(err) {
          console.log(err);
        });

        imap.once('end', function() {
          console.log('Connection ended');
        });

        imap.connect();

        process.exit();

        gias = await new GhostInAShell(config, pages);

        gias

            .addTestAssembly('Login Page')
                .addTestCollectionLoadPage('Login')
                .addTestCollection('Valid login')
                    .addTestCaseBrowser('Valid login')
                        .onPage('Login')
                        .element('Email Input Field')
                        .clear()
                        .sendKeys(process.env.LOGIN_USER_EMAIL)
                        .element('Password Input Field')
                        .clear()
                        .sendKeys('1]2[3p4o')
                        .element('Submit Button')
                        .click()
                        .runAsync(
                            'login to redirect to one of several possible URLs',
                            async function() {
                                return await Promise.race(
                                    [
                                        this.waitUntilPageLoadedSync('Onboarding Product Select', 40000),
                                        this.waitUntilPageLoadedSync('Dashboard Contract Select', 40000)
                                    ]
                                );
                            }
                        )

            .addTestAssembly('Onboarding Step1 Page')
                .addTestCollection('Click on the Personal Liability Product Link')
                    .addTestCaseBrowser('Click on the Personal Liability Product Link')
                        .onPage('Onboarding Product Select')
                        .waitUntilElementInDom('Personal Liability Product Link', 8000)
                        .click()
                        .waitUntilPageLoaded('Onboarding Step1', 8000)
                .addTestCollectionLoadPage('Onboarding Step1')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Step1')
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Valid submission')
                        .onPage('Onboarding Step1')
                        .element('User Record Verification Checkbox')
                        .check()
                        .element('Submit Button')
                        .click()
                        .waitUntilPageLoaded('Onboarding Step2', 8000)

            .addTestAssembly('Onboarding Step2 Page')
                .addTestCollectionLoadPage('Onboarding Step2')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Step2')
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Valid submission')
                        .onPage('Onboarding Step2')
                        .element('First Name Input Field')
                        .sendKeys('auto')
                        .element('Last Name Input Field')
                        .sendKeys('test')
                        .element('Street Input Field')
                        .sendKeys('Somestreet blvd.')
                        .element('Address Number Input Field')
                        .sendKeys('34')
                        .element('Postal Code Input Field')
                        .sendKeys('10000')
                        .element('City Input Field')
                        .sendKeys('Hamburg')
                        .element('Phone Number Input Field')
                        .sendKeys('+49171123456')
                        .element('Submit Button')
                        .click()
                        .waitUntilPageLoaded('Onboarding Step3', 8000)

            .addTestAssembly('Onboarding Step3 Page')
                .addTestCollectionLoadPage('Onboarding Step3')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Step3')
                .addTestCollection('Valid submission')
                    .addTestCaseBrowser('Valid submission')
                        .onPage('Onboarding Step3')
                        .element('Submit Button')
                        .click()
                        .waitUntilPageLoaded('Onboarding Step4', 8000)

            .addTestAssembly('Onboarding Step4 Page')
                .addTestCollectionLoadPage('Onboarding Step4')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Step4')
                .addTestCollection('Select payment method SEPA')
                    .addTestCaseBrowser('Select payment method SEPA')
                        .onPage('Onboarding Step4')
                        .runAsync(
                            'payment method radio buttons or pre-selected payment method elements to appear',
                            async function () {
                                return await Promise.race(
                                    [
                                        this.waitUntilElementInDomSync('Payment Method SEPA Radio Button', 20000),
                                        this.waitUntilElementInDomSync('Change Payment Method Button', 20000),
                                    ]
                                );
                            }
                        )
                        .if(
                            'if the Change Payment Method Button is present click it',
                            async function () {
                                return await this.elementSync('Payment Method Button', 1000);
                            },
                            async function () {
                                return await this.clickSync('Payment Method Button');
                            },
                        )
                        .runAsync(
                            'payment method radio buttons to appear',
                            async function () {
                                return await Promise.all(
                                    [
                                        this.waitUntilElementInDomSync('Payment Method SEPA Radio Button', 1000),
                                        this.waitUntilElementInDomSync('Payment Method Paypal Radio Button', 1000),
                                        this.waitUntilElementInDomSync('Payment Method Bank Card Radio Button', 1000),
                                    ]
                                );
                            }
                        )
                        .element('Payment Method SEPA Radio Button')
                        .click()
                        .runAsync(
                            'payment method SEPA form elements to appear',
                            async function () {
                                return await Promise.all(
                                    [
                                        this.waitUntilElementInDomSync('Payment Method SEPA Logo Image', 1000),
                                        this.waitUntilElementInDomSync('Change Payment Method Button', 1000),
                                        this.waitUntilElementInDomSync('Use Account Number/Bank Code Button', 1000),
                                        this.waitUntilElementInDomSync('IBAN Input Field', 1000),
                                        this.waitUntilElementInDomSync('Account Holder Input Field', 1000),
                                    ]
                                );
                            }
                        )
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Valid submission')
                        .onPage('Onboarding Step4')
                        .element('IBAN Input Field')
                        .clear()
                        .sendKeys('DE89370400440532013000')
                        .element('Agree To The SEPA Terms Checkbox')
                        .check()
                        .element('Submit Button')
                        .click()
                        .waitUntilElementInDom('Must Agree To The SEPA Terms Message', 8000)
                        .waitUntilPageLoaded('Onboarding Step5', 8000)

            .addTestAssembly('Onboarding Step5 Page')
                .addTestCollectionLoadPage('Onboarding Step5')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Step5')
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Valid submission')
                        .onPage('Onboarding Step5')
                        .element('Verify That The Provided Information Is Correct And Agree To All Terms And Conditions Checkbox')
                        .check()
                        .element('Verify Familiarity With The Product To Be Purchased Checkbox')
                        .check()
                        .element('Submit Button')
                        .click()
                        .waitUntilPageLoaded('Onboarding Signature', 8000)

            .addTestAssembly('Onboarding Signature Page')
                .addTestCollectionLoadPage('Onboarding Signature')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Onboarding Signature')
                .addTestCollection('Sign and submit')
                    .addTestCaseBrowser('Sign and submit')
                        .onPage('Onboarding Signature')
                        .element('Signature Canvas')
                        .mouseMoveToCenterOfCurrentElement()
                        .dragAndDrop(
                            [
                                {x: 1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 0, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 2, y: 3, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -5, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: -4, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 3, y: -2, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 2, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 0, y: 3, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 2, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -2, y: -1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -2, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 0, y: 2, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 2, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 2, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 3, y: -1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 3, y: -1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 2, y: -1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: -1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 1, y: 0, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: 0, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                                {x: -1, y: 1, duration: 0, origin: Browser.Input.Mouse.Origin.POINTER},
                            ]
                        )
                        .element('Submit Button')
                        //.click()
                        //.waitUntilPageLoaded('Onboarding Welcome', 60000)

                .addTestCollection('Emails received')
                    .addTestCase('Check inbox with IMAP')
        ;

        let testAssemblyResults = await gias.runTests(); // TODO: MK - See if passing the tasks as argument to gias.runTests() would be better

        //console.log('asdasdasdsadsadsadsad');

        return testAssemblyResults;
    })()
        .then(
            async(results) => {
                !gias || (async() => {return await gias.shutdownWithSuccess();})();
            }
        )
        .catch(async(error) => {
            !gias || await gias.shutdownWithError(error)
        });
} catch(e) {
    !gias || (async() => {return await gias.shutdownWithError(e);})();
}

!gias || (async() => {return await gias.shutdownWithSuccess();})();